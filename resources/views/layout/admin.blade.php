<!DOCTYPE html>
<html>
<head>
  <title> @yield('title')</title>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css">
  <link rel="stylesheet" href="/js/minified/themes/default.min.css" />
	{!! HTML::style('css/app.css') !!}
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
  @include('navigation.admin')
 
    @yield('content')
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.3.1/jquery.twbsPagination.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
  <script src="/js/minified/jquery.sceditor.bbcode.min.js"></script>
  <script src='/js/jquery-ui.min.js'></script>
  <script>

  var $hamburger = $(".hamburger");
  var adminMenu = $('.adminNavbar');
  $hamburger.on("click", function(e) {
    $hamburger.toggleClass("is-active");
    
    open = adminMenu.hasClass('active');
    adminMenu.removeClass('active').slideUp(800);
    e.preventDefault();
    if(!open) {
      adminMenu.addClass('active').slideDown(800);
       e.preventDefault()
    } 
  });

  </script>
  

    
</body>
</html>