<!DOCTYPE html>
<html>
<head>
  <title> @yield('title')</title>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css">
	{!! HTML::style('css/app.css') !!}
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
	@include('navigation.main')
 	@include('search.livesearch')
	@yield('header')
  

		@yield('content')
    

    <footer>
    	@include('navigation.footer')
    </footer>
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.3.1/jquery.twbsPagination.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
  <script src='/js/jquery-ui.min.js'></script>
  <script src='/css/jquery-ui.min.css'></script>
  <script>


var navBarLi = $('.mainNavbar li') ;
var windowWidth = $(window).width();

jQuery.curCSS = function(element, prop, val) {
    return jQuery(element).css(prop, val);
};

var $hamburger = $(".hamburger");
$hamburger.on("click", function(e) {
  $hamburger.toggleClass("is-active");
  $('.submenu').removeClass('open').slideUp();
  open = navBarLi.hasClass('active');
  navBarLi.removeClass('active').slideUp(800);
  e.preventDefault();
  if(!open) {
    navBarLi.addClass('active').slideDown(800);
     e.preventDefault()
  } 
});


$('.submenu').hide();
$('.submenu-container').hide();



$('.mainMenu').mouseenter(function(e){
  if(windowWidth > 1024) {
    submenuName = $(this).text();
    submenuName = submenuName.replace(" ","-");
    submenuName = submenuName.toLowerCase();
    $('#submenu-desktop').show();
    $(this).addClass('actif').siblings().removeClass('actif');
    $('#submenu-'+submenuName).addClass('open').show().siblings().removeClass('open').hide();   
    e.preventDefault();
  } 
});

$('#submenu-desktop').mouseleave(function(e){
  e.preventDefault();
  $(this).hide();
  $('.mainMenu').removeClass('actif');
});

$('.mainMenu').click(function(e){
  if(windowWidth > 1024) {
    return;
  } else {
  var id = this.id;
  $(this).css('list-style', 'url(../uploads/ressources/chevron_horiz_blanc.svg)')
  wasOpen = $(this).next('.submenu').hasClass('open');
  e.preventDefault();
  $('.submenu').removeClass('open').slideUp()
  $(this).removeClass('clicked');
  if (!wasOpen) {
    $('.submenu.' + id).addClass('open').slideDown();
    $( this ).toggleClass( "clicked" ).siblings().removeClass('clicked');
    if( $(this).hasClass('clicked')) {
      $(this).css('list-style', 'url(../uploads/ressources/chevron_vert_blanc.svg)').siblings().css('list-style', 'url(../uploads/ressources/chevron_horiz_blanc.svg)')
  }
}
}
});


  </script>
</body>
</html>