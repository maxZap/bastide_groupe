@extends('layout.app')
@section('title', 'article')
@endsection
@section('header')


<style> .color1{
		color:grey;
	}	
	.color2{
    color: white;
}

</style>
<div id="banniere"> 
	<img src="uploads/ressources/headerMain.JPG">
</div>
<div id="red">
	<div id ="titleRed">
		<h2 id="glowTitle1" class="color1">| PARTENAIRE</h2>
		<h2 id="glowTitle2" class="color1">| DE VOTRE SANTE </h2>
		<h2 id="glowTitle3" class="color1">| DEPUIS 40 ANS </h2>
	</div>
	<div id="contentRed">
		<p class="red0" >Nous portons des valeurs fortes a travers des personnalités affirmées</p>
		<p class="red1" style="display:none"> Bastide est présent dans près de 10 activités dans le domain médécial ...</p>
		<p class="red2" style="display:none;  width: 87%;"> le groupe Bastide fête, cette année, ses 40 ans d'existance. <br> L'entreprise fut créée a Nîmes en 1977 </p>
		<!--<a class="red0 redLink">Découvrez nos partenariats...</a>-->
		<!--<a class="red1 redLink"  style="display:none; background-color:rgb(2, 109, 207)">Découvrez toutes nos activités...</a>
		<a class="red2 redLink"  style="display:none; background-color:rgb(104, 53, 140)">Découvrez toute l'histoire..</a>-->
	</div>
	<a href="/partenariat"><span class="plusLink">+</span></a>
</div> 
@endsection
@section('content')
	<section id ="HotNews">
		<h1>A la une</h1>
		<h1> Bastide <span class="grisTv">Tv</span> </h1> 
    		<div class="hotarticle">
				@foreach($hotPosts as $hotPost)
	      			<img src="uploads/images/{{$hotPost->filename}}"> 
	      			<h1> Evènement </h1>
					<h2>{{ $hotPost->title }}</h2>
					<a href="{{ URL::to('/' .$hotPost->categorie['slug'] .'/'. $hotPost->subcategorie['slug'] . '/' . str_slug($hotPost->id. '-' . $hotPost->title)) }}">LIRE PLUS...</a>
				@endforeach

				@foreach($fileuploads as $files) 

					<iframe src="{{ $files->lien_youtube }}" frameborder="0" allowfullscreen></iframe>
				@endforeach
     		</div>
	</section>
	<section id="news-and-calendar">
		<div id ="news">
			<h1> Actualités </h1>
			<ul>
				@foreach($posts as $post) 
					<div class="table">
						<li><span class="date">13/06/2017</span><br/><span class="postTitle">{{$post->title}}</span><span class="plusLink">+</span></li>
					</div>
				@endforeach
				<a href='/presse' class="button"> VOIR PLUS D'ACTUALITES</a>
			</ul>
		</div>
		<div id="calendar">
			<h1> Agenda </h1>
			<ul>
				@foreach($agendas as $agenda) 
					<div class="table">
						<li><span class="date">{{$agenda->jour_debut}} {{$agenda->mois_debut}} {{$agenda->année}}</span><br/><span class="postTitle">{{$agenda->title}}</span><span class="plusLink">+</span></li>
					</div>
				@endforeach
				<a href='/presse' class="button"> VOIR TOUT L'AGENDA</a>
			</ul>
		</div>
	</section>
	<section id="chiffre-cles">
		<h1> En quelques chiffres </h1>
		<div id="cube-wrapper">
			<div class="cube rose">
				<div class="cubeContent">
					<p><span class="cubeTitleWhite"> 10</span></p><br/><p><span class="text"> secteur d'activité </span></p>
				</div>
			</div>
			<div class="cube blanc">
				<div class="cubeContent">
					<p><span class="cubeTitleAzur">1600</span></p><br/><p><span class="text"> salariés dans toute la France </span></p>
				</div>
			</div>
			<img id="image-façade" src="../uploads/ressources/cube-bastide.png">
			<div class="cube noir">
				<div class="cubeContent">
					<p><span class="cubeTitleWhite"> +53%</span></p><br/><p><span class="text"> de croissance sur les  dernières années</span></p>
				</div>
			</div>
			<div class="cube gris-moyen">
				<div class="cubeContent">
					<p><span class="cubeTitleRose"> 40</span></p><br/><p><span class="text"> années d'expériences dans le secteur medical </span></p>
				</div>
			</div>
			<div class="cube bleu-azur">
				<div class="cubeContent">
					<p><span class="cubeTitleWhite"> 10 000m²</span></p><br/><p><span class="text"> répartis sur deux plateformes logistique en France </span></p>
				</div>
			</div>
			<div class="cube gris-clair">
				<div class="cubeContent">
					<p><span class="cubeTitleRose"> 120</span></p><br/><p><span class="text"> agences réparties sur le territoire national </span></p>
				</div>
			</div>
			<img id="image-senior" src="../uploads/ressources/cube-senior.jpg">
		</div>
	</section>
<script type="text/javascript">
	function getPostId(id){
      $("#post_id").val(id);
  }

	$(document).ready(function(){
 		var $liCollection = $("#red h2");
		var i = 0
		// Cache the first list item for later use
		var $firstListItem = $liCollection.first();
		$('#red h2').on('click',function(){
    		$liCollection.removeClass('color2');
    		var redText = $(this).text();
	    	if(redText =="| DE VOTRE SANTE "){
	    		$('#red').css('background-color', "#026dcf");
	    		i = 1;
	    	/*	$('.red0').hide();
    			$('.red2').hide()
    			$('.red1').show()*/
	    	} else if (redText == "| DEPUIS 40 ANS "){
	    		$('#red').css('background-color', '#68358C')
	    		i = 2;
	    		 	$('.red0').hide();
    				$('.red1').hide()
    				$('.red2').show()
	    	} else if(redText == '| PARTENAIRE') {
	    		$('#red').css('background-color' , '#D31466');
	    		i = 0;
	    				$('.red1').hide();
    			$('.red2').hide()
    			$('.red0').show()
	    	}
	    	$(this).addClass('color2');
	    });
    	// Give the first list item the active state
    	$liCollection.first().addClass("color2");
    	// Each 500 ms
    	setInterval(function() {
    		i++
    		if( i == 1 ) {
    			$('#red').animate({backgroundColor: '#026dcf'}, 'slow');
    			$('.red0').hide();
    			$('.red2').hide()
    			$('.red1').show()

    		} else if( i== 2 ) {
    			$('#red').animate({backgroundColor: '#68358C'}, 'slow');
    			  $('.red0').hide();
    				$('.red1').hide()
    				$('.red2').show()
    			
    		} else {
    			$('#red').animate({backgroundColor: '#D31466'}, 'slow');
    			$('.red1').hide();
    			$('.red2').hide()
    			$('.red0').show()
    		}
        	// Get the active list item
        	var $activeListItem = $(".color2")
        	// Remove its active state
        	$activeListItem.removeClass("color2", 900, "easeInOutQuad");
        	// Try to find the next list item
        	var $nextListItem = $activeListItem.closest('#red h2').next();
        	// If the next list item (jQuery object) length property is 0
        	// (this means that this list item was the last in the list)
        	if ($nextListItem.length == 0) {
        		i = 0;
            	// The next list item is actually the first list item
            	$nextListItem = $firstListItem;
        	}
        	$nextListItem.addClass("color2", 1000, "easeInOutQuad");
    	}, 3000);
	});
</script>
@stop