@extends('layout.app')
@section('title', 'afficher un article')
@section('sidebar')
@endsection
@section('content')
	@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif
	@if ($message = Session::get('error'))
    <div class="alert alert-danger alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif
  
  <div id="banniere-article" data-id="{{$posts->filename}}">
  </div>
  <div id ="article-container">
    <div id="white-box-article"> 
      <div id="article-title">
        <h1> {{$posts->title}}</h1>
        <p class="date"> paru le {{Carbon\Carbon::parse($posts->created_at)->format('d/m/Y')}} dans <a href="/presse"> Actualité </a> </p>
      </div>
      <div id="article-content">
        <p>{!! str_replace("\n","<br>", $posts->content) !!}</p>
      </div>
    </div>
      <div id='similarPost-container'> 
        <h1> Articles similaires</h1>

        @foreach($similarPosts as $similarPost)
          <div class="similar-article">
              <div class="similar-article-title">
                  <h2> {{$similarPost->title}}</h2>
                  <p class="date"> paru le {{Carbon\Carbon::parse($similarPost->created_at)->format('d/m/Y')}} dans <a href="/presse"> Actualité </a></p>
              </div>
            <div class="overlay">
              <div class="overlay-back">
                <p> + </p>
              </div>
              <div class="similarPost-image" data-id="{{$similarPost->filename}}">
              </div>
            </div>
          </div>
        @endforeach
   
  </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script type="text/javascript">

  var imgLink = $('#banniere-article').attr('data-id');
  var similarImage = $('.similarPost-image').attr('data-id');
  $('#banniere-article').css('background-image', 'url(/uploads/images/'+imgLink);
  $('.similarPost-image').css('background-image', 'url(/uploads/images/'+similarImage);
    $('.overlay-back').hide();
  </script>

  <script>

    $('.similar-article').mouseenter(function(e) {
         $(this).find('.overlay-back').show(300);
         $(this).delay(300).addClass('actif');
    });

    $('.similar-article').mouseleave(function(e) {
      $(this).find('.overlay-back').hide(500);
      $(this).animate({opacity: 1}, 150, function() {
        $(this).toggleClass("actif")
      });    
    });

  </script>
@endsection


