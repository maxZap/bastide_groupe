<!-- search box container starts  -->
    <div class="search">
        <p>&nbsp;</p>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="input-group">
                    <span class="input-group-addon" style="color: white; background-color: rgb(124,77,255);">BLOG SEARCH</span>
                    <input type="text" autocomplete="off" id="search" class="form-control input-lg" placeholder="Enter Blog Title Here">
                </div>
            </div>
        </div>   
    </div>  
<!-- search box container ends  -->
<div id="txtHint" class="title-color" style="padding-top:50px; text-align:center;" ><b></b></div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#search").keyup(function(){
    var str=  $("#search").val();
    if(str == "") { 
      $( "#txtHint" ).html(' '); 
    }else {
      $.get( "{{ url('search/livesearch?id=') }}"+str, function( data ) {

        $( "#txtHint" ).html( data );  
      });
    }
  });  
}); 
</script>


