<?php
if(!empty($posts)) { 
  $count = 1;
  $outputhead = '';
  $outputbody = '';  
  $outputtail ='';
  $outputbodycate = '';
  $outputhead .= '<h2> Articles </h2>';
  
  foreach ($posts as $post) {   
    $body = substr(strip_tags($post->content),0,50)."...";
    $show = url('/'.$post->categorie->slug. '/' .$post->subcategorie->slug. '/'. $post->slug);
    $outputbody .=  ' 
      '.$count++.'
      <a href="'.$show.'" target="_blank" title="SHOW" >'.$post->title.'</a>
      '.$body.'
      '.$post->categorie->name.'<br/>';           
  } 

  $outputtail .= '</tbody>
                </table>
              </div>';        
  echo $outputhead; 
  echo $outputbody;
  echo $outputbodycate;
  echo $outputtail; 
} 


if(!$categories->isEmpty()) {   
  $count = 1;
  $outputhead = '';
  $outputbody = '';  
  $outputtail ='';
  $outputbodycate = '';
  if($categories[0]->id !='') {
    $outputhead .= '<h2>Categories</h2>';
  }    

  foreach ($categories as $categorie) {    
  $body = substr(strip_tags($categorie->content),0,50)."...";
  $show = url('/'.$categorie->slug);
  $outputbody .=  '
    '.$count++.'
    '.$categorie->name.'
    '.$body.'
    '.$categorie->name.'
    '.$categorie->created_at.'<br/>';          
  } 

  $outputtail .= '</tbody>
                </table>
              </div>';
  echo $outputhead; 
  echo $outputbody;
  echo $outputbodycate;
  echo $outputtail; 
}  

if(!$subcategories->isEmpty()) { 
  $count = 1;
  $outputhead = '';
  $outputbody = '';  
  $outputtail ='';
  $outputbodycate = '';
  if($subcategories[0]->id !='') {
    $outputhead .= '<h2>Subcategories</h2>';
  } 

  foreach ($subcategories as $subcategorie)    {   
    $show = url('/'.$subcategorie->slug);
    $outputbody .= 
    ' <tr> 
        <td>'.$count++.'</td>
        <td>'.$subcategorie->nom.'</td>
        <td>'.$subcategorie->categorie->name.'</td>
        <td>'.$subcategorie->created_at.'</td><br/>
    </tr> ';    
  }  

  $outputtail .= '</tbody>
                </table>
              </div>';   
  echo $outputhead; 
  echo $outputbody;
  echo $outputbodycate;
  echo $outputtail; 
} 

if(!$files->isEmpty()) { 
  $count = 1;
  $outputhead = '';
  $outputbody = '';  
  $outputtail ='';
  $outputbodycate = '';
  if($files[0]->id != ''){
    $outputhead .= '<h2>Fichier PDF</h2>';
  } 

  foreach ($files as $file)    {      
  $show = URL::asset('/uploads/pdf/'.$file->filename);
  $outputbody .= ' 
    <tr> 
        <td>'.$count++.'</td>
         <td><a href="'.$show.'" target="_blank" title="SHOW" target="_blank" type="application/octet-stream">'.$file->filename.'</a></td>
        <td>'.$file->title.'</td>
        <td>'.$file->folder->folder_id.'</td>
        <td>'.$file->created_at.'</td><br/>
    </tr>';         
  }  
  $outputtail .= '</tbody>
                </table>
              </div>';
  echo $outputhead; 
  echo $outputbody;
  echo $outputbodycate;
  echo $outputtail; 
} 
 
else {  
  echo 'Data Not Found';  
} 
?>  