@extends('layout.app')
@section('title', 'article')
@endsection
@section('header')	
<div id="banniere-orthopedie">
	<div class="text-banniere">
		@foreach($activites as $activite) 
			<h1> {{strtoupper($activite->titre)}}</h1>
		@endforeach
	</div>
</div>
@endsection
@section('content')
 	@foreach($activites as $activite)
 	<div id="content-wrapper">
 		<div id="content-container">
 			<div class="content-text">
 					{!! $activite->content !!}

 			</div>
 			<div class="content-files">
 				<div id="plaquette-et-contact">
 					<div class="left">
 						<img id="plaquette" src="/uploads/ressources/plaquette.png"><br/>
 						<a href='{{$activite->plaquette}}' target="_blank"> PLAQUETTE </a>
 					</div>
 					<div class="right">
 						</a><img id="contact" src="/uploads/ressources/contact.png"><br/>
 						<a href='osef'> CONTACT </a>
 					</div>
 				</div>
 				<div id="liste-agence">
 					<img id="plaquette" src="/uploads/ressources/carte.png">
 					<p> TROUVER UNE AGENCE PRES DE CHEZ VOUS : </p>
 					<div id="map-content-wrapper">
 						<form>
 							<input type="text" name="agence_field" id="agence_field" placeholder="NÎMES, 30000....">
 						</form>
 						<a> LISTE DES AGENCES </a>
 					</div>
 				</div>
 			</div>
 		</div>
	</div>
 	@endforeach 
@endsection