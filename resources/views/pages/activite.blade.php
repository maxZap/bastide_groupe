@extends('layout.app')
@section('title', 'article')
@endsection
@section('header')	
<div id="banniere-activite">
	<h1> ------ Nos Activités ------ </h1>
</div>
@endsection
@section('content')
 	<div id="activite-content-wrapper">
 		<div id="content-container-mad">
 			<div class="title-border">
 				<h2><span class="titleBlue">BASTIDE GROUPE,</span><br> UN ACTEUR MAJEUR DU SECTEUR MEDICAL</h2>
 			</div>
 			<p>Livraison adapatées aux besoins réels du patient. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
 			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
 			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
 			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
 			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

 			<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
 			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
 			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p>
 		</div>
 		<div id="activite-container">
 			<div class="title-activite-container">
 				<div class="title-border">
 					<h2><span class="titleBlue">DECOUVREZ</span> NOS ACTIVITES EN DETAIL</h2>
 				</div>
 			</div>
 		
 			<div class="activite-mad" id="mad">
 				<p>MAINTIEN <br/> A DOMICILE</p> 
 				<img src="uploads/ressources/activiteFleche.png">
 			</div>
 			<div class="activite-diabete" id="assist-respi">
 				<p>ASSISTANCE <br/> RESPIRATOIRE</p> 
 				<img src="uploads/ressources/activiteFleche.png">
 			</div>
 			 <div class="activite-mad" id="ortho">
 				<p>ORTHOPEDIE</p> 
 				<img src="uploads/ressources/activiteFleche.png">
 			</div>
 			<div class="activite-diabete" id="nutrition">
 				<p>NUTRITION</p> 
 				<img src="uploads/ressources/activiteFleche.png">
 			</div>
 			 <div class="activite-mad" id="cicat">
 				<p>CICATRISTATION <br/> UROLOGIE <br/> STOMATOTHERAPIE</p> 
 				<img src="uploads/ressources/activiteFleche.png">
 			</div>
 			<div class="activite-diabete" id="diabete">
 				<p>DIABETE</p> 
 				<img src="uploads/ressources/activiteFleche.png">
 			</div>
 			 <div class="activite-mad" id="collectivite">
 				<p>COLLECTIVITE</p> 
 				<img src="uploads/ressources/activiteFleche.png">
 			</div>
 			<div class="activite-diabete" id="escarre">
 				<p>ASSISTANCE ESCARRE</p> 
 				<img src="uploads/ressources/activiteFleche.png">
 			</div>
 			<div class="activite-mad" id="ehpad">
 				<p>PRESTATION <br/> EN EHPAD</p> 
 				<img src="uploads/ressources/activiteFleche.png">
 			</div>
 			<div class="activite-diabete" id="perfusion">
 				<p>PERFUSION</p> 
 				<img src="uploads/ressources/activiteFleche.png">
 			</div>
 		</div>
	</div>



	<div class="cubeWrapper">
 		<h3> En savoir plus sur ... </h3>
 			<div class="cube">
 				<a href="/historique">
 					<h4> L'HISTORIQUE </h4>
 					<p><span class="plusCube">+</span></p>
 				</a>
 			</div>
 			<div class="cube">
 				<a href ='/activités'>
 					<h4> LES ACTIVITES </h4>
 					<p><span class="plusCube">+</span></p>
 				</a>
 			</div>
 			<div class="cube">
 				<a href="/govuernance">
 				<h4> LA GOUVERNANCE </h4>
 				<p><span class="plusCube">+</span></p>
 			</div>
 			<div class="cube">
 				<a hre='/finance'>
 					<h4> LES CHIFFRES CLES </h4>
 					<p><span class="plusCube">+</span></p>
 				</a>
 			</div>
 		</div>
@endsection