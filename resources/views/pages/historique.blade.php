@extends('layout.app')
@section('title', 'article')
@endsection
@section('header')	
<div id="banniere-activite">
	<h1> ------ l'Historique ------ </h1>
</div>
@endsection
@section('content')
 	<div id="content-wrapper">
 		<div id="historique-content-container">
 			<div class="content-text">
 				<div class="title-border">
 					<h2><span class="titleBlue">BASTIDE MEDICAL,</span><br> UNE AVENTURE FAMILLIALE</h2>
 				</div>
 				<img src ="../uploads/ressources/historique.jpg" id="historique-image">
 				<div class="bastide-historique-first">
	 				<p>Au gré de ses expériences dans l'activté du maintiens a domicile, Bastide le confort medical a su indentifier des besoins auprès de ses patients et a souhaité enrichir sa préstation en apportant	 une offre complémentaire avec des prodtuis d'orthopedie et de contention</p>
	 				<p> a ce titre, Bastide le confort medical a adapté des points de vente afin d'accueilir les patients dans une cadre professionnel et dans le respect de leur intimité.</p>
	 			</div>
	 			<div id="date-historique">
	 				<div class="title-border">
		 				<h2><span class="titleBlue">DATES</span> CLES</h2>
		 			</div>
		 			<div class="left">
			 			@foreach($dates as $date)
			 				@if($date->date[0] == 1) 
				 				<h3>{{$date->date}}</h3>
				 				<p>{{$date->description}}</p>
				 				<img src="uploads/ressources/dots.svg" id="svgtest">
				 			@endif
			 			@endforeach
		 			</div>
		 			<div class="right">
			 			@foreach($dates as $date)
			 				@if($date->date[0] == 2)
			 					<div class="date-wrapper-right"> 
				 					<h3>{{$date->date}}</h3>
				 					<p>{{$date->description}}</p>
				 				</div>
				 				<img src="uploads/ressources/dots.svg" id="svgtest">
				 			@endif
			 			@endforeach
		 			</div>
	 			</div>
	 		</div>
 		</div>
	</div>
	<div class="cubeWrapper">
 		<h3> En savoir plus sur ... </h3>
 			<div class="cube">
 				<a href="/valeurs">
	 				<h4> LES VALEURS </h4>
	 				<p><span class="plusCube">+</span></p>
 				</a>
 			</div>
 			<div class="cube">
 				<a href="/activité">
	 				<h4> LES ACTIVITES </h4>
	 				<p><span class="plusCube">+</span></p>
	 			</a>
 			</div>
 			<div class="cube">
 				<a href="/gouvernance">
	 				<h4> LA GOUVERNANCE </h4>
	 				<p><span class="plusCube">+</span></p>
	 			</a>
 			</div>
 			<div class="cube">
 				<a href="/finance">
	 				<h4> LES CHIFFRES CLES </h4>
	 				<p><span class="plusCube">+</span></p>
	 			</a>
 			</div>
 		</div>
@endsection