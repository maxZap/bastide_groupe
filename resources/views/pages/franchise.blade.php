@extends('layout.app')
@section('title', 'article')
@endsection
@section('header')	
<div id="banniere-franchise">
	<h1> LA FRANCHISE BASTIDE <span class="boldTitle"><br>LE CONFORT MEDICAL</span> </h1>
</div>
<div class="onglet-container">
	<ul id="onglets-partenariat">
		<li class="actif">PRESENTATION</li>
		<li>DEVENEZ FRANCHISE</li>
		<li >LE RESEAU BASTIDE</li>
 	</ul>
</div>
@endsection
@section('content')
	<div id="franchise-container">
	<div id ="onglet-contenu">
		<div class="item">
			<div class="box-franchise">
				<img src="../uploads/ressources/franchise.jpg" class="imgEntete">
				<div class="box-title" id="devenir-franchise">
					<h2>Ouvrez une franchise de matériel médical</h2><span class="arrowDown"><img src="uploads/ressources/chevron_vtl.svg"></span>
				</div>
				<div class="box-content devenir-franchise">
					<div class="white-box-container content-devenir-franchise">
	 				<p><span id="boldText"> 
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non</span></p>
						<p>proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br>
					</p>
					</div>
				</div>
			</div>
			<div class="box-franchise">
				<img src="../uploads/ressources/agence-nimes.jpg" class="imgEntete">
				<div class="box-title" id="leader-maintiens">
					<h2> Ouvrez une franchise de matériel médical <span class="arrowDown"></h2><span class="arrowDown"><img src="uploads/ressources/chevron_vtl.svg"></span>
				</div>
				<div class="box-content leader-maintiens">
					<div class="white-box-container content-leader-maintiens">
	 				<p> 
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id ="onglet-contenu">
		<div class="item">
			<div class="left">
				<div class="box-franchise">
					<img src="../uploads/ressources/franchise.jpg" class="imgEntete">
					<div class="box-title" id="action">
						<h2>Ouvrez une franchise de matériel médical</h2><span class="arrowDown"><img src="uploads/ressources/chevron_vtl.svg"></span>
					</div>
					<div class="box-content action">
						<div class="white-box-container">
		 				<p><span id="boldText"> Créez votre activité sur un marché porteur a fort potentiel : </span></p>
							<ul>
								<li> Réalisez votre projet avec l'assistance de l'équipe de Bastide Franchise</li>
								<li> Bénéficiez de la formation BASTIDE le confort médical </li>
								<li> Beneficiez du savoir faire BASTIDE le confort médical et de la notoriété de l'enseigne</li>
								<ul>
									<li>formation au démarrage </li>
									<li> Formation annuelle de mise à jour et de perfection </li>
								</ul>
								<li> Bénéficiez du marketing BASTIDE le confort médical </li>
								<ul>
									<li> Produits publicitaire a marque </li>
									<li> Animation permanentes grâce aux catalogues</li>
									<li> Visite régulière et conseil d'un animateur du réseau produits en marque propre</li>
								</ul>
						</div>
					</div>
				</div>
				<div class="box-franchise">
					<img src="../uploads/ressources/franchise.jpg" class="imgEntete">
					<div class="box-title" id="action">
						<h2>Deux concepts de franchise</h2><span class="arrowDown"><img src="uploads/ressources/chevron_vtl.svg"></span>
					</div>
					<div class="box-content action">
						<div class="white-box-container">
		 				<p> 
		 					Nous vous proposons deux concepts de franchise pour la vente et la location de matériel médical. un concept local et un concept magasin, le premier est basé sur un local d'entreposage et une forte présence terrain, auprès des réseaux de prescripteur ( hôpitaux, cliniques, association d'aide à domicile..) Le deuxieme est un concept basé sur un point de vente doublé d'une action terrain auprès de prescripteurs  
						</p>
						</div>
					</div>
				</div>
			</div>
			<div class="right">
				<div class="box-franchise">
					<img src="../uploads/ressources/franchise.jpg" class="imgEntete">
					<div class="box-title" id="action">
						<h2>Devenez leader du maintien a domicile</h2><span class="arrowDown"><img src="uploads/ressources/chevron_vtl.svg"></span>
					</div>
					<div class="box-content action">
						<div class="white-box-container">
		 				<p><span id="boldText"> 
							Profitez de la stratégie claire et efficace du leader du maintien a domicile depuis 1977. Bastide Le Confort Médical :</span>
						</p>
							<ul> 
								<li> Un réseau de plus de 100 magasins avec un objectif de 200 a moyen terme</li>
								<li> Un marketing innovant </li>
								<li> Des produits à marques propre adaptés au marché </li>
								<li> Des plateformes logistiques performantes réparties sur le térritoire national </li>
								<li> Des support étudiés et évolutif </li>
							</ul>
						</div>
					</div>
				</div>
				<div class="box-franchise">
					<img src="../uploads/ressources/agence-nimes.jpg" class="imgEntete">
					<div class="box-title" id="avis">
						<h2> Construisons ensemble votre projet de franchise <span class="arrowDown"></h2><span class="arrowDown"><img src="uploads/ressources/chevron_vtl.svg"></span>
					</div>
					<div class="box-content avis">
						<div class="white-box-container">
		 				<p> 
							Aujourd'hui pour créer seul son affaire il faudrait être : Agent immobilier, juriste, architect financier, comptable. Créer son entreprise est une chose complexe nous reunissons les meilleurs compétences pour vous aider a ouvrir une franchise spécialisée dans la vente et la location de matériel médical
						</p>
						<p> 
							Notre complétmentarité et notre apport sont pour vous un gage de reussite Bastide le confort médical met a votre disposition une équipe qui allie son savoir faire et mettra tout en oeuvre pour créer, avec professionnalisme, la franchise adaptée a vos clients</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div >
@stop
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
	
	$(function() {

	$('#onglets-partenariat').css('display', 'block');
	
	$('#onglets-partenariat').click(function(event) {

		var actuel = event.target;
		
		if (!/li/i.test(actuel.nodeName) || actuel.className.indexOf('actif') > -1) {
			return;
		}
		$(actuel).addClass('actif').siblings().removeClass('actif');
		setDisplay();
	});

	$('box-content').hide();
	$('.box-title').click(function(e){
		var id = this.id,
		 wasOpen = $(this).next('.box-content').hasClass('clicked');
		 e.preventDefault();
		 $('.box-content').removeClass('clicked').slideUp();;
		 if (!wasOpen) {
			$('.box-content.' + id).addClass('clicked').slideDown();
    	}
	});

	function setDisplay() {
		var modeAffichage;
		$('#onglets-partenariat li').each(function(rang) {
			modeAffichage = $(this).hasClass('actif') ? '' : 'none';
			$('.item').eq(rang).css('display', modeAffichage);
		});
	}
	setDisplay();
});
</script>