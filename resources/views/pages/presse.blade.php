@extends('layout.app')
@section('title', 'article')
@endsection
@section('header')  
<div id="banniere-presse">
  <h1> ESPACE <span class="boldTitle">PRESSE</span></h1>
</div>
<div class="onglets-container" >
  <ul id="onglets-presse" ontouchstart="">
    <li class="actif" ontouchstart="">ACTUALITES</li>
    <li ontouchstart="">MEDIATHEQUE</li>
    <li ontouchstart="">COMMUNICATION</li>
    <li ontouchstart="">AGENDA</li>
  </ul>
</div>
@endsection
@section('content')

<div id="thumbnail">
  <div id="thumbnail-content">
      <img id="bigImg" src='' alt="" width="500px">
      <div id="thumbnail-content-wrapper">
        <p id="dateCaption"> </p>
        <p id="textCaption"> </p>
      </div>
  </div>
    <img src="../uploads/ressources/shareicon.svg" width='90px' id="shareicon">
    <img src="../uploads/ressources/dlicon.svg" width='90px' id="dlicon">
</div>

  <div id ="onglet-contenu">
    <div class="item">
    <div id="filterSearch">
      <form>
        <label for ="date">date </label>
        <input type="text" name="date">
      </form>
      <p> Mot clés: </p>
    </div>
    @foreach($posts as $post)
      <div class="box-presse">
        <div class="news-container">
          <div class="white-box-container">
            <p class="date">{{Carbon\Carbon::parse($post->created_at)->format('d/m/Y')}}</p>
            <h2>{{$post->title}}</h2>
            <div class="news-content">
              <img src="../uploads/images/{{$post->filename}}">
              <p> {{$post->description}} [...] </p> <span class="plusLink">+</span>
            </div>
          </div>
        </div>
      </div>
      @endforeach
      <div id="Pagination">
        <?php echo $posts->render()?>
      </div>
    </div>
  </div>
  <div id ="onglet-contenu">
    <div class="item">
      <div class="box">
        <div class="mediatheque-container">
          <form>
            <select name="osefa" id="filterMedia" class="salut">
              <option value="0" disbled> Tout </option>
              <option value="2"> vidéo </option>
              <option value="3"> images </option>
              <option value="4"> PDF </option>
            </select>
            <br>
            <label for="filterMosaique"> Affichage </label>
           <select name="list-Mosaique" id="FilterMosaique">
              <option value="2">Mosaique</option>
              <option value="1">Liste</option>
            </select>
          </form>
          <p> Mot clé : </p>
          <div id="white-media-container-list">
            @foreach($fileUploads as $fileUpload)
            <div class="searchFilterResults">
              <img src="../uploads/ressources/{{$fileUpload->picture_filename}}">
              <p> {{$fileUpload->title}} </p>
              </div>
            @endforeach
          </div>
          <div id="media-container-mosaique">
                @foreach($fileUploads as $fileUpload)
                  @if($fileUpload->type != 3)
                    <div class="mediatheque-container-data">
                  @else
                    <div class="mediatheque-container-data images">
                  @endif 
                  @if(!empty($fileUpload->lien_youtube) AND $fileUpload->lien_youtube != NULL AND $fileUpload->type == 2)
                    <iframe width="800" height="400" src="{{ $fileUpload->lien_youtube }}" frameborder="0" allowfullscreen></iframe>
                  @elseif(!empty($fileUpload->filename) AND $fileUpload->filename != NULL AND $fileUpload->type == 3)
                  <div id="imgWrapper">
                    <img src="../uploads/images/{{ $fileUpload->filename }}" class="Enlargeimages"  id="{{$fileUpload->id}}" onclick="enlarge(this.src, this.id);">
                    <p class="date" id='imgP{{$fileUpload->id}}'>{{$fileUpload->created_at->format('d/m/Y')}}</p>
                    <figcaption class="caption" id="imgCaption{{$fileUpload->id}}">{{$fileUpload->title}}</figcaption>
                  </div>
                  @else 
                    <img src="../uploads/images/pdfskin.jpg" class="pdfImage">
                  @endif
                </div>
                @endforeach 
            </div>
          <div id="media-button"><a>CHARGER PLUS DE CONTENU</a></div>
          <div id="media-buttonless"><a>VOIR MOINS</a></div>
        </div>  
      </div>
    </div>
  </div>
  <div id ="onglet-contenu">
    <div class="item">
      <div class="box">
        <div class="box-content-finance">
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 
          </p>  
        </div>  
      </div>
    </div>
  </div>
   <div id ="onglet-contenu">
    <div class="item">
      <div class="box">
      @foreach($agendas as $agenda)
        <div class="agenda-container" id="{{$agenda->ordre}}">
          <h2> Du {{ $agenda->jour_debut }} {{str_limit($agenda->mois_debut, $limit=3, $end='')}} au {{$agenda->jour_fin}} {{str_limit($agenda->mois_fin, $limit=3, $end="")}} {{$agenda->année}}  <br/> {{$agenda->adresse}},  {{$agenda->ville}}</h2> <a type="button" data-toggle="modal" data-target="#modal{{$agenda->id}}" href="#collapse1"><span class="plusLink" href="#collapse1">+</span></a>
          <p> {{$agenda->title}} </p> 
        </div>  
      @endforeach
      <div id="button"><a>CHARGER PLUS DE CONTENU</a></div>
      <div id="buttonless"><a>VOIR MOINS</a></div>
      </div>
    </div>
  </div>
  @foreach($agendas as $agenda)
    <div class="modal hide fade" id="modal{{$agenda->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <p class="modal-title" id="myModalLabel"> Du {{ $agenda->jour_debut }} {{str_limit($agenda->mois_debut, $limit=3, $end='')}} au {{$agenda->jour_fin}} {{str_limit($agenda->mois_fin, $limit=3, $end="")}} {{$agenda->année}}  <br/> {{str_replace(' ', '+',$agenda->adresse)}}, {{$agenda->ville}}</p>
            <h2> {{$agenda->title}}</h2>
          </div>
          <div class="modal-body">
            <div class="left">
              <p>{{$agenda->description}}</p>
              <p> Le Salon est ouvert de {{$agenda->horaire_debut}}h à {{$agenda->horaire_fin}}h non-stop. <br/> Retrouvez-nous sur le stand N°{{$agenda->numero_stand}}</p>
              <p>+ d'info sur : <br> <a > {{$agenda->site_internet}}</a></p>
            </div>
            <div class="right">
              <a href="https://www.google.com/maps/place/{{str_replace(' ', '+',$agenda->adresse)}}+{{$agenda->ville}}/"><img id ="firstImage" src="../uploads/ressources/localicon.svg"></a><br>
              <a><img src="../uploads/ressources/agenda.svg"></a><br>
              <a><img src="../uploads/ressources/mailicon.svg"></a><br>
              <a><img src="../uploads/ressources/shareicon.svg"></a><br>
            </div>
          </div>
        </div>
      </div>
    </div>
  @endforeach
    <div class="cubeWrapper">
    <h3> En savoir plus sur ... </h3>
      <div class="cube">
        <a href="/partenariat">
          <h4> NOS PARTENAIRES </h4>
          <p><span class="plusCube">+</span></p>
        </a>
      </div>
      <div class="cube">
        <a href="/activité">
          <h4> LES ACTIVITES </h4>
          <p><span class="plusCube">+</span></p>
        </a>
      </div>
      <div class="cube">
        <a href="/gouvernance">
          <h4> LA GOUVERNANCE </h4>
          <p><span class="plusCube">+</span></p>
        </a>
      </div>
      <div class="cube">
        <a href="finance">
          <h4> LES CHIFFRES CLES </h4>
          <p><span class="plusCube">+</span></p>
        </a>
      </div>
    </div>
@stop
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>


  function enlarge(src, id){
    $('#pull').hide();
    $('html').css('overflow-y', 'hidden');
    $('#thumbnail').fadeIn(200)
    $('#thumbnail-content').fadeIn(200);
    $('#bigImg').attr('src', src);
    
    getImageData(id);
   
    return false;
  }


function getImageData(id) {
  dateImg = $('#imgP'+id).text();
  textImg = $('#imgCaption'+id).text();
  $('#dateCaption').html(dateImg);
  $('#textCaption').html(textImg);
  

}


  $(function() {
    $('#onglets-presse').css('display', 'block');
    $('#onglets-presse').click(function(event) {
      var actuel = event.target;
      if (!/li/i.test(actuel.nodeName) || actuel.className.indexOf('actif') > -1) {
        return;
      }
      $(actuel).addClass('actif').siblings().removeClass('actif');
      setDisplay();
    });

    $('#thumbnail-content').click(function(){
      $('#pull').show();
      $('body, html').css('overflow-y', 'scroll');
      $('#thumbnail').fadeOut(200)
      $('#thumbnail-content').fadeOut(200); 
    });

    $('box-content').hide();
    $('.box-title').click(function(e){
      var id = this.id,
       wasOpen = $(this).next('.box-content').hasClass('clicked');
       e.preventDefault();
       $('.box-content').removeClass('clicked').slideUp();;
       if (!wasOpen) {
        $('.box-content.' + id).addClass('clicked').slideDown();
        }
    });

    $(".agenda-container").slice(0, 2).show();
    $("#button").on('click', function (e) {
        e.preventDefault();
        $(".agenda-container:hidden").slice(0, 2).slideDown();
        if ($(".agenda-container:hidden").length == 0) {
            $("#button").css('display', 'none');
            $('#buttonless').css('display','block');
        }
    });

    $("#buttonless").on('click', function(e) {
      div = $('.agenda-container:visible');
      $('.agenda-container').not('#1, #2').hide(500);
      if ($(".agenda-container:hidden").length == 0) {
         $("#buttonless").css('display', 'none');
         $('#button').css('display','block');
      }
    });

   
    $("#media-button").on('click', function (e) {
        e.preventDefault();
        $(".searchFilterResults:hidden").slice(0, 2).slideDown(800)
        if ($(".searchFilterResults:hidden").length == 0) {
          $("#media-button").css('display', 'none');
          $('#media-buttonless').css('display','block');
        }
    });

    $("#media-buttonless").on('click', function(e) {
      $('.searchFilterResults').slice(2).hide(500);
      if ($(".searchFilterResults:hidden").length == 0) {
         $("#media-buttonless").css('display', 'none');
         $('#media-button').css('display','block');
      }
    });

    $("#media-button").on('click', function (e) {
        e.preventDefault();
        $(".mediatheque-container-data:hidden").slice(0, 2).slideDown(800, function(){
          $(".images").css('display', 'inline');
        });
        if ($(".mediatheque-container-data:hidden").length == 0) {
          $("#media-button").css('display', 'none');
          $('#media-buttonless').css('display','block');
        }
    });

    $("#media-buttonless").on('click', function(e) {
      div = $('.mediatheque-container-data:visible');
      $('.mediatheque-container-data').not(':first').hide(500);
      if ($(".mediatheque-container-data:hidden").length == 0) {
         $("#media-buttonless").css('display', 'none');
         $('#media-button').css('display','block');
      }
    });



    function setDisplay() {
      var modeAffichage;
      $('#onglets-presse li').each(function(rang) {
        modeAffichage = $(this).hasClass('actif') ? '' : 'none';
        $('.item').eq(rang).css('display', modeAffichage);
      });
    }
    setDisplay();


    $("#filterMedia").change(function(){
      var value = $(this).val();// Value of selected option
      var Text =  $(this).find('option:selected').text();;// Text of selected option
      $.get('/mediaFilter/'+value , function(html){
        document.getElementById('white-media-container-list').innerHTML = html;
        console.log(html);
        $('#media-button').css('display', 'block');
        $('#media-buttonless').css('display', 'none');
      });
    });

    $("#filterMedia").change(function(){
      var value = $(this).val();// Value of selected option
      var Text =  $(this).find('option:selected').text();;// Text of selected option
      $.get('/mosaiqueFilter/'+value,function(html){
        $('#media-button').css('display', 'block');
        $('#media-buttonless').css('display', 'none');
        document.getElementById('media-container-mosaique').innerHTML = html;
        if(value =="3") {
         $('.image-searchFilterResults').slice(0,2).show();
        }
      });
    });

    $('#FilterMosaique').change(function(){
      var value = $(this).val();
      if(value == 2) {
        $('#white-media-container-list').hide(650);
        $('#media-container-mosaique').show(650);
        $('#media-button').css('display', 'block');
        $('#media-buttonless').css('display', 'none');
      }

      if(value==1) { 
        $('#media-container-mosaique').hide(650)
        $('#white-media-container-list').show(650);
             $('#media-button').css('display', 'block');
        $('#media-buttonless').css('display', 'none');
      }
    });
  });
</script>
