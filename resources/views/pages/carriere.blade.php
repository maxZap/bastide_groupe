@extends('layout.app')
@section('title', 'article')
@endsection
@section('header')	
<div id="banniere-carriere">
	<h1>Carriere </h1>
</div>
@endsection
@section('content')
	<div id="carriere-container">
	 	<div id="carriere-content-wrapper">
	 		<div id="carriere-container">
	 			<div class="carriere-gauche" id="magasinier">
	 				<p>MAGASINIER </p> 
	 				<img src="uploads/ressources/activiteFleche.png">
	 			</div>
	 			<div class="carriere-droite" id="secretaire">
	 				<p>SECRETAIRE ADMINISTRATIF</p> 
	 				<img src="uploads/ressources/activiteFleche.png">
	 			</div>
	 			 <div class="carriere-gauche" id="livreur">
	 				<p>LIVREUR </p> 
	 				<img src="uploads/ressources/activiteFleche.png">
	 			</div>
	 			<div class="carriere-droite" id="technicien">
	 				<p>TECHNICIEN CONSEIL</p> 
	 				<img src="uploads/ressources/activiteFleche.png">
	 			</div>
	 			 <div class="carriere-gauche" id="respAgence">
	 				<p>RESPONSABLE D'AGENCE</p> 
	 				<img src="uploads/ressources/activiteFleche.png">
	 			</div>
	 			<div class="carriere-droite" id="infiermier">
	 				<p>INFIERMIER CONSEIL</p> 
	 				<img src="uploads/ressources/activiteFleche.png">
	 			</div>
	 		</div>
		</div>
	</div>
	<div class="cubeWrapper">
 		<h3> En savoir plus sur ... </h3>
 			<div class="cube">
 				<a href="/valeurs"> 
 				<h4> LES VALEURS </h4>
 				<p><span class="plusCube">+</span></p>
 			</div>
 			<div class="cube">
 				<h4> LES ACTIVITES </h4>
 				<p><span class="plusCube">+</span></p>
 			</div>
 			<div class="cube">
 				<h4> LA GOUVERNANCE </h4>
 				<p><span class="plusCube">+</span></p>
 			</div>
 			<div class="cube">
 				<h4> LES CHIFFRES CLES </h4>
 				<p><span class="plusCube">+</span></p>
 			</div>
 		</div>
@endsection