@extends('layout.app')
@section('title', 'article')
@endsection
@section('header')	
<div id="banniere-finance">
	<h1> ESPACE <span class="boldTitle">FINANCE</span> </h1>
</div>
<div class="onglet-container">
	<ul id="onglets">
		<li class="actif">ACTIONNARIAT</li>
		<li>RAPPORTS</li>
	</ul>
</div>
@endsection
@section('content')
	<div id ="onglet-contenu">
		<div class="item">
			<div class="box">
				<div class="box-title" id="action">
					<h2> L'Action Bastide  <span class="arrowDown"></h2><span class="arrowDown"><img src="uploads/ressources/chevron_vtl.svg"></span>
				</div>
				<div class="box-content action">
					<div class="white-box-container content-action">
					<img src="../uploads/ressources/euronext.jpg">
	 				<p><span id="boldText"> 
						Profitez de la stratégie claire et efficace du leader du maintien à domicile depuis 1977, Bastide le Confort Médical</span></p>
						<p>Unréseau de plus de 100 magasins avec un objectif de 200 a moyen terme , Un marketing innovent, des produits a marques propres adaptés au marché, Des platforme logistique performante répartisseur, le territoire national Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud ex<br>
					</p>
					<a href='osef' class="button"> Suivre le cours de l'action Bastide Groupe</a>
					</div>
				</div>
			</div>
			<div class="box">
				<div class="box-title" id="avis">
					<h2> Avis Financier <span class="arrowDown"></h2><span class="arrowDown"><img src="uploads/ressources/chevron_vtl.svg"></span>
				</div>
				<div class="box-content avis">
					<div class="white-box-container">
	 					<ul> 
	 						<li> Bastide bien orienté après le nouveau financement</li>
	 						<li> Bastide : Nouveau crédit syndiqué de 155 M€</li>
	 						<li> Bastide : Gilbert Dupont passe positif </li>
	 						<li> Départ du vendée globe 2017 </li>
	 						<li>les prestations de services deviennent majoritaire dans le chiffre </li>
	 					</ul>
					<a href='osef' class="button"> consulter tout les avis</a>
					</div>
				</div>
			</div>
			<div class="box">
				<div class="box-title" id="franchise">
					<h2> Les Franchises</h2><span class="arrowDown"><img src="uploads/ressources/chevron_vtl.svg"></span>
				</div>
				<div class="box-content franchise">
					<div class="white-box-container">
	 				<p> 
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					</p>
					<p id="fleche_down"><a href="osef" id="dlIcon"><img src="uploads/ressources/dlicon.svg"></a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id ="onglet-contenu">
		<div class="item">
			<div class="box">
				<div class="box-content-finance">
					@foreach($files as $file)
	 					<p> 
							<a id="downloadLink" href="{{URL::asset('/uploads/pdf/'.$file->filename)}}" target="_blank" type="application/octet-stream" ><img src="{{URL::asset('/uploads/images/pdfskin.jpg')}}" height="50" width="50"> {{$file->title}}</a>
						</p>
					@endforeach
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	
					</p>		
				</div>	
			</div>
		</div>
	</div>
@stop
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
	
	$(function() {
	
	$('#onglets').css('display', 'block');
	$('#onglets').click(function(event) {
		var actuel = event.target;
		if (!/li/i.test(actuel.nodeName) || actuel.className.indexOf('actif') > -1) {
			return;
		}
		$(actuel).addClass('actif').siblings().removeClass('actif');
		setDisplay();
	});

	$('box-content').hide();
	$('.box-title').click(function(e){
		var id = this.id,
		 wasOpen = $(this).next('.box-content').hasClass('clicked');
		 e.preventDefault();
		 $('.box-content').removeClass('clicked').slideUp();;
		 if (!wasOpen) {
			$('.box-content.' + id).addClass('clicked').slideDown();
    	}
	});

	function setDisplay() {
		var modeAffichage;
		$('#onglets li').each(function(rang) {
			modeAffichage = $(this).hasClass('actif') ? '' : 'none';
			$('.item').eq(rang).css('display', modeAffichage);
		});
	}
	setDisplay();
});
</script>