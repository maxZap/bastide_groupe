@extends('layout.app')
@section('title', 'article')
@endsection
@section('header')	
<div id="banniere-activite">
	<h1> ------ NOS VALEURS ------ </h1>
</div>
@endsection
@section('content')
 	<div id="content-wrapper">
 		@foreach($valeurs as $valeurs)
 		@if($valeurs->active == 1)
 		<div class="box-container">

 			<div class="title-border">
 				<h2>{{$valeurs->titre}}</h2>
 			</div>
 			<img src="uploads/ressources/{{$valeurs->filename}}">
 			<p> {{$valeurs->description}} </p>
 		</div>
 		@endif
		@endforeach
	</div>
 	<div class="cubeWrapper">
 		<h3> En savoir plus sur ... </h3>
 			<div class="cube">
 				<a href="/historique">
	 				<h4> L'HISTORIQUE </h4>
	 				<p><span class="plusCube">+</span></p>
	 			</a>
	 			</a>
 			</div>
 			<div class="cube">
 				<a href="/activité">
 				<h4> LES ACTIVITES </h4>
 				<p><span class="plusCube">+</span></p>
 			</div>
 			<div class="cube">
 				<a href="/gouvernance">
 					<h4> LA GOUVERNANCE </h4>
 					<p><span class="plusCube">+</span></p>
 				</a>
 			</div>
 			<div class=;"cube">
 				<a href="/finance">
 					<h4> LES CHIFFRES CLES </h4>
 					<p><span class="plusCube">+</span></p>
 				</a>
 			</div>
 		</div>

@endsection