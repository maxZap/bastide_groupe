@extends('layout.app')
@section('title', 'article')
@endsection
@section('header')	
<div id="background">
	<div id="blueBackground">
		<div id="banniere-partenariat">
			<h1> ------ Partenariat ------ </h1>
		</div>
		@endsection
		@section('content')
 			<div id="content-wrapper-partenariat">
 				<div class="white-box">
 					<div class="white-box-title">
 						<h2><span class="redTitle">BASTIDE DANS LA COURSE,<br/></span>POUR LA TRANSAT JACQUES VABRE 2017</h2>
 					</div>
 					<div class="white-box-content">
			 			<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			 			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			 			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			 			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			 			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			 			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.S</p>
			 			<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			 			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			 			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			 			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			 			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			 			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
 					</div>
 				</div>
 				<div class="white-box" id="equipage-bastide-desktop">
 					<div class="white-box-title">
 						<h2><span class="redTitle">L'EQUIPAGE,</span><br/> DU BASTIDE-OTIO</h2>
 					</div>
 					<div class="white-box-content">
 						<div id="kito-informations">
 							<div class="kito-informations-dektop" > 
				 				<img src="../uploads/ressources/kito_photo.png">
				 				<p><span class="boldTitle">Kito DE PAVANT </span><br> 56 ans, skipper IMOCA60'</p>
				 			</div>
			 			</div>
 					</div>
				 	<div class="presentation-kito-desktop">
				 		<div class="contenu-presentation-kito">
			 				<p> Homme souriant et chaleureux, Kito est aussi chaleureux que le Sud où il a grandi. Enfant, c'est entre deux sorties a cheval que Kito découvre la voile sur l'étange de Milhac àbord de l'Optimist
			 				construit par son père. La vie du marin se construit autour des voyages et des rencontres</p>
			 				<p> <span class="redPres"> 50 transatlantique parmi lesquelles:</span><br> 8 solitaire du Figaro, 7 transat Jacques Vabre, 2 Routes du Rhum, 2 Participation au Vendée Globe 5 transats AG2R ( dont une victoire en 2006)</p>
			 				<p> <span class="redPres"> Le mot de Kito : </span> "J'esper vréman keyora pa 2 cachalo 7 foi ptdlol"</span></p>
			 			</div>
				 	</div>
		 			<div class="kito-informations-dektop">
		 				<img src="../uploads/ressources/yannick.png">
		 				<p><span class="boldTitle"> Yannick BESTAVEN </span><br> 56 ans, skipper IMOCA60'</p>
		 			</div>
				 	<div class="presentation-kito-desktop">
				 		<div class="contenu-presentation-kito">
			 				<p> Homme souriant et chaleureux, Kito est aussi chaleureux que le Sud où il a grandi. Enfant, c'est entre deux sorties a cheval que Kito découvre la voile sur l'étange de Milhac àbord de l'Optimist
			 				construit par son père. La vie du marin se construit autour des voyages et des rencontres</p>
			 				<p> <span class="redPres"> 50 transatlantique parmi lesquelles:</span><br> 8 solitaire du Figaro, 7 transat Jacques Vabre, 2 Routes du Rhum, 2 Participation au Vendée Globe 5 transats AG2R ( dont une victoire en 2006)</p>
			 				<p> <span class="redPres"> Le mot de Kito : </span> "J'esper vréman keyora pa 2 cachalo 7 foi ptdlol"</span></p>
			 			</div>
				 	</div>
 				</div>
 				<!--<div class="white-box" id ="equipage-bastide">
 					<div class="white-box-title">
 						<h2><span class="redTitle">L'EQUIPAGE,</span><br/> DU BASTIDE-OTIO</h2>
 					</div>
 					<div class="white-box-content">
 						<div id="kito-informations-wrapper">
 							<div class="kito-informations kito actif" > 
				 				<img src="../uploads/ressources/kito_photo.png">
				 				<p><span class="boldTitle">Kito DE PAVANT </span><br> 56 ans, skipper IMOCA60'</p>
				 			</div>
				 			<div class="kito-informations yannick">
				 				<img src="../uploads/ressources/yannick.png">
				 				<p><span class="boldTitle"> Yannick BESTAVEN </span><br> 56 ans, skipper IMOCA60'</p>
				 			</div>
			 			</div>
 					</div>
				 	<div class="presentation-kito">
				 		<div class="contenu-presentation-kito">
			 				<p> Homme souriant et chaleureux, Kito est aussi chaleureux que le Sud où il a grandi. Enfant, c'est entre deux sorties a cheval que Kito découvre la voile sur l'étange de Milhac àbord de l'Optimist
			 				construit par son père. La vie du marin se construit autour des voyages et des rencontres</p>
			 				<p> <span class="redPres"> 50 transatlantique parmi lesquelles:</span><br> 8 solitaire du Figaro, 7 transat Jacques Vabre, 2 Routes du Rhum, 2 Participation au Vendée Globe 5 transats AG2R ( dont une victoire en 2006)</p>
			 				<p> <span class="redPres"> Le mot de Kito : </span> "J'esper vréman keyora pa 2 cachalo 7 foi ptdlol"</span></p>
			 			</div>
				 	</div>
				 	<div class="presentation-yannick">
				 		<div class="contenu-presentation-kito">
			 				<p> Homme souriant et chaleureux, Kito est aussi chaleureux que le Sud où il a grandi. Enfant, c'est entre deux sorties a cheval que Kito découvre la voile sur l'étange de Milhac àbord de l'Optimist
			 				construit par son père. La vie du marin se construit autour des voyages et des rencontres</p>
			 				<p> <span class="redPres"> 50 transatlantique parmi lesquelles:</span><br> 8 solitaire du Figaro, 7 transat Jacques Vabre, 2 Routes du Rhum, 2 Participation au Vendée Globe 5 transats AG2R ( dont une victoire en 2006)</p>
			 				<p> <span class="redPres"> Le mot de Kito : </span> "J'esper vréman keyora pa 2 cachalo 7 foi ptdlol"</span></p>
			 			</div>
				 	</div>
 				</div>-->
 				<div class="white-box">
 					<div class="white-box-title">
 						<h2><span class="redTitle">BASTIDE DANS LA COURSE,<br/></span>POUR LA TRANSAT JACQUES VABRE 2017</h2>
 					</div>
 					<div class="white-box-content">
			 			<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			 			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			 			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			 			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			 			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			 			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.S</p>
			 			<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			 			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			 			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			 			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			 			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			 			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
 					</div>
 				</div>
			</div>
			
		</div>
	<div id="notre-bateau">
		<div id="partenariat-kito-container">
			<h2><span class="redTitle">NOTRE BATEAU</span> <br> LE BASTIDE-OTIO </h2>
			<ul> 
				<li>Modèle : IMOCA 60'</li>
				<li> Hauteur du m$at : 29 mètres</li>
				<li> Profondeur de la quille : 4,5 mètres </li>	
				<li> Largeur: 5,85 mètres</li>
				<li> Poid a vide : 8,2 tonnes </li>
				<li> Vitesse de pointe : 55kM/H</li>
				<li> Longueur de coque maxomale : 60 pieds(18,28m)</li>
			</ul>
			<div id="gallery-kito">
				@foreach($fileUploads as $fileUpload)
		            @if($fileUpload->type === 2)
		            	<iframe width="800" height="500" src="{{ $fileUpload->lien_youtube }}" frameborder="0" allowfullscreen></iframe>
		           	@elseif($fileUpload->type === 3)
		          		<div class="imgWrapper">
		            		<img src="../uploads/images/{{ $fileUpload->filename }}">
		          		</div>
		          @endif
		        @endforeach
		         <div id="partenariat-button"><a>CHARGER PLUS DE CONTENU</a></div>
	          	<div id="partenariat-buttonless"><a>VOIR MOINS</a></div>
	       	</div>
		<div id="bastide-et-kito">
			 <h2><span class="redTitle">BASTIDE ET KITO </span> <br> DANS L'ACTUALITE </h2>
			 <div class="white-box">
				<div class="white-box-title">
					 <p class="date"> {{Carbon\Carbon::parse($posts->created_at)->format('d/m/Y')}}</p>
					<h3>{{$posts->title}}</h3>
				</div>
				<div class="white-box-content">
					<img src="../uploads/images/{{$posts->filename}}" class="postImage">
					<p> {{$posts->description}} [...]</p>
					<p class="showmoreicon"><a type="button" data-toggle="modal" data-target="#modal{{$posts->id}}" href="#collapse1"> +</a> <p>
		 		</div>

			</div>
	 		<div id="Bast-kito-button"><a>VOIR PLUS D'ACTUALITE</a></div>
	      	<div id="Bast-kito-buttonless"><a>LE DIRECT SUR TWITTER</a></div>


	    <div class="modal hide fade" id="modal{{$posts->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	      <div class="modal-dialog">
	        <div class="modal-content">
	          <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	            <p class="modal-title" id="myModalLabel"> {{Carbon\Carbon::parse($posts->created_at)->format('d/m/Y')}} <br/> 
	            <h2> {{$posts->title}}</h2>
	          </div>
	          <div class="modal-body">
	            <img src='../uploads/images/{{$posts->filename}}'>
	              <p>{{$posts->content}}</p>
	             	
	          </div>
	        </div>
	      </div>
	    </div>
	</div>
	</div>  
	</div>
	@stop

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
     
$(document).ready(function(){
	
	$("#partenariat-button").on('click', function (e) {
    	e.preventDefault();
    	$(".imgWrapper:hidden").slice(0, 1).slideDown(500, function() {
    		$('.imgWrapper:visible').css('display', 'inline-block');
    	});
    	
    	if ($(".imgWrapper:hidden").length == 0) {
      		$("#partenariat-button").css('display', 'none');
      		$('#partenariat-buttonless').css('display','block');
    	}
	});

	$("#partenariat-buttonless").on('click', function(e) {
		$('.imgWrapper:visible').slice().slideUp(500);
		if ($(".imgWrapper:hidden").length == 0) {
 			$("#partenariat-buttonless").css('display', 'none');
 			$('#partenariat-button').css('display','block');
		}
	});

	if($('.kito').hasClass('actif')){
		$('.presentation-kito').show();
	} 


	$('.kito-informations').on('click', function(e){
		if($(this).hasClass('actif')){ return;}
			e.preventDefault();
			$('.actif').removeClass('actif');
			$(this).addClass('actif')
			$(".kito-informations").insertAfter('.actif')
		
		if($('.kito').hasClass('actif')) {
			$('.presentation-yannick').fadeOut()
			$('.presentation-kito').delay(400).fadeIn();
		}

		if($('.yannick').hasClass('actif')){
			$('.presentation-kito').fadeOut()
			$('.presentation-yannick').delay(400).fadeIn();
		}
	});
});


       
</script>