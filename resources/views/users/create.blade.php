@extends('layout.app')
@section('title', 'inscription')
@section('sidebar')
@endsection
@section('content')
	<h1> Create user </h1>
	{!! Form::open(['method' => 'POST', 'url'=>'users/inscription'], array('url' => 'users')) !!}
		<div class="form-group"> 
			{!! Form::label('nom', 'Nom') !!}
			{!! Form::text('nom', Input::old('nom')) !!}
		</div>
		<div class="form-group"> 
			{!! Form::label('prenom', 'Prenom') !!}
			{!! Form::text('prenom', Input::old('prenom'), array('class' => 'form-control')) !!}
		</div>
		<div class="form-group"> 
			{!! Form::label('email', 'Email') !!}
			{!! Form::text('email', Input::old('email'), array('class' => 'form-control')) !!}
		</div>
		<div class="form-group"> 
			{!! Form::label('password', 'Password') !!}
			{!! Form::password('password', Input::old('password')) !!}
		</div>
		{!! Form::submit('valider', array('class'=>'btn')) !!}
	{!! Form::close() !!}
@endsection
</body>
</html>