@extends('layout.app')
@section('title', 'Profil')
@section('sidebar')
@endsection
@section('content')
	<div class="titrePage">
		<h1> Profil de l'utilisateur  {{ $users['email']}}  </h1>
	</div>
	<p> ID : {{ $users['id'] }}</p>
	<p> NOM : {{ $users['nom'] }}</p>
	<p> PRENOM : {{ $users['prenom'] }}</p>
	<p> EMAIL : {{ $users['email'] }}</p>
	<p> Date de création : {{ $users['created_at'] }}</p>
	<p> Dernière modification : {{ $users['updated_at'] }}</p>
	@if(Auth::id() == $users['id'] || Auth::user()->admin >= 1 )
		<a class="btn btn-small btn-info" href="{{ URL::to('users/edit/' . $users['id'] ) }}">Modifié mon profil</a>
	@endif
@endsection