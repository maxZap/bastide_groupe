@extends('layout.app')
@section('title', 'connexion')
@section('sidebar')
@endsection
@section('content')
 @if($message = Session::get('error'))
      <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
    @endif
	{!! Form::open(['method' => 'POST', 'url'=>'users/connexion'], array('url' => 'users')) !!}
		<div class="form-group"> 
			{!! Form::label('email', 'email') !!}
			{!! Form::text('email', Input::old('email')) !!}
		</div>
		<div class="form-group"> 
			{!! Form::label('password', 'password') !!}
			{!! Form::password('password', Input::old('password'), array('class' => 'form-control')) !!}
		</div>
		{!! Form::submit('connexion', array('class'=>'btn')) !!}
	{!! Form::close() !!}
@endsection