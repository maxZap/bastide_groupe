@extends('layout.app')

@section('title', 'user')

@section('sidebar')
@endsection

@section('content')
	<h2> Modifier votre profil </h2>
	{!! Form::open(['route' => ['users.update', Auth::id()], 'method' => 'patch']) !!}
		<div class="form-group">
			{!! Form::label('email', 'Email') !!}
			{!! Form::text('email', Input::old('email'), array('class' => 'form-control')) !!}
		</div>
		<div class="form-group">
			{!! Form::label('password', 'password') !!}
			{!! Form::password('password', Input::old('password'), array('class' => 'form-control')) !!}
		</div>
		{!! Form::submit('modifié', array('class'=>'btn')) !!}
	{!! Form::close() !!}
@endsection