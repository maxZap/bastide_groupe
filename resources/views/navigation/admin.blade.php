<div id="fixedTop-admin">
	<div id="fixedTop-menu"><h2>Bastide Groupe <span id="left-menu">{{Auth::user()->email}} / <a href ="#">messages</a> / Deconnexion </span></h2></div>
	<div id="fixedTop-burger">
		<a href="/" class="admin-button"> retour au site</a> 
		<div class="hamburger hamburger--emphatic"  aria-label="Menu" aria-controls='navigation' id ="burgerAdmin">
			<div class="hamburger-box">
				<div class="hamburger-inner"></div>
			</div>
		</div>
	</div>

	<nav class="adminNavbar">
		<div class="admin-navbar-wrapper">
			<div class="left">
				<div id="valeurs-wrapper" class="icone">
					<img src="/uploads/ressources/users.svg">
					<a href="/admin/valeurs"> Valeurs </a>
				</div>
				<div id="user-wrapper" class="icone">
					<img src="/uploads/ressources/users.svg">
					<a href="/admin/users"> Utilisateurs </a>
				</div>
				<div id="comment-wrapper" class="icone">
					<img src="/uploads/ressources/comments.svg">
					<a href="/admin/historique"> Historique </a>
				</div>
			</div>
			<div class="middle">
				<div id="accueil-wrapper" class="icone">
					<img src="/uploads/ressources/home.svg">
					<a href="/admin">accueil</a>
				</div>
				<div id="posts-wrapper" class="icone">
					<img src="/uploads/ressources/posts.svg">
					<a href="/admin/posts"> Articles</a>
				</div>
				<div id="files-wrapper" class="icone">
					<img src="/uploads/ressources/pdf.svg">
					<a href="/admin/files">PDF</a>
				</div>
			</div>

			<div class="right">
				<div id="categorie-wrapper" class="icone">
					<img src="/uploads/ressources/categories.svg">
					<a href="/admin/categories">Categories</a>
				</div>
				
				<div id="folder-wrapper" class="icone">
					<img src="/uploads/ressources/dossiers.svg">
					<a href="/admin/agenda"> Agenda </a>
				</div>
			</div>
		</div>
	</nav>


	<nav class="adminNavbar-desktop">
		<a href="/" class="admin-button"> retour au site</a>
		<ul> 
			<li><img src="/uploads/ressources/home.svg" > <a href="/admin">Accueil  </a></li>
			<li><img src="/uploads/ressources/posts.svg" > <a href="/admin/posts"> Articles </a> </li>
			<li><img src="/uploads/ressources/users.svg" ><a href="/admin/users"> Utilisateurs </a></li>
			<li><img src="/uploads/ressources/dossiers.svg" ><a href="/admin/agenda"> Agenda </a> </li>
			<li><img src="/uploads/ressources/comments.svg" > <a href="/admin/historique"> Histoire </li>
			<li><img src='/uploads/ressources/categories.svg'><a href="/admin/categories"> Categories </a> </li>
			<li><img src="/uploads/ressources/pdf.svg" ><a href="/admin/files"> Fichier </a></li>
		</ul>
	</nav>
</div>
