<div id="footer-Wrapper">
	<div id="newsletter">
		<h2>INSCRIVEZ VOUS A NOTRE NEWSLETTER :</h2><br/>
		<form> 
		<input type="text" placeholder="ENTREZ VOTRE ADRESSE MAIL">
		</form>
	</div>
	<div id="lien-utile">
		<div class="left"> 
			<h2> LIENS UTILES : </h2>
			<ul>
				<li>Bastide le Confort Medical </li>
				<li>Le Blog Conseil Bastide </li>
				<li>Plateforme Bastide Access</li>
				<li>Bastide Recrutement</li>
			</ul>
		</div>
		<div class="right">
			<div id ="reseaux-sociaux">
				<a href ="https://www.facebook.com/BastideConseil/"><img src="/uploads/ressources/facebook.png"></a>
				<a href="https://twitter.com/BastideMedical"/><img src="/uploads/ressources/twitter.png"></a>
			</div>
			<ul>
				<li>Presse</li>
				<li>Franchise</li>
				<li>Notre réseau</li>
			</ul>
		</div>
	</div>
</div>