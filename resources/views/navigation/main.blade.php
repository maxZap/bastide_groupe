<nav class="mainNavbar">
	<div class="fixedTop">
		<a href="/"><img src="/uploads/ressources/logo.png" id="logoPhone"></a> <div class="hamburger hamburger--emphatic"  aria-label="Menu" aria-controls='navigation' id="BurgerPhone">
		<div class="hamburger-box">
			<div class="hamburger-inner"></div>
		</div>
	</div>
	<ul>
		<li id="nous-connaître" class="mainMenu"><span class="categoryName">Nous <span class="boldTitle">connaître</span></span></li>
		<div class="submenu nous-connaître">
			<ul>
				<li><a class="submenu-nous-connaître" href='http://localhost:8000/valeurs'>Valeurs<span class="plusLink">+</span></a></li>
				<li><a class="submenu-nous-connaître" href='http://localhost:8000/gouvernance'>Gouvernance<span class="plusLink">+</span></a></li>
				<li><a class="submenu-nous-connaître" href='http://localhost:8000/historique'>Histoire<span class="plusLink">+</span></a></li>
			</ul>
		</div>
		<li id="nos-activités" class="mainMenu"><span class="categoryName">Nos <span class="boldTitle">Activités</span></span></li>
		<div class="submenu nos-activités">
			<ul>	
				<li><a class="submenu-nos-activités" href='http://localhost:8000/orthopedie'>Orthopédie<span class="plusLink">+</span></li></a>
				<li><a class="submenu-nos-activités" href='http://localhost:8000/activites'>Activités<span class="plusLink">+</span></li></a>
			</ul>
		</div>
		<li id="notre-communication" class="mainMenu"><span class="categoryName">Notre <span class="boldTitle">communication</span></span></li>
		<div class="submenu notre-communication">
			<ul>
				<li><a class="submenu-notre-communication" href='http://localhost:8000/presse'>Presse<span class="plusLink">+</span></li></a>
				<li><a class="submenu-notre-communication" href='http://localhost:8000/finance'>Nos chiffres<span class="plusLink">+</span></li></a>
				<li><a class="submenu-notre-communication" href='http://localhost:8000/valeurs'>Les liens utiles<span class="plusLink">+</span></li></a>
			</ul>
		</div>
		<li id="nous-rejoindre" class="mainMenu"><span class="categoryName">Nous <span class="boldTitle">rejoindre</span></span></li>
		<div class="submenu nous-rejoindre">
			<ul>
				<li><a class="submenu-nous-rejoindre" href='http://localhost:8000/partenariat'>Nos partenaires<span class="plusLink">+</span></li></a>
				<li><a class="submenu-nous-rejoindre" href='http://localhost:8000/franchise'>Nos franchises<span class="plusLink">+</span></li></a>
				<li><a class="submenu-nous-rejoindre" href='http://localhost:8000/carriere'>Nos carrière<span class="plusLink">+</span></li></a>
			</ul>
		</div>
	</ul>
	<div id="submenu-desktop">
			<div id="submenu-nous-connaître" class="submenu-container">
				<div class="left">
					<div class="submenu-link-wrapper-first">
						<a href='http://localhost:8000/historique'>HISTORIQUE<span class="plusLink">+</span></a>
						<p>Découvrez l'histoire de Bastide Le Confort Médical depuis sa création en 1977 par Guy Bastide jusqu'à nos jours </p>
					</div>
					<div class="submenu-link-wrapper-second">
						<a href='http://localhost:8000/valeurs'>VALEURS & ENGAGEMENTS<span class="plusLink">+</span></a> 
						<p> La culture Bastide se définie par un socle de valeurs fortes qui se retrouvent à tout les degrés de notre entreprise</p>
					</div>
				</div>
				<div class="right">
					<div class="submenu-link-wrapper-third">
						<a href='http://localhost:8000/gouvernance'>GOUVERNANCE<span class="plusLink">+</span></a>
						<p> Dirigeant et conseil d'administration </p>
					</div>
					<div class="submenu-link-wrapper-fourth">
						<a href='http://localhost:8000/finance'>NOS CHIFFRES<span class="plusLink">+</span></a>
						<p> Decouvrez nos chiffre clés de l'aventure Bastide </p>
					</div>
				</div>
			</div>
		<div id="submenu-nous-rejoindre" class="submenu-container">
			<div class="left">
				<div class="submenu-link-wrapper-first">
					<a href='http://localhost:8000/carriere'>CARRIERE<span class="plusLink">+</span></a>
					<ul> 
						<li> PRESENTATION </li>
						<li> NOS OFFRES </li>
						<li> NOS METIERS </li>
					</ul>
				</div>
				<div class="submenu-link-wrapper-second">
					<a href='http://localhost:8000/partenariat'>PARTENARIAT<span class="plusLink">+</span></a>
					<p> Apprenez en plus sur les partenariats établis par le groupe bastide au fils des années </p>
				</div>
			</div>
			<div class="right">
				<div class="submenu-link-wrapper-third">
					<a href='http://localhost:8000/franchise'>FRANCHISE<span class="plusLink">+</span></a>
					<p> Decouvrez le fonctionnement de nos franchise et rejoignez vous aussi l'aventure Bastide ! </p>
					<ul> 
						<li>PRESENTATION </li>
						<li> LE RESEAU </li>
						<li> DEVENEZ FRANCHiSER </li>
					</ul>
				</div>
			</div>
		</div>
		<div id="submenu-notre-communication" class="submenu-container">
			<div class="left">
				<div class="submenu-link-wrapper-first">
					<a href='http://localhost:8000/presse'>ACTUALITE<span class="plusLink">+</span></a>
					<p> Consulter les articles concernant l'actualité du groupe Bastide</p>
				</div>
				<div class="submenu-link-wrapper-second">
					<a href='http://localhost:8000/presse'>AGENDA<span class="plusLink">+</span></a>
					<p>consultez tout les évènement où Bastide sera présent </p>
				</div>
			</div>
			<div class="middle">
				<div class="submenu-link-wrapper-third">
					<a href='http://localhost:8000/presse'>PRESSE<span class="plusLink">+</span></a>
					<ul> 
						<li> RESSOURCES</li>
						<li> REVUE DE PRESSE</li>
						<li> CONTACT </li>
					</ul>
				</div>
				<div class="submenu-link-wrapper-fourth">
					<a href='http://localhost:8000/valeurs'>FINANCE<span class="plusLink">+</span></a>
					<ul>
						<li> RAPPORTS </li>
						<li> AVIS FINANCIERS</li>
						<li> ACTIONNARIAT </li>
					</ul>
				</div>
			</div>
			<div class='right'>
				<div class="submenu-link-wrapper-fifth">
					<a href='http://localhost:8000/presse'>LIENS<span class="plusLink">+</span></a>
					<p> Retrouvez toutes les pages liées au groupe Bastide </p>
					<ul> 
						<li> BASTIDE LE CONFORT MEDICAL </li>
						<li> BLOG CONSEIL </li>
						<li> BASTIDE ACCESS </li>
						<li> BASTIDE RECRUTEMENT </li>
						<li> RESEAUX SOCIAUX </li>
					</ul>
				</div>
			</div>
		</div>
		<div id="submenu-nos-activités" class="submenu-container">
			<p><a href='http://localhost:8000/orthopedie'>Orthopédie<span class="plusLink">+</span></a></p>
			<p><a href='http://localhost:8000/activites'>Activités<span class="plusLink">+</span></a></p>
		</div>
	</div>
</nav>
