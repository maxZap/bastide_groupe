@extends('layout.app')
@section('title', 'rapport bastide')
@section('content')
	<table class="table table-striped">
		<thead>
			
				<tr>		
					<td><h3>{{$folders->name}}</h3></td>
				</tr>
		</thhead>
		<tbody>
			@foreach($files as $file)
				<tr>
					<td><a id="downloadLink" href="{{URL::asset('/uploads/pdf/'.$file->filename)}}" target="_blank" type="application/octet-stream" ><img src="{{URL::asset('/uploads/images/pdfskin.jpg')}}" height="50" width="50"> {{$file->title}}</a></td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection
