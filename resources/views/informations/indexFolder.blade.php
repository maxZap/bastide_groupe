@extends('layout.app')
@section('title', 'informations')
@endsection
@section('content')

	@if ($message = Session::get('error'))
    <div class="alert alert-danger alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif
	<h2> Autres informations </h2>
	<p>Vous trouverez ici toutes les informations financières réglementées, depuis les rapports financiers annuels de Bastide Médical jusqu’aux rapports d'acquisition. Utilisez les fonctions de recherche ci-dessous pour trouver les documents qui vous intéressent.</p> 
		<div class="folders">
			@foreach($folders as $folder)
				<p><img src="/uploads/images/folder.png"><br/><a href="{{$folder->categorie['slug']  .'/'. $folder->subcategorie['slug'] . '/category/' . $folder->slug}}"> {{$folder->name}} </a>({{count($folder->fileupload)}})</p>
			
			@endforeach

		</div>



@endsection


