  @extends("layout.admin")


  @section('content')

<meta name="csrf-token" content="{{ csrf_token() }}">
    @if($message = Session::get('success'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
      @elseif($message = Session::get('error'))
      <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
    @endif

    <div id="admin-categorie-container">
      <h2> Gestion des categories </h2>
      <button type="button" class="btn btn-info" data-toggle="modal" data-target="#addModal">Créer une catégorie</button>
      <table class="table">
        <thead>
          <td> ID </td>
          <td> Nom </td>
          <td> Action </td>
        </thead>
        <tbody class="tableBody">
          @foreach($data as $categorie)
            @if($categorie->active == 1) 
            <tr id="{{$categorie->id}}" class="table-row active">
              <td>{{$categorie->id}}</td>
              <td>{{$categorie->name}}</td>
              <td><button  data-toggle="modal" data-target="#editModal" onclick="fun_edit('{{$categorie->id}}')"><img src="/uploads/ressources/edit.svg"></button></td>
              {!! Form::open(['method' => 'POST', 'id' => 'formDeleteCategorie', 'action' => ['CategoriesController@destroy', $categorie->id]]) !!}
                <td><button type="submit" class="delete deleteCategorie" id="btnDeleteCategorie" data-id ="$categorie->id"><img src="/uploads/ressources/checked.svg">  </button></td>
              {!! Form::close() !!}
            </tr>
            @else 
              <tr id="{{$categorie->id}}" class="table-row inactive">
                <td>{{$categorie->id}}</td>
                <td>{{$categorie -> name}}</td>
                <td><button  data-toggle="modal" data-target="#editModal" onclick="fun_edit('{{$categorie->id}}')"><img src="/uploads/ressources/edit.svg"></button></td>
                {!! Form::open(['method' => 'POST', 'id' => 'formDeleteCategorie', 'action' => ['CategoriesController@destroy', $categorie->id]]) !!}
                  <td><button type="submit", class="delete deleteCategorie" id="btnDeleteCategorie" data-id ="$categorie->id"><img src="/uploads/ressources/unchecked.svg">  </button></td>
                {!! Form::close() !!}
              </tr>
            @endif
          @endforeach
        </tbody>
      </table>
      <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('admin/categories/')}}">
      <input type="hidden" name="hidden_delete" id="hidden_delete" value="{{url('admin/categories/delete')}}">
      <input type="hidden" name="hidden_view_sub" id="hidden_view_sub" value="{{url('admin/subcategories')}}">
      <h2> Gestion des sous-categories </h2>
      <button type="button" class="btn btn-info" data-toggle="modal" data-target="#addsubCategorieModal">Créer une sous-catégorie</button>
      <table class="table">
        <thead>
          <tr>
            <td> ID </td>
            <td> Nom </td>
            <td> Categorie </td>
            <td> action </td>
          </tr>
        </thead>
        <tbody class="tableBody">
          @foreach($subcategories as $subcategorie)
            @if($subcategorie->active == 1) 
            <tr id="{{$subcategorie->id}}" class="table-row active"> 
                <td>{{$subcategorie->id}}</td>
                <td>{{$subcategorie -> nom}}</td>
                <td>{{$subcategorie->categorie->name}}</td>
              
              <td><button  data-toggle="modal" data-target="#editsubCategorieModal" onclick="edit_sub('{{$subcategorie->id}}')"><img src="/uploads/ressources/edit.svg"></button></td>
              {!! Form::open(['method' => 'POST', 'id' => 'formDeleteSubCategorie', 'action' => ['subCategoriesController@destroy', $subcategorie->id]]) !!}
                <td><button type="submit", class="delete deleteSubCategorie" id="btnDeleteSubCategori" data-id ="$subcategorie->id"><img src="/uploads/ressources/checked.svg" class="checked">  </button></td>
              {!! Form::close() !!}
            </tr>
            @else 
              <tr id="{{$subcategorie->id}}" class="table-row inactive"> 
                <td>{{$subcategorie->id}}</td>
                <td>{{$subcategorie -> nom}}</td>
                <td>{{$subcategorie->categorie->name}}</td>
                <td><button  data-toggle="modal" data-target="#editsubCategorieModal" onclick="edit_sub('{{$subcategorie->id}}')"><img src="/uploads/ressources/edit.svg"></button></td>
                {!! Form::open(['method' => 'POST', 'id' => 'formDeleteSubCategorie', 'action' => ['subCategoriesController@destroy', $subcategorie->id]]) !!}
                  <td><button type="submit", class="delete deleteSubCategorie" id="btnDeleteSubCategori" data-id ="$subcategorie->id"><img src="/uploads/ressources/unchecked.svg" class="unchecked">  </button></td>
                {!! Form::close() !!}
            </tr>
            @endif
          @endforeach
        </tbody>
      </table>

    <!-- ADD CATEGORIE MODAL -->
    <div class="modal fade" id="addModal" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Add Record</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('admin/categories') }}" method="post">
              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <div class="form-group">
                <div class="form-group">
                  <label for="first_name">nom</label>
                  <input type="text" class="form-control" id="nom" name="nom">
                </div>     
                <button type="submit" class="btn btn-default">Submit</button>
              </div>
            </form>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div> 
    </div>

    <!-- EDIT CATEGORIE MODAL -->
    <div class="modal fade" id ="editModal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"> Modifié une categorie </h4>
            </div>
            <div class="modal-body">
              <form action="{{ url('admin/categories/update') }}" method="post">
                 <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="form-group">
                    <label for="edit_nom"> nom: </label>
                    <input type="text" class="form-control" id="edit_nom" name="edit_nom">
                  </div>
                  <button type="submit" class="btn btn-default"> enregistré les modifications</button>
                  <input type="hidden" id='edit_id' name = "edit_id">
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div> 
  

  <!--ADD SUBCATEGORIE MODAL -->
    <div class="modal fade" id="addsubCategorieModal" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">créer une sous-categorie</h4>
          </div>
          <div class="modal-body">
            <form action="{{ url('admin/subcategories') }}" method="post">
              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <div class="form-group">
                <div class="form-group">
                  <label for="subcategorie_nom">nom de la sous-categorie</label>
                  <input type="text" class="form-control" id="subcategorie_nom" name="subcategorie_nom">
                </div>  
                <div class="form-group">
                    {!!  Form::label('categorie', 'categorie')!!}
                    <select name="subcategorie_categorie" id="subcategorie_categorie" class="form-control">
                      <option value="0" disabled selected>Selectionner une categorie</option>
                      @foreach($data as $categorie)
                        <option value="{{$categorie->id}}">{{ $categorie->name }}</option>
                      @endforeach
                    </select>
                  </div>   
                <button type="submit" class="btn btn-default">Submit</button>
              </div>
            </form>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div> 
    </div>

    <!-- EDIT SUBCATEGORIE MODAL -->
    <div class="modal fade" id ="editsubCategorieModal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"> Modifié une sous-categorie </h4>
            </div>
            <div class="modal-body">
              <form action="{{ url('admin/subcategories/update') }}" method="post">
                 <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="form-group">
                    <label for="edit_nom"> nom: </label>
                    <input type="text" class="form-control" id="edit_subcategorie_nom" name="edit_subcategorie_nom">
                  </div>
                  <div class="form-group">
                    {!!  Form::label('categorie', 'categorie')!!}
                    <select name="edit_subcategorie_categorie" id="edit_subcategorie_categorie" class="form-control">
                      <option value="0" disabled selected>Selectionner une categorie</option>
                      @foreach($data as $categorie)
                        <option value="{{$categorie->id}}">{{ $categorie->name }}</option>
                      @endforeach
                    </select>
                  </div>
                  <button type="submit" class="btn btn-default"> enregistré les modifications</button>
                  <input type="hidden" id='edit_subcategorie_id' name = "edit_subcategorie_id">
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div> 
    </div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script type="text/javascript">

      $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

      $('.tableBody').children().each(function(n, i) {
         var id = this.id;
        if(id%2 == 0) {
            $('#'+id).css('background-color','#EAEAEA');
        }
      });



     function fun_edit(id) {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          $("#edit_id").val(id);
          $("#edit_nom").val(result.name);
        }
      });
    }

    function edit_sub(id) {
      var view_url = $("#hidden_view").val();
      console.log(id)
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){

          $("#edit_subcategorie_id").val(id);
          $("#edit_subcategorie_nom").val(result.nom);
          $("#edit_subcategorie_categorie").val(result.categorie_id);

        }
      });
    }
    </script>
  @endsection
</body>
</html>