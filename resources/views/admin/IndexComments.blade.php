@extends('layout.app')
@section('title', 'Commentaires')
@endsection
@include('navigation.admin')
@section('content')
 	@if($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif
  @if($message = Session::get('error'))
    <div class="alert alert-danger alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif
	<table class="table">
		<h2> Commentaires inactifs : </h2>
		<thead>
			<tr>
				<td> ID </td>
				<td> Contenue </td>
				<td> Titre de l'artice </td>
				<td> Action </td>
			</tr>
		</thead>
    @if(!empty($comments))   
		  <tbody>
			@foreach($comments as $comment)
				<tr>
					<td>{{$comment->id}}</td>
					<td>{{$comment->content}}</td>
					<td>{{$comment->post->title}}</td>
					{!! Form::open(['method' => 'DELETE', 'id' => 'formDeleteComments', 'action' => ['CommentsController@destroy', $comment->id]]) !!}
						<td>
							<a href="/admin/comments/active/{{$comment->id}}"><img src="{{URL::asset('/uploads/images/validate.png')}}" height="20" width="20"></a>
              <button type="submit" id="btnDeleteComments" data-dismiss="{{$comment->id}}" onclick="deleteComments('{{$comments[0]->post->id}}')"><img src="{{URL::asset('/uploads/images/delete.png')}}" height="20" width="20"></button>
            </td>
            {!! Form::close() !!}
            <td><a href="#" id="RespondComments" onclick="showHideForm('hiddenForm{{$comment->id}}');">Répondre</a></td>
            <td>
            	<form action="{{ url('admin/subcomments/add') }}" method="post" id="hiddenForm{{$comment->id}}" style="display: none;">
              	<textarea type="text" cols="50" rows="3" id="submitSubCommentForm" name="subComsContent"></textarea><br/>
              	<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" value="{{$comment->id}}" id="data_id" name="commentId">
                <input type="submit" value="envoyer" class="btn btn-success"/>
             </form>
           </td>
				</tr>
			@endforeach

    @else 
    <h2> il n'y a pas de commentaire inactif</h2>
    @endif
		</tbody>
	</table>
	<table class="table">
		<h2> Sous-commentaires inactifs : </h2>
		<thead>
			<tr>
				<td> ID </td>
				<td> Commentaire affilé </td>
				<td> contenu </td>
				<td> Action </td>
			</tr>
		</thead>
		<tbody>
      @if(!empty($subcomment))
      @foreach($subcomments as $subcomment)
				<tr>
					<td>{{$subcomment->id}}</td>
					<td>{{$subcomment->comment->content}}</td>
					<td>{{$subcomment->content}}</td>
					{!! Form::open(['method' => 'DELETE', 'id' => 'formDeletesubComments', 'action' => ['subCommentsController@destroy', $subcomment->id]]) !!}
						<td>
							<a href="/admin/subcomments/{{$subcomment->id}}"><img src="{{URL::asset('/uploads/images/validate.png')}}" height="50" width="50"></a>
            	<button type="submit" id="btnDeletesubComments" data-dismiss="{{$subcomment->id}}" onclick="deletesubComments('{{$subcomments[0]->id}}')"><img src="{{URL::asset('/uploads/images/delete.png')}}" height="35" width="35"></button>
          	</td>
          {!! Form::close() !!}
				</tr>
      @endforeach
      @endif
		</tbody>
	</table>
<script>
	function deleteComments(id) {
    var inputData = $('#formDeleteComments').serialize();
    var dataId =$(this).attr('data-id');
    $.ajax({
      url: "{{ url('/comments/delete') }}" + '/' + did,
     	type: 'POST',
      data: inputData,
      success: function( msg ) {
        if ( msg) {
          window.location.reload();
        }
      },
  	});
  	return false;
  }

  function deletesubComments(id) {
    var inputData = $('#formDeletesubComments').serialize();
    $.ajax({
      url: "{{ url('/subcomments/delete') }}" + '/' + did,
      type: 'POST',
      data: inputData,
      success: function( msg ) {
        if ( msg) {
          window.location.reload();
        }
     	},
  	});
  	return false;
  }

  function showHideForm(formid) {
    var _form = document.getElementById(formid);
    _form.style.display = (_form.style.display != "block") ? "block" : "none";
    return false;
  }
</script>
@endsection