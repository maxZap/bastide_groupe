@extends("layout.admin")
@section('title','utilisateurs')
@endsection
@section('content')
	<div id="admin-users-container"> 
		<h2> Utilisateurs enregistrés </h2>
		<button type="button" class="btn btn-info" data-toggle='modal' data-target="#addUserModal"> Créer un utilisateur </button>
		<table class="table">
			<thead>
				<td> Nom </td>
				<td> Prénom </td>
				<td> Email </td>
			</thead>
			<tbody class="tableBody">
				@foreach($users as $user)
					@if($user->active == 1)
						<tr id="{{$user->id}}" class="table-row active">
							<td> {{$user->email}} </td>
							<td><img src="/uploads/ressources/edit.svg" data-toggle="modal" data-target="#editUserModal"  onclick="editUsers('{{$user->id}}')"></td>
            				<td><a><img src="/uploads/ressources/voir.svg" ></a></td>
							{!! Form::open(['method' => 'POST', 'id' => 'formDeleteUser', 'action' => ['UsersController@ActiveUser', $user->id]]) !!}
								<td> <button type="submit" class="delete deleteUser" id="btnDeleteUser" data-id='{{$user->id}}'><img src="/uploads/ressources/checked.svg"> </button></td>
							{!! Form::close() !!}
						</tr>
					@else
						<tr id="{{$user->id}}" class="table-row inactive">
							<td> {{$user->email}} </td>
							<td><img src="/uploads/ressources/edit.svg" data-toggle="modal" data-target="#editUserModal"  onclick="editUsers('{{$user->id}}')"></td>
            				<td><a><img src="/uploads/ressources/voir.svg" ></a></td>
							{!! Form::open(['method' => 'POST', 'id' => 'formDeleteUser', 'action' => ['UsersController@ActiveUser', $user->id]]) !!}
								<td> <button type="submit" class="delete deleteUser" id="btnDeleteUser" data-id='{{$user->id}}'><img src="/uploads/ressources/unchecked.svg"> </button></td>
							{!! Form::close() !!}							
						</tr>
					@endif
				@endforeach
			</tbody>
		</table>
		<input type="hidden" name="hidden_view" id="hidden_user_view" value="{{url('admin/users')}}">
		<div class="modal fade" id="addUserModal" role="dialog">
      		<div class="modal-dialog">
        		<div class="modal-content">
          			<div class="modal-header">
            			<button type="button" class="close" data-dismiss="modal">&times;</button>
            			<h4 class="modal-title">Créer un utilisateur</h4>
          			</div>
          			<div class="modal-body">
            			<form action="{{ url('admin/users') }}" method="post">
            				<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
	              			<div class="form-group">
	              				<label for="nom"> nom </label>
	              				<input type="text" name="nom" id="nom" class="form-control"> 
								{!! Form::label('prenom', 'Prenom') !!}
								{!! Form::text('prenom', Input::old('prenom'), array('class' => 'form-control')) !!}
	              				<label for="email"> email </label>
	              				<input type="email" name="email" id="email" class="form-control">
	              				<label for="password"> mot de passe </label>
	              				<input type="password" name="password" id="password" class="form-control">
	              				<p>le mot de passe doit contenir entre 5 et 16 caractère </p>
	              			</div> 
			            	<button type="submit" class="btn btn-default">enregistrer</button>
		        		</form>
		        	</div>
		        	<div class="modal-footer">
		          		<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
		        	</div>
		    	</div>
			</div>
		</div>
		<div class="modal fade" id="editUserModal" role="dialog">
      		<div class="modal-dialog">	
		    	<div class="modal-content">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal">&times;</button>
		        		<h4 class="modal-title">modifier un utilisateurs</h4>
		    		</div>
		      		<div class="modal-body">
		        		<form action="{{ url('admin/users/update') }}" method="post">
		              		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
		              		<label for="edit_users_email"> email </label>
		              		<input type="email" name="edit_user_email" id='edit_user_email' class='form-control'>
		              		<label for="edit_users_password"> password</label>  
		              		<input type="password" name="edit_user_password" id="edit_user_password" class="form-control">
			    			<input type="hidden" id='edit_user_id' name ="edit_user_id">
			              	<button type="submit" class="btn btn-default">Enregistrer</button>
			    		</form>
					</div>
		        	<div class="modal-footer">
		            	<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
		        	</div>
				</div>
    		</div>
		</div>
  	</div>
	</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script>
	function editUsers(id) {
      	var view_url = $("#hidden_user_view").val();
      	console.log(id);
      	console.log(view_url);
      	$.ajax({
        	url: view_url,
        	type:"GET", 
        	data: {"id":id}, 
        	success: function(result){
        		console.log(id);
          		$("#edit_user_id").val(id);
          		$("#edit_user_email").val(result.email);
          		$("#edit_user_password").val(result.password);
        	}
      	});
    }
</script>
@stop