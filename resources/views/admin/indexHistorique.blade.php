@extends('layout.admin');
@section('title', 'Bastide Groupe')
@endsection
@section('content')

	<div id="admin-historique-container">
		<h2> Date importantes : </h2>
		<button type="button" class="btn btn-info" data-toggle='modal' data-target="#addHistoriqueModal"> Créer un évênement </button>
		<table class="table">
			<thead>
				<td> Date </td>
				<td> Description </td>
			</thead>
			<tbody class="tableBody">
				@foreach($historiques as $historique) 
					@if($historique->active === 1 )
						<tr id="{{$historique->id}}" class="table-row active">
							<td> {{$historique->date}}</td>
							<td> {{ substr($historique->description, 0, 70) . '...'}} </td>
							<td><img src="/uploads/ressources/edit.svg" data-toggle="modal" data-target="#editHistoriqueModal" onclick="editHistorique('{{$historique->id}}')"></td>
		            		<td><a type="button" data-toggle="modal" data-target="#modal{{$historique->id}}" href="#collapse1"><img src="/uploads/ressources/voir.svg" ></a></td>
		              		{!! Form::open(['method' => 'POST', 'id' => 'formDeleteArticles', 'action' => ['HistoriquesController@destroy', $historique->id]]) !!}
		                		<td><button type="submit", class="delete deleteHistorique" id="btnDeleteHistorique"><img src="/uploads/ressources/checked.svg">  </button></td>
		              		{!! Form::close() !!}
						</tr>
					@else
						<tr id="{{$historique->id}}" class="table-row inactive">
							<td> {{$historique->date}}</td>
							<td> {{substr($historique->description, 0, 70) . '...'}}</td>
							<td><img src="/uploads/ressources/edit.svg" data-toggle="modal" data-target="#editHistoriqueModal" onclick="editHistorique('{{$historique->id}}')"></td>
		            		<td><a type="button" data-toggle="modal" data-target="#modal{{$historique->id}}" href="#collapse1"><img src="/uploads/ressources/voir.svg" ></a></td>
		              		{!! Form::open(['method' => 'POST', 'id' => 'formDeleteArticles', 'action' => ['HistoriquesController@destroy', $historique->id]]) !!}
		                		<td><button type="submit", class="delete deleteHistorique" id="btnDeleteHistorique"><img src="/uploads/ressources/unchecked.svg"></button></td>
		              		{!! Form::close() !!}
						</tr>
					@endif
				@endforeach
			</tbody>
		</table>
		<input type="hidden" name="hidden_view" id="hidden_historique_view" value="{{url('admin/historique')}}">

		<div class="modal fade" id="addHistoriqueModal" role="dialog">
      		<div class="modal-dialog">
        		<div class="modal-content">
          			<div class="modal-header">
            			<button type="button" class="close" data-dismiss="modal">&times;</button>
            			<h4 class="modal-title">ajouter une date</h4>
          			</div>
          			<div class="modal-body">
            			<form action="{{ url('admin/historique') }}" method="post">
	              			<div class="form-group">
	              				<label for="date_historique"> Date </label>
	              				<input type="text" name="date_historique" id="date_historique" class="form-control">
	              				<label for='description_historique'> Description </label>
	              				<textarea type="text" name="description_historique" id="description_historique" class="form-control"></textarea>
	              			</div> 
			            	<button type="submit" class="btn btn-default">Submit</button>
		        		</form>
		        	</div>
		        	<div class="modal-footer">
		          		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        	</div>
		    	</div>
			</div>
		</div>
		<div class="modal fade" id="editHistoriqueModal" role="dialog">
      		<div class="modal-dialog">	
		    	<div class="modal-content">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal">&times;</button>
		        		<h4 class="modal-title">ajouter une date historique de l'entreprise</h4>
		    		</div>
		      		<div class="modal-body">
		        		<form action="{{ url('admin/historique/update') }}" method="post">
		              		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
		              		<label for="edit_historique_date"> date </label>
		              		<input type="text" name="edit_historique_date" id='edit_historique_date' class='form-control'>
		              		<label for="edit_historique_description"> Description</label>  
		              		<textarea name="edit_historique_description" id="edit_historique_description" class='form-control'></textarea>
			    			<input type="hidden" id='edit_historique_id' name ="edit_historique_id">
			              	<button type="submit" class="btn btn-default">Submit</button>
			    		</form>
					</div>
		        	<div class="modal-footer">
		            	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        	</div>
				</div>
    		</div>
		</div>
  	</div>
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  	<script> 
  		function editHistorique(id) {
      	var view_url = $("#hidden_historique_view").val();
      	$.ajax({
        	url: view_url,
        	type:"GET", 
        	data: {"id":id}, 
        	success: function(result){
          		$("#edit_historique_id").val(id);
          		$("#edit_historique_date").val(result.date);
          		$("#edit_historique_description").val(result.description);
        	}
      	});
    }

    $('.tableBody').children().each(function(n, i) {
        var id = this.id;
        if(id%2 == 0) {
            $('#'+id).css('background-color','#EAEAEA');
        }
    });

  	</script>
@stop