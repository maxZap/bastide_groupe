@extends('layout.admin')
@section('title', 'Bastide Groupe')
@endsection



<style> #admin-agenda-container {
	margin-top:150px;
}

</style>
@section('content')
	<div id="admin-agenda-container">
		<h2> Prochains Evênement </h2>
		<button type="button" class="btn btn-info" data-toggle="modal" data-target="#addAgendaModal">Créer un évênement</button>
		<table class="table">
			<thead>
				<td> titre </td>
				<td> Dates </td>
				<td> adresse </td>
			</thead>
			<tbody class="tableBody">
				@foreach($agendas as $agenda)
					@if($agenda->active === 1) 
						<tr id="{{$agenda->id}}" class="table-row active">
							<td> {{$agenda->title}}</td>
							<td>  Du {{$agenda->mois_debut}} au {{$agenda->mois_fin}}</td>
							<td>  {{$agenda->adresse}},  {{$agenda->ville}} </td>
							<td><img src="/uploads/ressources/edit.svg" data-toggle="modal" data-target="#editAgendaModal" onclick="editAgenda('{{$agenda->id}}')"></td>
		            		<td><a type="button" data-toggle="modal" data-target="#modal{{$agenda->id}}" href="#collapse1"><img src="/uploads/ressources/voir.svg" ></a></td>
		              		{!! Form::open(['method' => 'POST', 'id' => 'formDeleteArticles', 'action' => ['AgendasController@destroy', $agenda->id]]) !!}
		                		<td><button type="submit", class="delete deleteAgenda" id="btnDeleteAgenda"><img src="/uploads/ressources/checked.svg">  </button></td>
		              		{!! Form::close() !!}
						</tr>
					@else 
						<tr id="{{$agenda->id}}" class="table-row inactive">
							<td> {{$agenda->title}}</td>
							<td>  Du {{ $agenda->jour_debut }} {{str_limit($agenda->mois_debut, $limit=3, $end='')}} au {{$agenda->jour_fin}} {{str_limit($agenda->mois_fin, $limit=3, $end="")}} {{$agenda->année}} </td>
							<td>  {{$agenda->adresse}}, {{$agenda->ville}} </td>
							<td><img src="/uploads/ressources/edit.svg" data-toggle="modal" data-target="#editAgendaModal" onclick="editAgenda('{{$agenda->id}}')"></td>
		            		<td><a><img src="/uploads/ressources/voir.svg" ></a></td>
		              		{!! Form::open(['method' => 'POST', 'id' => 'formDeleteArticles', 'action' => ['AgendasController@destroy', $agenda->id]]) !!}
		                		<td><button type="submit", class="delete deleteAgenda" id="btnDeleteAgenda"><img src="/uploads/ressources/unchecked.svg">  </button></td>
		              		{!! Form::close() !!}
						</tr>
					@endif
				@endforeach
			</tbody>
		</table>
		<input type="hidden" name="hidden_view" id="hidden_agenda_view" value="{{url('admin/agenda/')}}">
		@foreach($agendas as $agenda)
	    	<div class="modal hide fade" id="modal{{$agenda->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	      		<div class="modal-dialog">
	        		<div class="modal-content">
	          			<div class="modal-header">
	            			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	            			<p class="modal-title" id="myModalLabel"> Du {{str_limit($agenda->mois_debut)}} au {{$agenda->mois_fin}} <br/> {{$agenda->adresse}}, {{$agenda->ville}}</p>
	            			<h2> {{$agenda->title}}</h2>
	          			</div>
	          			<div class="modal-body">
			              <p>{{$agenda->description}}</p>
			              <p> Le Salon est ouvert de {{$agenda->horaire_debut}}h à {{$agenda->horaire_fin}}h non-stop. <br/> Retrouvez-nous sur le stand N°{{$agenda->numero_stand}}</p>
			              <p>+ d'info sur : <br> <a > {{$agenda->site_internet}}</a></p>
			            </div>
			        </div>
			    </div>
			</div>
  		@endforeach
 		<div class="modal fade" id="addAgendaModal" role="dialog">
      		<div class="modal-dialog">
        		<div class="modal-content">
          			<div class="modal-header">
            			<button type="button" class="close" data-dismiss="modal">&times;</button>
            			<h4 class="modal-title">ajouter un évênement</h4>
          			</div>
          			<div class="modal-body">
            			<form action="{{ url('admin/agenda') }}" method="post">
	              			<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
	                		<div class="form-group">
	                  			<label for="titre">Titre</label>
	                  			<input type="text" class="form-control" id="titre" name="titre">
	                		</div>
	                		<div class='form-group'>
		                  		<label for='adresse'> Lieu  </label>
		                  		<input type='text' name="adresse" id="adresse" class="form-control">
		                  		<label for='ville'> Ville  </label>
		                  		<input type='text' name="ville" id="ville" class="form-control">
		                	</div>
			            	<div class="form-group">
			              		<label for ='mois_debut'> Date debut</label> 
			              		<input type="text" name="mois_debut" id="mois_debut" class="form-control">
			             	</div>
			             	<div class="form-group">
			              		<label for ='mois_fin'> Date fin</label>
			              		<input type="text" name="mois_fin" id="mois_fin" class="form-control">
			            	</div>
			            	<div class="form-group">
			              		<label for ='mois_fin'> Horaire de début</label>
			              		<input type="text" name="horaire_debut" id="horaire_debut" class="form-control">
			            	</div>
			            	<div class="form-group">
			              		<label for ='mois_fin'> Horare de fin</label>
			              		<input type="text" name="horaire_fin" id="horaire_fin" class="form-control">
			            	</div>
			            	<div class="form-group">
			              		<label for ='mois_fin'> Numero du stand</label>
			              		<input type="text" name="numero_stand" id="numero_stand" class="form-control">
			            	</div>
			            	<div class="form-group">
			              		<label for ='mois_fin'> description</label>
			              		<textarea name="description" id="description" class="form-control"></textarea>
			            	</div>
			            	<div class="form-group">
			              		<label for ='mois_fin'> site internet</label>
			              		<input type="text" name="site_internet" id="site_internet" class="form-control">
			            	</div>                
			            	<button type="submit" class="btn btn-default">Submit</button>
		        		</form>
		        	</div>
		        	<div class="modal-footer">
		          		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        	</div>
		    	</div>
			</div>
		</div>
	 
    	<div class="modal fade" id="editAgendaModal" role="dialog">
      		<div class="modal-dialog">	
		    	<div class="modal-content">
		      		<div class="modal-header">
		        		<button type="button" class="close" data-dismiss="modal">&times;</button>
		        		<h4 class="modal-title">ajouter un évênement</h4>
		    		</div>
		      		<div class="modal-body">
		        		<form action="{{ url('admin/agenda/update') }}" method="post">
		              		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
			                <div class="form-group">
			                  	<label for="titre">Titre</label>
			                  	<input type="text" class="form-control" id="edit_agenda_titre" name="edit_agenda_titre">
			                </div>
			                <div class='form-group'>
			                  	<label for='adresse'> Lieu  </label>
			                  	<input type='text' name="edit_agenda_adresse" id="edit_agenda_adresse" class="form-control">
			                  	<label for='ville'> Ville  </label>
			                  	<input type='text' name="edit_agenda_ville" id="edit_agenda_ville" class="form-control">
			                </div>
			                <div class="form-group">
			                  	<label for ='mois_debut'> Date debut</label> 
			                  	<input type="text" name="edit_agenda_mois_debut" id="edit_agenda_mois_debut" class="form-control">
			                 </div>
			                 <div class="form-group">
			                  	<label for ='mois_fin'> Date fin</label>
			                  	<input type="text" name="edit_agenda_mois_fin" id="edit_agenda_mois_fin" class="form-control">
			                </div>
			                <div class="form-group">
			                  	<label for ='mois_fin'> Horaire de début</label>
			                  	<input type="text" name="edit_agenda_horaire_debut" id="edit_agenda_horaire_debut" class="form-control">
			                </div>
			                <div class="form-group">
			                  	<label for ='mois_fin'> Horare de fin</label>
			                  	<input type="text" name="edit_agenda_horaire_fin" id="edit_agenda_horaire_fin" class="form-control">
			                </div>
			                <div class="form-group">
			                  	<label for ='mois_fin'> Numero du stand</label>
			                  	<input type="text" name="edit_agenda_numero_stand" id="edit_agenda_numero_stand" class="form-control">
			                </div>
			                <div class="form-group">
			                  	<label for ='mois_fin'> description</label>
			                  	<textarea name="edit_agenda_description" id="edit_agenda_description" class="form-control"></textarea>
			                </div>
			                <div class="form-group">
			                  	<label for ='mois_fin'> site internet</label>
			                  	<input type="text" name="edit_agenda_site_internet" id="edit_agenda_site_internet" class="form-control">
			                </div> 
			                <input type="hidden" id='edit_agenda_id' name = "edit_agenda_id">               
			              	<button type="submit" class="btn btn-default">Submit</button>
			    		</form>
					</div>
		        	<div class="modal-footer">
		            	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        	</div>
				</div>
    		</div>
		</div>
  	</div>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src='/js/jquery-ui.min.js'></script>
  <link type="text/css" href='/css/jquery-ui.min.css' rel="stylesheet" />
  <script>
    function editAgenda(id) {
      	var view_url = $("#hidden_agenda_view").val();
      	$.ajax({
        	url: view_url,
        	type:"GET", 
        	data: {"id":id}, 
        	success: function(result){
          		$("#edit_agenda_id").val(id);
          		$("#edit_agenda_titre").val(result.titre);
          		$('#edit_agenda_adresse').val(result.adresse);
          		$('#edit_agenda_ville').val(result.ville);
          		$('#edit_agenda_mois_debut').val(result.mois_debut);
          		$('#edit_agenda_mois_fin').val(result.mois_fin)
          		$('#edit_agenda_horaire_debut').val(result.horaire_debut);
          		$('#edit_agenda_horaire_fin').val(result.horaire_fin);
          		$('#edit_agenda_numero_stand').val(result.numero_stand);
          		$('#edit_agenda_site_internet').val(result.site_internet);
        	}
      	});
    }

    $('.tableBody').children().each(function(n, i) {
        var id = this.id;
        if(id%2 != 0) {
            $('#'+id).css('background-color','#EAEAEA');
        }
    });

 	$(function() {
  		$( "#mois_debut, #mois_fin, #edit_agenda_mois_debut, #edit_agenda_mois_fin").datepicker({
		    firstDay: 1,
		    altField: this,
		    closeText: 'Fermer',
		    prevText: 'Précédent',
		    nextText: 'Suivant',
		    currentText: 'Aujourd\'hui',
		    monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
		    monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
		    dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
		    dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
		    dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
		    weekHeader: 'Sem.',
		    dateFormat: 'dd MM yy'
		});	
	}); 
</script>
@stop