@extends('layout.app')
@section('title','créer un article')
@section('sidebar')
@endsection
@section('content')
	<div class="titrePage">
		<h1> Créer un nouvel article </h1>
	</div>
	{!! Form::open(['method' => 'POST', 'files' => true, 'url'=>'admin/posts/nouveau'], array('url' => 'posts')) !!}
		<div class="form-group"> 
			{!! Form::label('titre', 'title') !!}
			{!! Form::text('title', Input::old('title')) !!}
		</div>
		<div class="form-group"> 
			{!! Form::label('content', 'content') !!}
			{!! Form::textarea('content', Input::old('content'), array('class' => 'form-control')) !!}
		</div>
		<div class="form-group"> 
			{!! Form::label('image', 'image') !!}
			{!! Form::file('image', Input::old('image')) !!}
		</div>
		<div class="form-group"> 
			{!!  Form::label('categories', 'categorie')!!}
			<select name="categories" id="categories" class="form-control">
  			@foreach($categories as $categorie)
  				<option value="" disabled selected>Selectionner une catégorie</option>
    			<option value="{{ $categorie->id }}">{{ $categorie->nom }}</option>
  			@endforeach
			</select>
		</div>
		{!! Form::submit('valider', array('class'=>'btn')) !!}
	{!! Form::close() !!}
@endsection



