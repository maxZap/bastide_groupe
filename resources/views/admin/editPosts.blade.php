@extends('layout.app')
@section('title', 'modifié un article')

@section('sidebar')
@endsection

@section('content')
	<h1> modifié un article </h1>
	{!! Form::open(['route' => ['posts.update', $posts->id], 'method' => 'patch', 'files' => true,]) !!}
		<div class="form-group"> 
			{!! Form::label('titre', 'title') !!}
			{!! Form::text('title', Input::old('title'), array('placeholder'=>$posts->title)) !!}
		</div>
		<div class="form-group"> 
			{!! Form::label('description', 'description') !!}
			{!! Form::text('description', Input::old('description'), array('placeholder'=>$posts->description)) !!}
		</div>
		<div class="form-group"> 
			{!! Form::label('content', 'content') !!}
			{!! Form::textarea('content', Input::old('content'), array('class' => 'form-control', 'placeholder' => $posts->content)) !!}
		</div>
		<div class="form-group"> 
			{!! Form::label('image', 'image') !!}
			{!! Form::file('image', Input::old('image')) !!}
		</div>
		<div class="form-group"> 
			{!!  Form::label('categories', 'categorie')!!}
			<select name="categories" id="categories" class="form-control">
  			<option value="" disabled selected>Selectionner une catégorie</option>
  			@foreach($categories as $categorie)
    			<option value="{{ $categorie['id'] }}">{{ $categorie['nom'] }}</option>
    		@endforeach
			</select>
		</div>
		{!! Form::submit('valider', array('class'=>'btn')) !!}
	{!! Form::close() !!}
	</div>
@endsection


