@extends('layout.admin');
@section('title', 'panneau administration Carriere')
@endsection

@section('content')

<div id="carriere-admin-container">
	<h2> Carrieres </h2>
	 <button type="button" class="btn btn-info" data-toggle="modal" data-target="#addCarriereModal">Créer une catégorie</button>
	<table class="table">
		<thead>
			<td>Nom</td>
			<td>Description</td>
			<td>url</td>
			<td> </td>
		</thead>
		<tbody class="tableBody">
			@foreach($carrieres as $carriere)
				@if($carriere->active == 1)
					<tr class="table-row active">
						<td> {{ $carriere->nom }} </td>
						<td> {{ $carriere->description }} </td>
						<td> {{ $carriere->url }} </td>
						<td><img src="/uploads/ressources/edit.svg" data-toggle="modal" data-target="#editCarriereModal" onclick="editCarriere('{{$carriere->id}}')"></td>
	            		<td><a type="button" data-toggle="modal" data-target="#modal{{$carriere->id}}" href="#collapse1"><img src="/uploads/ressources/voir.svg" ></a></td>
	              		{!! Form::open(['method' => 'POST', 'id' => 'formDeleteCarriere', 'action' => ['CarrieresController@destroy', $carriere->id]]) !!}
	                		<td><button type="submit", class="delete deleteCarriere" id="btnDeleteCarriere"><img src="/uploads/ressources/checked.svg"></button></td>
	              		{!! Form::close() !!}
					</tr>
				@else
					<tr class="table-row inactive">
						<td> {{ $carriere->nom }} </td>
						<td> {{ $carriere->description }} </td>
						<td> {{ $carriere->url }} </td>
						<td><img src="/uploads/ressources/edit.svg" data-toggle="modal" data-target="#editCarriereModal" onclick="editCarriere('{{$carriere->id}}')"></td>
	            		<td><a type="button" data-toggle="modal" data-target="#modal{{$carriere->id}}" href="#collapse1"><img src="/uploads/ressources/voir.svg" ></a></td>
	              		{!! Form::open(['method' => 'POST', 'id' => 'formDeleteCarriere', 'action' => ['CarrieresController@destroy', $carriere->id]]) !!}
	                		<td> <button type="submit", class="delete deleteCarriere" id="btnDeleteCarriere"><img src="/uploads/ressources/unchecked.svg"></button> </td>
	              		{ !! Form::close() !! }
					</tr>
				@endif
			@endforeach
		</tbody>
	</table>
	<input type="hidden" name="hidden_view" id="hidden_carriere_view" value="{{url('admin/carriere/')}}">
	@foreach($carrieres as $carriere)
	    	<div class="modal hide fade" id="modal{{$carriere->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	      		<div class="modal-dialog">
	        		<div class="modal-content">
	          			<div class="modal-header">
	            			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	            			<p class="modal-title" id="myModalLabel">  </p>
	            			<h2>{{ $carriere->nom }}</h2>
	          			</div>
	          			<div class="modal-body">
			              <p> {{ $carriere->description }}</p>
			              <p> {{ $carriere->url }} </p>
			              <p> <img src="/uploads/ressources/carrieres/{{$carriere->image_presentation}}"></p>
			            </div>
			        </div>
			    </div>
			</div>
  		@endforeach
  		<div class="modal fade" id="addCarriereModal" role="dialog">
      		<div class="modal-dialog">
        	<!-- Modal content-->
        		<div class="modal-content">
          			<div class="modal-header">
            			<button type="button" class="close" data-dismiss="modal">&times;</button>
            			<h4 class="modal-title">Add Record</h4>
          			</div>
          			<div class="modal-body">
           				<form action="{{ url('admin/carriere') }}" method="post" >
							<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
							<div class="form-group">
								<label for="carriere_nom">nom</label>
								<input type="text" class="form-control" id="carriere_nom" name="carriere_nom">
								<label for="carriere_nom">image</label>
								<input type="text" class="form-control" id="carriere_image_presentation" name="carriere_image_presentation">
								<label for="carriere_description">description</label>
								<input type="text" class="form-control" id="carriere_description" name="carriere_description">
								<label for="carriere_url">url</label>
								<input type="text" class="form-control" id="carriere_url" name="carriere_url">
							</div>     
							<button type="submit" class="btn btn-default">Submit</button>
						</form>
            			<div class="modal-footer">
              				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            			</div>
          			</div>
        		</div>
      		</div> 
		</div>
	</div>
@stop