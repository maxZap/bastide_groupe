@extends('layout.admin')

@section('title', 'Activité')
@endsection


@section('content')

<div id="activite-admin-container">
	<h2> Gestion des activités </h2>
	<button type="button" class="btn btn-info" data-toggle="modal" data-target="#addActivitesModal">Ajouter un article </button>
	<table class="table">
		<thead>
			<td> titre</td>
			<td> slug</td>
			<td> plaquette </td>
			<td> </td>
		</thead>
		<tbody class="tableBody">
			@foreach($activites as $activite)
				@if($activite->active == 1)
					<tr class="table-row active">
						<td> {{$activite->titre}} </td>
						<td> {{$activite->slug}} </td>
						<td> {{$activite->plaquette}} </td>
						<td><img src="/uploads/ressources/edit.svg" data-toggle="modal" data-target="#editActivitesModal"  onclick="editActivite('{{$activite->id}}')"></td>
              			{!! Form::open(['method' => 'POST', 'id' => 'formDeleteActivites', 'action' => ['ActivitesController@destroy', $activite->id]]) !!}
                			<td><button type="submit", class="delete deleteActivites" id="btnDeleteActivites"><img src="/uploads/ressources/checked.svg">  </button></td>
              			{!! Form::close() !!}
					</tr>
				@else
					<tr class="table-row inactive">
						<td> {{$activite->titre}} </td>
						<td> {{$activite->slug}} </td>
						<td> {{$activite->plaquette}} </td>
						<td><img src="/uploads/ressources/edit.svg" data-toggle="modal" data-target="#editActivitesModal"  onclick="editActivite('{{$activite->id}}')"></td>
              			{!! Form::open(['method' => 'POST', 'id' => 'formDeleteActivites', 'action' => ['ActivitesController@destroy', $activite->id]]) !!}
                			<td><button type="submit", class="delete deleteActivites" id="btnDeleteActivites"><img src="/uploads/ressources/unchecked.svg">  </button></td>
              			{!! Form::close() !!}
					</tr>
				@endif
			@endforeach
		</tbody> 
	</table>
	<input type="hidden" name="hidden_activites_view" id="hidden_activites_view" value="{{url('admin/activites')}}">
		<div class="modal fade" id="addActivitesModal" role="dialog">
      		<div class="modal-dialog">
        		<div class="modal-content">
          			<div class="modal-header">
            			<button type="button" class="close" data-dismiss="modal">&times;</button>
            			<h4 class="modal-title">Ajouter un activité </h4>
          			</div>
          			<div class="modal-body">
          				{!! Form::open(['method' => 'POST', 'files' => true, 'url'=>'admin/activites']) !!}
	              			<div class="form-group">
	              				<label for="activites_titre"> Titre </label>
	              				<input type="text" name="activites_titre" id="activites_titre" class="form-control">
	              				<label for='activites_contenu'> contenu </label>
	              				<textarea type="text" name="activites_contenu" id="activites_contenu" class="form-control"></textarea>
	              				{!! Form::label('activites_header', 'image') !!}
                				{!! Form::file('activites_header', Input::old('activites_header')) !!}
                				{!! Form::label('activites_plaquette', 'plaquette') !!}
                				{!! Form::file('activites_plaquette', Input::old('activites_plaquette')) !!}
	              			</div> 
			            	<button type="submit" class="btn btn-default">Submit</button>
		        		{!! Form::close() !!}
		        	</div>
		        	<div class="modal-footer">
		          		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        	</div>
		    	</div>
			</div>
		</div>
		<div class="modal fade" id ="editActivitesModal" role="dialog">
      		<div class="modal-dialog">
        		<div class="modal-content">
          			<div class="modal-header">
            			<button type="button" class="close" data-dismiss="modal">&times;</button>
              			<h4 class="modal-title"> Modifié un article </h4>
            		</div>
            		<div class="modal-body">
	             		{!! Form::open(['method' => 'POST', 'files' => true, 'url'=>'admin/activites/update'], array('url' => 'activites')) !!}
	             			<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
	             			<div class="form-group">
		             		 	<label for="edit_activites_titre"> Titre </label>
			              		<input type="text" name="edit_activites_titre" id="edit_activites_titre" class="form-control">
		          				<label for='edit_activites_contenu'> contenu </label>
		          				<textarea type="text" name="edit_activites_contenu" id="edit_activites_contenu" class="form-control"></textarea>
		          				{!! Form::label('edit_activites_header', 'image') !!}
		        				{!! Form::file('edit_activites_header', Input::old('edit_activites_header')) !!}
		        				{!! Form::label('edit_activites_plaquette', 'plaquette') !!}
		        				{!! Form::file('edit_activites_plaquette', Input::old('edit_activites_plaquette')) !!}
		             		</div>
	             			<button type="submit" class="btn btn-default">Submit</button>
	             			<input type="hidden" id='edit_activites_id' name="edit_activites_id">
		        		{!! Form::close() !!}
            		</div>
	            	<div class="modal-footer">
	              		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	            	</div>
	        	</div>
	    	</div>
		</div> 
	</div>


	<script> 
		function editActivite(id){
		var view_url = $("#hidden_activites_view").val();
          $.ajax({
            url: view_url,
            type:"GET", 
            data: {"id":id}, 
            success: function(result){
              $("#edit_activites_id").val(id);
              $("#edit_activites_titre").val(result.titre);
              $("#edit_activites_contenu").val(result.content);
              $("#edit_activites_header").val(result.header_picture);
              $("#edit_activites_plaquette").val(result.plaquette);
              console.log(id);
            }
          });
		}
	</script>
@stop