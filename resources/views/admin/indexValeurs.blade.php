@extends('layout.admin');
@section('title','valeurs')
@endsection

@section('content')

	<div id="admin-valeurs-container">
		<h2> Valeurs </h2>
		<button type="button" class="btn btn-info" data-toggle='modal' data-target="#addValeurModal"> Ajouter une valeur </button>
		<table class="table">
			<thead>
				<td> Titre </td>
				<td> Description </td>
				<td> </td>
			</thead>
			<tbody class="tableBody">
				@foreach($valeurs as $valeur)
					@if($valeur->active==1)
						<tr id="{{$valeur->id}}" class="table-row active">
							<td> {{$valeur->titre}} </td>
							<td> {{substr($valeur->description, 0, 70) . '...'}} </td>
							 <td><button  data-toggle="modal" data-target="#EditValeurModal" onclick="editValeurs('{{$valeur->id}}')"><img src="/uploads/ressources/edit.svg"></button></td>
							<td><a type="button" data-toggle="modal" data-target="#modal{{$valeur->id}}" href="#collapse1"><img src="/uploads/ressources/voir.svg" ></a></td>
			              	{!! Form::open(['method' => 'POST', 'id' => 'formDeleteValeurs', 'action' => ['ValeursController@destroy', $valeur->id]]) !!}
			                	<td><button type="submit" class="delete deleteValeurs" id="btnDeleteValeurs" data-id ="$valeur->id"><img src="/uploads/ressources/checked.svg">  </button></td>
			              	{!! Form::close() !!}
			            </tr>
					@else 
						<tr id="{{$valeur->id}}" class="table-row inactive">
                			<td>{{$valeur->titre}}</td>
                			<td>{{ substr($valeur->description, 0, 70) . '...'}}</td>
                			<td><button  data-toggle="modal" data-target="#EditValeurModal" onclick="editValeurs('{{$valeur->id}}')"><img src="/uploads/ressources/edit.svg"></button></td>
                			<td><a type="button" data-toggle="modal" data-target="#modal{{$valeur->id}}" href="#collapse1"><img src="/uploads/ressources/voir.svg" ></a></td>
                			{!! Form::open(['method' => 'POST', 'id' => 'formDeleteValeurs', 'action' => ['ValeursController@destroy', $valeur->id]]) !!}
			                  <td><button type="submit", class="delete deleteValeurs" id="btnDeleteValeurs" data-id ="$valeur->id"><img src="/uploads/ressources/unchecked.svg">  </button></td>
			                {!! Form::close() !!}
             			 </tr>
					@endif
				@endforeach
			</tbody>
		</table>
		<input type="hidden" name="hidden_valeurs_view" id="hidden_valeurs_view" value="{{url('admin/valeurs')}}">
		@foreach($valeurs as $valeur)
	    	<div class="modal hide fade" id="modal{{$valeur->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	      		<div class="modal-dialog">
	        		<div class="modal-content">
	          			<div class="modal-header">
	            			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	            			<p class="modal-title" id="myModalLabel"></p>
	            			<h2> {{$valeur->titre}}</h2>
	          			</div>
	          			<div class="modal-body">
			              <p>{{$valeur->description}}</p>
			              <img src="/uploads/ressources/{{$valeur->filename}}">
			            </div>
			        </div>
			    </div>
			</div>
  		@endforeach
		<div class="modal fade" id="addValeurModal" role="dialog">
      		<div class="modal-dialog">
        		<div class="modal-content">
          			<div class="modal-header">
            			<button type="button" class="close" data-dismiss="modal">&times;</button>
            			<h4 class="modal-title">Ajouter une valeur </h4>
          			</div>
          			<div class="modal-body">
          				{!! Form::open(['method' => 'POST', 'files' => true, 'url'=>'admin/valeurs']) !!}
	              			<div class="form-group">
	              				<label for="valeur_titre"> Titre </label>
	              				<input type="text" name="valeur_titre" id="valeur_titre" class="form-control">
	              				<label for='valeur_description'> Description </label>
	              				<textarea type="text" name="valeur_description" id="valeur_description" class="form-control"></textarea>
	              				{!! Form::label('valeur_image', 'image') !!}
                				{!! Form::file('valeur_image', Input::old('valeur_image')) !!}
	              			</div> 
			            	<button type="submit" class="btn btn-default">Submit</button>
		        		{!! Form::close() !!}
		        	</div>
		        	<div class="modal-footer">
		          		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        	</div>
		    	</div>
			</div>
		</div>
		<div class="modal fade" id="EditValeurModal" role="dialog">
      		<div class="modal-dialog">
        		<div class="modal-content">
          			<div class="modal-header">
            			<button type="button" class="close" data-dismiss="modal">&times;</button>
            			<h4 class="modal-title">Modifier une valeur </h4>
          			</div>
          			<div class="modal-body">
            			<form action="{{ url('admin/valeurs/osef/update') }}" enctype="multipart/form-data" method="post">
            				<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
	              			<div class="form-group">
	              				<label for="edit_valeur_titre"> Titre </label>
	              				<input type="text" name="edit_valeur_titre" id="edit_valeur_titre" class="form-control">
	              				<label for='edit_valeur_description'> Description </label>
	              				<textarea type="text" name="edit_valeur_description" id="edit_valeur_description" class="form-control"></textarea>
	              				{!! Form::label('edit_valeur_image', 'image') !!}
                				{!! Form::file('edit_valeur_image', Input::old('edit_valeur_image')) !!}
	              			</div>
	              			<input type="hidden" id='edit_valeur_id' name = "edit_valeur_id">
			            	<button type="submit" class="btn btn-default">Submit</button>
		        		 {!! Form::close() !!}
		        	</div>
		        	<div class="modal-footer">
		          		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		        	</div>
		    	</div>
			</div>
		</div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script>

		 $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
		function editValeurs(id) {
      	var view_url = $("#hidden_valeurs_view").val();
      	$.ajax({
        	url: view_url,
        	type:"GET", 
        	data: {"id":id}, 
        	success: function(result){
          		$("#edit_valeur_id").val(id);
          		$("#edit_valeur_titre").val(result.titre);
          		$("#edit_valeur_description").val(result.description);
          		$("#edit_valeur_filename").val(result.filename);
          		console.log(id);
        	}
      	});
    }

    $('.tableBody').children().each(function(n, i) {
        var id = this.id;
        if(id%2 == 0) {
            $('#'+id).css('background-color','#EAEAEA');
        }
    });

	</script>
@stop