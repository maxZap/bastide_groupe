@extends('layout.app')
@section('title', 'gerer les dossiers')
@endsection
@include('navigation.admin')
@section('content')
 @if($message = Session::get('success'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
      @elseif($message = Session::get('error'))
      <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
    @endif
	<h2> Gestion des dossiers de rangement des PDF </h2>
		<button type="button" class="btn btn-info" data-toggle="modal" data-target="#addFoldersModal">Créer un dossier</button>
	<table class="table table-striped">
		<thead>
			<tr>
				<td>ID</td>
				<td>Nom</td>
				<td>Categorie</td>
				<td>Sous Categorie</td>
				<td>Action</td>
			</tr>
		</thead>
		<tbody>
			@foreach($folders as $folder)  
			<tr>

				<td>{{$folder->id}}</td>
        <td>{{$folder->name}}</td>
				<td>{{$folder->categorie->name}}</td>
        <td>{{$folder->subcategorie->nom}}</td>
				<td><button class="btn btn-warning" data-toggle="modal" data-target="#editFoldersModal" onclick="fun_edit('{{$folder->id}}')">Modifié</button></td>
				{!! Form::open(['method' => 'DELETE', 'id' => 'formDeleteFolders', 'action' => ['FoldersController@destroy', $folder->id]]) !!}
        			<td><button type="submit" id="btnDeleteFolders" data-dismiss="{{$folder->id}}"><img src="{{URL::asset('/uploads/images/delete.png')}}" height="20" width="20"></button></td>
        		{!! Form::close() !!}
			</tr>
			@endforeach
		</tbody>

		<input type="hidden" name="hidden_view" id="hidden_view" value="{{url('admin/folders/')}}">
	<div class="modal fade" id="addFoldersModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Créer un nouveau dossier</h4>
        </div>
        <div class="modal-body">
          {!! Form::open(['method' => 'POST' , 'url'=>'admin/folders']) !!}
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <div class="form-group">
              <div class="form-group">
                {!! Form::label('nom du dossier', 'Nom du dossier') !!}
                {!! Form::text('name', Input::old('name'), array('class'=>'form-control')) !!}
              </div>
              
                {!!  Form::label('categorie', 'categorie')!!}
                <select name="categorie" id="categorie" class="form-control">
                  <option value="0" disabled selected>Selectionner une categorie</option>
                  @foreach($categories as $categorie)
                    <option value="{{$categorie->id}}">{{ $categorie->name }}</option>
                  @endforeach
                </select>
                  {!!  Form::label('sous-categorie', 'sous-categorie')!!}
                <select name="subcategorie" id="subcategorie" class="form-control">
                  <option value="0" disabled selected>Selectionner une sous categorie</option>
                  @foreach($subcategories as $subcategorie)
                    <option value="{{$subcategorie->id}}">{{ $subcategorie->nom }}</option>
                  @endforeach
                </select>
              </div>                   
              <button type="submit" class="btn btn-default">Submit</button>
            </div>
          {!! Form::close() !!}
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div> 
  </div>



  <div class="modal fade" id ="editFoldersModal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"> Modifié un dossier </h4>
            </div>
            <div class="modal-body">
              <form action="{{ url('admin/folders/update') }}" method="post" >
                 <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="form-group">
                  <div class="form-group">
                    <label for="edit_name"> nom du dossier: </label>
                    <input type="text" class="form-control" id="edit_name" name="edit_name"><br/>
                   {!!  Form::label('categorie', 'categorie')!!}
                <select name="edit_categorie" id="edit_categorie" class="form-control">
                  <option value="0" disabled selected>Selectionner une categorie</option>
                  @foreach($categories as $categorie)
                    <option value="{{$categorie->id}}">{{ $categorie->name }}</option>
                  @endforeach
                </select>
                  {!!  Form::label('sous-categorie', 'sous-categorie')!!}
                <select name="edit_subcategorie" id="edit_subcategorie" class="form-control">
                  <option value="0" disabled selected>Selectionner une sous categorie</option>
                  @foreach($subcategories as $subcategorie)
                    <option value="{{$subcategorie->id}}">{{ $subcategorie->nom }}</option>
                  @endforeach
                </select>
                  </div>
                  <button type="submit" class="btn btn-default"> enregistré les modifications</button>
                  <input type="hidden" id='edit_id' name="edit_id">
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
 <script type="text/javascript">
   $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

   function fun_edit(id) {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          $("#edit_id").val(id);
          $("#edit_nom").val(result.name);
          $("#edit_categorie").val(result.categorie_id);
          $("#edit_subcategorie").val(result.subcategorie_id);        }
      });
    }

</script>




@endsection