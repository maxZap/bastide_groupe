@extends('layout.app')
@section('title', 'manage files')
@endsection
@include('navigation.admin')
@section('content')
	@if($message = Session::get('success'))
  	<div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif
  @if($message = Session::get('error'))
    <div class="alert alert-danger alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif
	<h2> Gerer les fichiers PDF  </h2>
	<button type="button" class="btn btn-info" data-toggle="modal" data-target="#addFileModal">importer un fichier</button>
	<table class="table table-striped">
		<thead>
			<tr>
				<td> ID </td>
				<td> Nom </td>
        <td> Dossier </td>
				<td> date de publication </td>
				<td> action </td>
			</tr>
		</thead>
		<tbody>
			@foreach($files as $file)
				<tr>
					<td> {{$file->id}}</td>
					<td> {{$file->title}}</td>
          <td> {{$file->folder->name}}</td>
					<td> {{$file->created_at}}</td>
					<td><button class="btn btn-warning" data-toggle="modal" data-target="#editModal" onclick="fun_edit('{{$file->id}}')">Modifié</button></td>
       		{!! Form::open(['method' => 'DELETE', 'id' => 'formDeleteFiles', 'action' => ['fileUploadsController@destroy', $file->id]]) !!}
        		<td><button type="submit" id="btnDeleteFiles" data-dismiss="{{$file->id}}"><img src="{{URL::asset('/uploads/images/delete.png')}}" height="20" width="20"></button></td>
        	{!! Form::close() !!}
      	</tr>
			@endforeach
		</tbody>
	</table>
	<input type="hidden" name="hidden_view" id="hidden_view" value="{{url('admin/files/')}}">
	<div class="modal fade" id="addFileModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Créer un nouvel article</h4>
        </div>
        <div class="modal-body">
          {!! Form::open(['method' => 'POST', 'files' => true, 'url'=>'admin/files']) !!}
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <div class="form-group">
              <div class="form-group">
                {!! Form::label('titre du fichier', 'titre du fichier') !!}
                {!! Form::text('title', Input::old('title'), array('class'=>'form-control')) !!}
              </div>
              <div class="form-group">
                {!! Form::label('fichier', 'fichier') !!}
                {!! Form::file('filename', Input::old('filename')) !!}
              </div>
              <div class="form-group">
                {!!  Form::label('dossier', 'dossier')!!}
                <select name="folder" id="folder" class="form-control">
                  <option value="0" disabled selected>Selectionner un dossier</option>
                  @foreach($folders as $folder)
                    <option value="{{$folder->id}}">{{ $folder->name }}</option>
                  @endforeach
                </select>
              </div>                   
              <button type="submit" class="btn btn-default">Submit</button>
            </div>
          {!! Form::close() !!}
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div> 
  </div>

  <div class="modal fade" id ="editModal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"> Modifié une categorie </h4>
            </div>
            <div class="modal-body">
              <form action="{{ url('admin/files/update') }}" method="post" enctype="multipart/form-data">
                 <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div class="form-group">
                  <div class="form-group">
                    <label for="edit_nom"> nom: </label>
                    <input type="text" class="form-control" id="edit_name" name="edit_name"><br/>
               			<label for="filename"> Nouveau Fichier : </label>
                    <input type="file" name="edit_filename" id="edit_filename">
                    <select name="edit_folder" id="edit_folder" class="form-control">
                  <option value="0" disabled selected>Selectionner un dossier</option>
                  @foreach($folders as $folder)
                    <option value="{{$folder->id}}">{{ $folder->name }}</option>
                  @endforeach
                </select>
                  </div>
                  <button type="submit" class="btn btn-default"> enregistré les modifications</button>
                  <input type="hidden" id='edit_id' name = "edit_id">
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
 <script type="text/javascript">
   $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

   function fun_edit(id) {
      var view_url = $("#hidden_view").val();
      $.ajax({
        url: view_url,
        type:"GET", 
        data: {"id":id}, 
        success: function(result){
          $("#edit_id").val(id);
          $("#edit_name").val(result.title);
          $("#edit_filename").val(result.filename);
          $("#edit_folder").val(result.folder_id);        }
      });
    }

</script>

@endsection