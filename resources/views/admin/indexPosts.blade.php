@extends('layout.admin')
@section('title', 'Gerer les articles')
@endsection
@section('content')
   
  <style>
  button, input {
  margin: 0;
  padding: 0;
  border: none;
  font: inherit;
  line-height: normal;
  background-color:transparent;
}
</style>
  @if($message = Session::get('success'))
    <div class="alert alert-success alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif
  @if($message = Session::get('error'))
    <div class="alert alert-danger alert-block">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>{{ $message }}</strong>
    </div>
  @endif
  <div id="article-admin-container">
    <h2> Articles </h2>
  <button type="button" class="btn btn-info" data-toggle="modal" data-target="#addPostModal">Créer un article</button>
  <table class="table table-striped">
    <thead>
      <tr>
        <td> Date </td>
        <td> Auteur </td>
        <td> Categorie </td>
        <td> Titre </td>
        <td> </td>
        
      </tr>
    </thead>
    <tbody class="tableBody">
      @foreach($posts as $post)
      @if($post->active == 1)
        <tr id="{{$post->id}}" class="table-row active">
          <td class="date-row"> {{Carbon\Carbon::parse($post->created_at)->format('d/m/Y à H:m')}}</td>
          <td class="autor-row"> {{$post['user']->nom}} {{$post['user']->prenom}} </td>    
          <td class="categorie-row">{{$post['categorie']['name']}}</td>
          <td class="title-row">{{$post-> title}}</td>
          
            <td><img src="/uploads/ressources/edit.svg" data-toggle="modal" data-target="#editPostModal"  onclick="fun_edit('{{$post->id}}')"></td>
            <td><a><img src="/uploads/ressources/voir.svg" ></a></td>
              {!! Form::open(['method' => 'POST', 'id' => 'formDeleteArticles', 'action' => ['PostsController@destroy', $post->id]]) !!}
                <td><button type="submit", class="delete deleteArticles" id="btnDeleteArticles"><img src="/uploads/ressources/checked.svg">  </button></td>
              {!! Form::close() !!}
 
        </tr>
      @else 
        <tr id="{{$post->id}}" class="table-row inactive">
          <td class="date-row"> {{Carbon\Carbon::parse($post->created_at)->format('d/m/Y à H:m')}}</td>
          <td class="autor-row"> {{$post['user']->nom}} {{$post['user']->prenom}} </td>    
          <td class="categorie-row">{{$post['categorie']['name']}}</td>
          <td class="title-row">{{$post-> title}}</td>
           <td class="icone">
            <td><img src="/uploads/ressources/edit.svg" data-toggle="modal" data-target="#editPostModal"  onclick="fun_edit('{{$post->id}}')"></td>
            <td><a><img src="/uploads/ressources/voir.svg" ></a></td>
              {!! Form::open(['method' => 'POST', 'id' => 'formDeleteArticles', 'action' => ['PostsController@destroy', $post->id]]) !!}
                <td><button type="submit", class="delete deleteArticles" id="btnDeleteArticles"><img src="/uploads/ressources/unchecked.svg">  </button></td>
              {!! Form::close() !!}
            </td>
        </tr>
      @endif
      @endforeach
    </tbody>
  </table>
  <div class="modal fade" id="addPostModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Créer un nouvel article</h4>
        </div>
        <div class="modal-body">
          {!! Form::open(['method' => 'POST', 'files' => true, 'url'=>'admin/posts/nouveau'], array('url' => 'posts')) !!}
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <div class="form-group">
              <div class="form-group">
                {!! Form::label('titre', 'title') !!}
                {!! Form::text('title', Input::old('title'), array('class'=>'form-control')) !!}
              </div>
              <div class="form-group ">
                {!! Form::label('description', 'description') !!}
                {!! Form::text('description', Input::old('description'), array('class' => 'form-control') ) !!}
              </div>
              <div class="form-group">
                {!! Form::label('content', 'content') !!}
                {!! Form::textarea('content', Input::old('content'), array('class' => 'form-control')) !!}
              </div>
              <div class="form-group">
                {!! Form::label('image', 'image') !!}
                {!! Form::file('image', Input::old('image')) !!}
              </div>
              <div class="form-group">
                {!!  Form::label('categories', 'categorie')!!}
                <select name="categories" id="categories" class="form-control">
                  <option value="0" disabled selected>Selectionner une catégorie</option>
                  @foreach($categories as $categorie)
                    <option value="{{$categorie->id}}">{{$categorie->name }}</option>
                  @endforeach
                </select>
                 {!!  Form::label('sous-categories', 'sous-categorie')!!}
                <select name="subcategories" id="subcategories" class="form-control">
                  <option value="0" disabled selected>Selectionner une sous-catégorie</option>
                  @foreach($subcategories as $subcategorie)
                    <option value="{{$subcategorie->id}}">{{$subcategorie->nom}}</option>
                  @endforeach
                </select>
              </div>                   
              <button type="submit" class="btn btn-default">Submit</button>
            </div>
          {!! Form::close() !!}
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div> 
  </div>
   <div class="modal fade" id ="editPostModal" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"> Modifié un article </h4>
            </div>
            <div class="modal-body">
             {!! Form::open(['method' => 'POST', 'files' => true, 'url'=>'admin/posts/update'], array('url' => 'posts')) !!}
              <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
              <div class="form-group">
                <div class="form-group">
                  <label for="edit_post_title"> Titre : </label>
                  <input type="text" class="form-control" id="edit_post_title" name="edit_post_title">
                  <label for="edt_post_description"> Description : </label>
                  <input type="text" class="form-control" id="edit_post_description" name="edit_post_description">
                  <label for="edit_post_content"> Content </label>
                  <textarea class="form-control" style="width:900px; height:450px;" id="edit_post_content" name="edit_post_content"></textarea>
                  {!! Form::label('edit_post_image', 'image') !!}
                  {!! Form::file('edit_post_image', Input::old('edit_post_image')) !!}
                  <label for="edit_post_categories">categories</label>
                  <select name="edit_post_categories" id="edit_post_categories" class="form-control">
                    <option value="0" disabled selected>Selectionner une catégorie</option>
                    @foreach($categories as $categorie)
                      <option value="{{$categorie->id}}">{{$categorie->name}}</option>
                    @endforeach
                  </select>
                 <label for="edit_post_subcategories"> sous-catégorie </label>
                <select name="edit_post_subcategories" id="edit_post_subcategories" class="form-control">
                  <option value="0" disabled selected>Selectionner une sous-catégorie</option>
                  @foreach($subcategories as $subcategorie)
                    <option value="{{$subcategorie->id}}">{{$subcategorie->nom}}</option>
                  @endforeach  
                </select>
                </div>
                  <button type="submit" class="btn btn-default"> enregistré les modifications</button>
                  <input type="hidden" id='edit_post_id' name = "edit_post_id">
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div> 
    </div>
    @endsection
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript">
     $(function() {
           // Replace all textarea tags with SCEditor
          $('textarea').sceditor({
            plugins: 'bbcode',
            style: '/js/minified/jquery.sceditor.default.min.css',
            emoticonsRoot: '/js/',
            resizeEnabled : false
          });
        }); 
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

        $('.tableBody').children().each(function(n, i) {
          var id = this.id;
          console.log(id);
          if(id%2 == 0) {
            $('#'+id).css('background-color','white');
          } else {
             $('#'+id).css('background-color','#EAEAEA');
          }
        });

        function fun_edit(id) {
          var view_url = $("#hidden_view").val();
          $.ajax({
            url: view_url,
            type:"GET", 
            data: {"id":id}, 
            success: function(result){
              $("#edit_post_id").val(id);
              $("#edit_post_title").val(result.title);
              $("#edit_post_description").val(result.description);
              $("#edit_post_content").val(result.content);
              $("#edit_post_filename").val(result.filename);
              $("#edit_post_categories").val(result.categorie_id);
              $("#edit_post_subcategories").val(result.subcategorie_id);
            }
          });
        }
    </script>
