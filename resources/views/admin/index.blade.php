@extends('layout.admin')
@section('title', 'Index admin panel')

@section('content')
	<div id="admin-container">
		<div id="stat-block">
			<div id="stat-block-title">
				<h1> Overview </h1>
			</div>
			<div id="overviewContent">
				<h2> Total du site </h2>
				<p><a href="/admin/posts">{{count($posts)}} Articles visible</a></p>
				
				<p><a href="admin/categories">{{count($categories)}} categories et {{count($subcategories)}} sous categories actives</a></p>
				<p><a href="admin/users">{{count($users)}} Utilisateurs enregistré</a></p>
				<p><a href="admin/files">{{count($files)}} Fichier PDF actif</a></p>
			</div>
		</div>

		<div id="recentActivity">
			<div id="RecentActivityTitle">
				<h2> Activité récente </h2>
			</div>
			<div id="RecentActivityContent">
				@foreach($postsLimit as $post)
					<p class="date">{{$post->created_at}}</p> 
					<p><a href="/admin/posts/edit/{{$post->id}}">{{$post->title}}</a></p>
				@endforeach
			</div>
		</div>

		<div id="agenda">
			<div id="agendaTitle">
				<h2> Agenda</h2>
			</div>
			<div id="agendaContent">
				@foreach($agendas as $agenda) 
				<p class="date"">Du {{$agenda->mois_debut}} Au {{$agenda->mois_fin}}</p>
				<p > {{$agenda->adresse}} , {{$agenda->ville}}</p>
				@endforeach

			</div>
		</div>

		<div id="dates-historique">
			<div id="dates-historique-Title">
				<h2> Dates historiques </h2>
			</div>
			<div id="dates-historique-Content">
				@foreach($histoires as $histoire)
					<p class="date">{{$histoire->date}}</p> 
					<p><a href="/admin/posts/edit/{{$post->id}}">{{ substr($histoire->description, 0, 70) . '...'}}</a></p>
				@endforeach
			</div>
		</div>	
	</div>
@endsection

