<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {

	public function user() {
		return $this->belongsTo('App\User');
	}

	public function comment() {
        return $this->hasMany('App\Comment');
	} 

	public function categorie() {
		return $this->belongsTo('App\Categorie');
	}

	public function subcategorie() {
		return $this->belongsTo('App\subCategorie');
	}

}
