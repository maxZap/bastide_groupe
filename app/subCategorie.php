<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class subCategorie extends Model {

	public function Categorie(){
		return $this->belongsTo('App\Categorie');
	}

	public function post() {
   		return $this->hasMany('App\Post', 'post_id');
	}

}
