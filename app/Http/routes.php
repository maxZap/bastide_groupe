<?php
use App\Categorie;
use App\Subcategorie;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

# ============== USER ROUTE ==================== #

Route::get('/users/connexion', array('uses' => 'UsersController@login'));
Route::post('/users/connexion', array('uses'=> 'UsersController@doLogin'));
Route::delete('users/deconnexion/{id}',array('uses' => 'UsersController@destroy', 'as' => 'users.deco'));


Route::get('/post/{id}', 'PostsController@show');

# ============== BLOG ROUTE ==================== #
Route::get('{categorie}/{subcategorie}/{slug}', function($categorie, $subcategorie, $slug){
	$post = DB::table('posts')->where('slug', '=', $slug)->get();
	$user = DB::table('users')->where('id', "=", $slug )->get();
	$app = app();
	if(!empty($post)) { //si le slug correspond a celui d'un article, alors il ramene sur l'action show du controller post
  		$controller = $app->make('\App\Http\Controllers\PostsController');
  		return $controller->callAction('show', $parameters = array('categorie' => $categorie, 'subcategorie' => $subcategorie, 'slug' => $slug));
	} elseif(!empty($user)) { // sinon il ramene sur le controller correspondant;
		$controller = $app->make('\App\Http\Controllers\UsersController');
  		return $controller->callAction('show', $parameters = array('categorie' => $categorie, 'subcategorie' => $subcategorie, 'slug' => $slug));
	} else {
		abort(403, 'Unauthorized action.');
	}
});

Route::get('/', 'PostsController@index');

# ============== COMMENT / SUBCOMMENT ROUTE ==================== #

Route::post('comments/add', 'CommentsController@store');
Route::delete('comments/delete/{id}',array('uses'=>'CommentsController@destroy', 'as' => 'comments.delete'));
Route::delete('subcomments/delete/{id}', 'subCommentsController@destroy');
Route::post('subcomments/add', 'subCommentsController@store');


# ============== FILES ROUTE ==================== #
Route::get('informations', 'FoldersController@index');
Route::get('{categorie}/{subcategorie?}/category/{slug}', 'fileUploadsController@information');


# ============== ADMIN PANEL ROUTE ==================== #

/*Route::group(['middleware' => 'admin'], function() {*/
	Route::get('admin/categories',  'CategoriesController@index');
	Route::post('admin/categories', 'CategoriesController@store');
	Route::post('admin/categories/update/', 'CategoriesController@update');
	Route::get('admin/posts', 'AdminController@adminPost');
	Route::post('admin/categories/delete/{id}', 'CategoriesController@destroy');
	Route::post('admin/posts/update', array('uses' => 'PostsController@update', 'as' => 'posts.update'));
	Route::get('admin/posts/nouveau', 'PostsController@create');
	Route::post('admin/posts/nouveau', 'PostsController@store');
	Route::post('admin/posts/delete/{id}', 'PostsController@destroy');
	Route::get('admin/users/{id}/profil/', 'UsersController@show');
	Route::get('admin/users/', 'UsersController@index');
	Route::get('admin/', 'AdminController@index' );
	Route::get('admin/comments', 'AdminController@getInactiveComments');
	Route::get('admin/comments/active/{id}', 'AdminController@activeComments');
	Route::get('admin/subcomments/{id}', 'AdminController@activeSubComment');
	Route::post('admin/subcomments/add','AdminController@addSubComment');
	Route::get('admin/folders', 'FoldersController@information');
	Route::post('admin/folders', 'FoldersController@createFolders');
	Route::get('admin/files', 'AdminController@adminFiles');
	Route::post('admin/files', 'fileUploadsController@addFile');
	Route::delete('admin/files/delete/{id}', 'fileUploadsController@destroy');
	Route::post('admin/files/update', 'fileUploadsController@editFiles');
	Route::get('admin/folders', 'AdminController@adminFolders');
	Route::post('admin/folders', 'FoldersController@addFolders');
	Route::delete('admin/folders/delete/{id}', 'FoldersController@destroy');
	Route::post('admin/folders/update', 'FoldersController@update');
	Route::post('admin/subcategories', 'subCategoriesController@store');
	Route::post('admin/subcategories/delete/{id}','subCategoriesController@destroy');
	Route::post('admin/subcategories/update', 'subCategoriesController@update');
	Route::get('admin/agenda', 'AdminController@agenda');
	Route::post('admin/agenda', 'AgendasController@store');
	Route::post('admin/agenda/update', 'AgendasController@update');
	Route::post('admin/agenda/delete/{id}', 'AgendasController@destroy');
	Route::get('admin/historique', 'HistoriquesController@index');
	Route::post('admin/historique/delete/{id}', 'HistoriquesController@destroy');
	Route::post('admin/historique/update', 'HistoriquesController@update');
	Route::post('/admin/users','UsersController@store');
	Route::post('admin/users/update','UsersController@update');
	Route::post('admin/users/delete/{id}', 'UsersController@ActiveUser');
	Route::get('admin/valeurs', 'AdminController@valeur');
	Route::post('admin/valeurs/{id}', 'ValeursController@destroy');
	Route::post('admin/valeurs/osef/update', 'ValeursController@update');
	Route::post('admin/valeurs', 'ValeursController@store');
	Route::get('admin/activites', 'AdminController@activite');
	Route::post('admin/activites', 'ActivitesController@store');
	Route::post('admin/activites/delete/{id}', 'ActivitesController@destroy');
	Route::post('admin/activites/update','ActivitesController@update');
	Route::get('admin/carriere', 'AdminController@carriere');
	Route::post('admin/carriere', 'AdminController@carriere');
	Route::post('admin/carriere/delete/{id}', 'CarrieresController@destroy');
	Route::get('admin/carriere/update', 'CarrieresControllers@update');

/*});*/


Route::get('search/livesearch','SearchController@searchBarFilters'); 
View::creator('navigation.main', function($view){
	$view->with(array(
		'categories' => Categorie::all(),
		'subcategories' => Subcategorie::all()
	));
});


Route::get('/activites/{slug}', 'ActivitesController@index');
Route::get('/activites', 'PagesController@Activité');
Route::get('/valeurs', 'ValeursController@index');
Route::get('/historique', 'PagesController@Historique');
Route::get('/finance', 'PagesController@Finance');
Route::get('/partenariat', 'PagesController@Partenariat');
Route::get('/presse', 'PagesController@Presse');
Route::get('/franchise', 'PagesController@Franchise');
Route::get('/mediaFilter/{id}', 'SearchController@searchMedia');
Route::get('/mosaiqueFilter/{id}', 'SearchController@mosaiqueFilter');
route::get('/carriere', 'CarrieresController@index');
