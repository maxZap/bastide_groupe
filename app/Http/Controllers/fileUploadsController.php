<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Folder;
use App\fileUpload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class fileUploadsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function information($categorie, $subcategorie, $slug) {
		$folders = Folder::where('slug', $slug)->first();
		$folderId = $folders['id'];
		$files = FileUpload::with('folder')->where('folder_id', $folderId)->get();
		if(isset($files)){
			return view::make('informations.indexFiles')
				->with(compact('files', 'folders'));
		} else {
			return back()
				->with('error', 'le dossier est vide');
		}
	}

	public function addFile(request $request) {
		$destinationPath = 'uploads/pdf';
		$validator = Validator::make($request->all(), [
			'title' => 'required',
			'filename' => 'required',
			'folder' => 'required'
		]);

		if($validator->fails()){
			return back()
				->with('error', 'Veuillez remplir tout les champs');
		} else {
		$files 				= new fileUpload;
		$files->title = Input::get('title');
		if(Input::file('filename')!= null && Input::file('filename')->isValid()) {
			$fileName 		= Input::file('filename')->getClientOriginalName();
			Input::file('filename')->move($destinationPath,$fileName);
			$files->filename = $fileName;
		}
		$files->folder_id 	= Input::get('folder');
		$files->save();
		return back()
			->with('success', 'Le fichier a bien été enregistré');
		}

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function editFiles(request $request) {
		$id = $request->edit_id;
		$destinationPath = 'uploads/pdf';
		$files = FileUpload::find($id);
		if(isset($files)){
			if( $request->edit_name)		{
				$files->title = $request->edit_name;
			} else {
				$files->title = $files->title;
			}
			if(Input::file('edit_filename')!= null && Input::file('edit_filename')->isValid()) {
				$fileName 		= Input::file('edit_filename')->getClientOriginalName();
				Input::file('edit_filename')->move($destinationPath,$fileName);
				$files->filename = $fileName;
			} else {
				$files->filename = $files->filename;
			}
			if($request->edit_folder){
				$files->folder_id 	= $request->edit_folder;
			} else {
				$files->folder_id = $files->folder_id;
			}
			$files->save();
			return back()
				->with('success', 'Le fichier a bien été enregistré');
		}else {
			return back()
				->with('error', 'vous devez renseigner les champs');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request, $id){
		$data = FileUpload::find($id);
		if(isset($data)) {
			$data->delete($request->all());
			return back()
				->with('success', 'fichier supprimée');
			} else {
				return back()
					->with('error', 'impossible de supprimer le fichiers, veuillez réessayer plus tard');
			}
	
	}

}
