<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Folder;
use View;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class FoldersController extends Controller {

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function index () {
		$folders  = folder::with('fileUpload', 'categorie', 'subcategorie')->get();

		return view::make('informations.Indexfolder')
			->with('folders', $folders);
	}


	public function addFolders(Request $request){
		$validator = Validator::make($request->all(), ['name' => 'required', 'categorie'=> 'required', 'subcategorie' => 'required']);
		if($validator->fails()){
			return back()
				->with('error', 'Erreur : Vous devez remplir tout les champs afin de créer une nouvelle catégorie');
		} else {
			 
			$folders = new Folder;
			$folders->name = Input::get('name');
			$folders->slug = $slug;
			$folders->categorie_id = Input::get('categorie');
			$folders->subcategorie_id = Input::get('subcategorie');
			$folders->save();
			return back()
				->with('success', 'Le nouveau dossier a été créé');
			}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request){
		$id = $request->edit_id;
		$data = Folder::find($id);
		if(isset($data)){
			if($request->edit_name) {
				$data->name = $request->edit_name;
			} else {
				$data->name = $data->name;
			}
			if($request->edit_categorie) {
				$data->categorie_id = $request->edit_categorie;
	 		} else {
	 			$data->categorie_id = $data->categorie_id;
	 		}
	 		if($request->edit_subcategorie) {
	 			$data->subcategorie_id = $request->edit_subcategorie;
	 		}
	 		else {
	 			$data->subcategorie_id = $data->subcategorie_id;
	 		}
			$data->save();
			return back()
				->with('success', 'dossier correctement modifié');
		} else {
			return back()
				->with('error', 'vous devez remplir tout les champs');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request, $id){
		$data = Folder::find($id);
		if(isset($data)) {
			$data->delete($request->all());
			return back()
				->with('success', 'fichier supprimée');
		} else {
			return back()
				->with('error', 'impossible de supprimer le fichiers, veuillez réessayer plus tard');
		}
	}

}
