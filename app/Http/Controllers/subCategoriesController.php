<?php namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use View;
use App\SubCategorie;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class subCategoriesController extends Controller {


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request) {
	$validator = Validator::make($request->all(), ['subcategorie_nom' => 'required', 'subcategorie_categorie' => 'required']); 
		if($validator->fails()){
			return back()
				->with('error', 'Le nom de la sous-catégorie et sa catégorie d\'appartenance doivent être renseigné');
		} else {
			$slug = str::slug(Input::get('subcategorie_nom'));
			$folders = new SubCategorie;
			$folders->nom = Input::get('subcategorie_nom');
			$folders->slug = $slug;
			$folders->categorie_id = Input::get('subcategorie_categorie');
			$folders->save();
			return back()
				->with('success', 'Le nouvelle sous-catégorie a été créé');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request) {
		$validator = Validator::make($request->all(), ['edit_subcategorie_categorie' => 'required']);
		if($validator->fails()){
			return back()
				->with('error', 'vous devez renseigner la catégorie d\'affiliation');
		} else {
			$id = $request->edit_subcategorie_id;
			$data = subCategorie::find($id);
			if(isset($data)){
				$slug = str::slug($request->edit_subcategorie_nom);
				if($request->edit_subcategorie_nom) {
					$data->nom = $request->edit_subcategorie_nom;
				} else {
					$data->nom = $data->nom;
				}
				$data->slug = $slug;
				$data->categorie_id = $request->edit_subcategorie_categorie;
				$data->save();
				return back()
					->with('success', ' Sous categorie modifiée avec succès');
			} else {
				return back()
					->with('error', 'la sous categorie n\'a pas pu être modifiée ');
			}
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request, $id) {
		$data = subCategorie::find($id);
		
		if(isset($data)) {
			if($data->active == 1) {
			$data->active = 0;
			$data->save();
			return back()
				->with('success', 'sous categorie archivée avec succès');
			} else {
				$data->active = 1; 
				$data->save();
				return back()
					->with('success', 'sous categorie affichée avec succes');
			}
			
		} else {
			return back()
				->with('error', 'impossible de supprimer le fichiers, veuillez réessayer plus tard');
		}
	}


	public function activate($request, $id) {

	}

}
