<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Historique;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;



class HistoriquesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		$historiques = Historique::all();
		return view::make('admin.indexHistorique')
			->with('historiques', $historiques );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request) {
		$message = ['requiered' => 'veuillez remplir le champs'];
		$validator = Validator::make($request->all(), [
			'date' => 'requiered',
			'description' => "requiered"
		], $message);  

		if($validator->fails()) {
			return back()
				->with('error', 'veuillez remplir tout les champs');
		} else {
			$historique = new Historique;
			$historique->date = Input::get('date_historique');
			$historique->description = Input::get('description_historique');
			$historique->save();
			return back()
				->with('success', 'la date a bien été enregistrée');
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	
	public function update(Request $request) {

		$id = $request->edit_historique_id;
		$data = Historique::find($id);

		if(isset($data)) {
			if($request->edit_historique_date) {
				$data->date = $request->edit_historique_date;
			} else {
				$data->date = $data->date;
			}
			if($request->edit_historique_description) {
				$data->description = $request->edit_historique_description;
			} else {
				$data->description = $data->description;
			}
			$data->save();
			return back()
			 	->with('success', 'la date a bien été modifié');

		} else {
			return back()
			->with('error', 'un problème est survenu veuillez réessayer plus tard');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		$data= Historique::find($id);
		if(isset($data)) {
			if($data->active == 1) {
				$data->active = 0;
				$data->save();
				return back()
					->with('success', 'La date selectionné a été désactivé avec succès');
			} else {
				$data->active = 1;
				$data->save();
				return back()
					->with('success', 'la date a été activée avec succès');
			}
		} else {
			return back()
				->with('error', 'un problème est survenue veuillez réessayer ultérieurement');
		}
	}

}
