<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use View;
use Session;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class UsersController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		$users = User::all();
		return view::make('admin.indexUsers')
			->with('users', $users);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		return View::make('users.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request) {
		$message = [ 'required' => 'Le champ :attribute est requis'];
		$validator = Validator::make($request->all(), [
			'nom' => 'required',
			'prenom' => 'required',
			'email' => 'required|unique:users|email',
			'password' => 'required|between:5,16'
		]);

		if($validator->fails()) {
			return back()
				->with('error', 'vous devez remplir tout les champs');
				
		} else {
			$user 			= new User;
			$user->nom 		= Input::get('nom');
			$user->prenom 	= Input::get('prenom');
			$user->email 	= Input::get('email');
			$user->password = hash::make(Input::get('password'));
			$user->active 	= 1;
			$user->save();
			return back()
				->with('success', "l'utilisateur a bien été créé");
		}
	}

	public function login() {
		return View::make('users.login');
	}

	public function doLogin(){
		$data = array(
			'email' => Input::get('email'),
			'password'=> Input::get('password')
		);

		$user = User::where('email', '=', Input::get('email'))->first();
		if(Auth::attempt($data)) {
			return Redirect::to('/')
				->with('success','vous êtes bien connecté'); 
		} else {
			Auth::logout();
			return Redirect('/users/connexion')
				->with('error', 'Email ou mot de passe érroné ');
		}
	}

	public function show($category, $subcategory , $id){
		$users = User::find($id); 
			return View::make('users.show')
				->with('users', $users);
	}

	

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request) {
			$id = $request->edit_user_id;
			$data = User::find($id);
			if(isset($data)){
				if($request->edit_user_email) {
					$data->email = $request->edit_user_email;
				} else {
					$data->email = $data->email;
				}
				if($request->edit_user_password){
					$data->password = hash::make($request->edit_user_password);
				} else {
					$data->password = $data->password;
				}
				$data->save();
				
			} else {
				
			}
	
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		if(Auth::check()){
			Auth::logout();
			return Redirect::to('/')
				->withSuccess('Vous avez bien été déconnecté');
		} else {
		return Redirect::to('/')
			->withErrors("Suite a un problème internet nous n'avons pas pu vous deconnecter");
		}
	}


	public function ActiveUser($id) {
		$data = User::find($id);
		if(isset($data)) {
			if($data->active == 1) {
			$data->active = 0;
			$data->save();
			return back()
				->with('success', 'sous categorie archivée avec succès');
			} else {
				$data->active = 1; 
				$data->save();
				return back()
					->with('success', 'sous categorie affichée avec succes');
			}
		} else {
			return back()
				->with('error', 'impossible de supprimer le fichiers, veuillez réessayer plus tard');
		}
	}
}
