<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Comment;
use View;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class CommentsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		$comments = Comment::with('subComment')->where('active', '=', '1');
		return view('comments.index')->with('comments', $comments);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request) {
		$message = 'Le contenu du commentaire ne peut pas être vide, veuillez réessayer';
		$validator 				= Validator::make($request->all(), [
			'content' 			=> 'required',
		]); 
		if($validator->fails()){
			return back()
				->with('error', $message);
		} else {
			$data = new Comment;
			$data->content = $request->content;	
			$data->user_id = Auth::id();
			$data->post_id = $request->post_id;
			$data->save();
			return back()
				->with('success', 'Votre commentaire a été enregistré et est en attente de validation par un modérateur');
		}
	}

/*	

	public function update($id) {
		$id = $request->edit_id;
		$data = Comment::find($id);
		if(isset($data)){
			if($edit_contet) {
				$data->nom = $request->edit_content;
			} else {
				$data->nom = $data->nom;
			}
			$data->save();
			return back()
			->with('success', "commentaires modifié avec succès");
		} else {
			return back() 
				->with('error', 'vous devez remplir les champs');
		}
	}
*/
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request, $id){
		if($request){	
			$data = Comment::find($id);
			$data->delete($request->all() );
			return back()
				->with('success', 'commentaire supprimer avec succès' );
		} else {
			return back()->with('errors','un probleme est survenue lors de la suppression du commentaire');
		}
	}

}
