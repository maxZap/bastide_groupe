<?php namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Post;
use App\Categorie;
use App\User;
use App\Comment;
use App\subComment;
use App\subCategorie;
use App\Valeur;
use App\Folder;
use App\fileUpload;
use App\Agenda;
use App\Historique;
use App\Activite;
use App\Carriere;
use View;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DB;

class AdminController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		$subcategories = subCategorie::where('active','=', '1')->get();
		$postsLimit = DB::table('posts')->where('active','=', '1')->limit('5')->OrderBy('created_at', 'DESC' )->get();
		$categories = Categorie::where('active','=', '1')->get();
		$agendas = Agenda::where('active', '=', '1')->get();
		$histoires = Historique::where('active', '=', '1')->limit('5')->orderBy('date', 'DESC')->get();
		$files = fileUpload::where('active','=', '1')->get();
		$users = User::all();
		$posts = Post::where('active','=', '1')->get();
		return view::make('admin.index')
			->with(compact('subcategories','posts', 'users', 'histoires', 'agendas', 'categories', 'postsLimit', 'files'));
	}

	/**
	 *Display all post into the admin panel 
	 *
	 * @return Reponse
	 */
	public function adminPost(){
		$posts = Post::with('categorie', 'subcategorie', 'user')->get();
		$categories = Categorie::all();
		$subcategories = subCategorie::all();
		return view::make('admin.indexPosts')
			->with(compact('categories', 'subcategories', 'posts'));
	}	

	/*public function getInactiveComments() {
 		$comments = Comment::with('post')->where('active', '=', '0')->get();
 		$subcomments = subComment::with('comment')->where('active', '=', '0')->get();
 		return view::make('admin.indexComments')
 			->with(compact('comments', 'subcomments'));
 	}*/
	
  	/*public function activeComments($id){
 		$comments = Comment::where('id', $id)->first();
 		$comments->active = 1;
 		$comments->save();
 		return back()
 			->with('success', 'Le commentaire a bien été activé');
 	}
*/
 	/*public function activeSubComment($id){
 		$subcomments = subComment::where('id', $id)->first();
 		$subcomments->active = 1;
 		$subcomments->save();
 		return back()
 			->with('success', 'La réponse au commentaire a bien été activée'); 
 	}*/


 	/*public function addSubComment(Request $request) {
 		$validator = Validator::make($request->all(), ['subComsContent' => 'required']);
		if($validator->fails()){
			return back()
				->with('error', 'le contenue de votre commentaire ne peut pas être vide');
		} else {
			$subComments = new subComment; 
			$subComments->content = Input::get('subComsContent');
			$subComments->comment_id = Input::get('commentId');
			$subComments->user_id = Auth::id();
			$subComments->active = 1;
			$subComments->save();
			return back()
				->with('success' , 'Votre réponse au commentaire à bien été enregistrée');
		}
	}*/

	public function adminFolders(){
		$folders = Folder::with('categorie', 'subcategorie')->get();
		$categories = Categorie::all();
		$subcategories = Subcategorie::all();
		return view::make('admin.manageFolder')
			->with(compact('folders', 'categories', 'subcategories'));

	}

	public function adminFiles(){
		$folders = Folder::all();
		$files = fileUpload::all();
		return view::make('admin.manageFiles')
			->with(compact('folders', 'files'));			
	}


	public function agenda() {
		$agendas = Agenda::all();
		return view::make('admin.indexAgenda')
			->with('agendas', $agendas);
	}

	public function valeur(){
		$valeurs = Valeur::all();
		return View::make('admin.indexValeurs')
			->with('valeurs', $valeurs);
	}

	public function activite() {
		$activites = Activite::all();
		return View::make('admin.indexActivite')
			->with('activites', $activites);	
	}

	public function carriere() {
		$carrieres = Carriere::all();
		return View::make('admin.indexCarriere')
			->with('carrieres', $carrieres);
	}

}
