<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Activite;
use view; 
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Input;
use DB;


use Illuminate\Http\Request;

class ActivitesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($slug) {
		$activites = Activite::where('slug', '=', $slug)->get();
		return view::make('pages.orthopedie')
			->with('activites', $activites);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request) {
		$destinationPath = 'uploads/ressources/activites/';
		$message 		 = ['required' => 'Vous devez remplir tout les champs'];
		$validator 				= Validator::make($request->all(), [
			'activites_titre' 	  => 'required',
			'activites_contenu'   => 'required',
			'activites_header' 	  => 'required',
			'activites_plaquette' => 'required'
		], $message); 

		if($validator->fails()){
			return back()
				->with('error', 'veuillez remplir tout les champs');
		} else {
			$activites = new Activite;
			$slug = str::slug(Input::get('activites_titre'));
			$activites->titre = Input::get('activites_titre');
			$activites->content = Input::get('activites_contenu');
			$activites->slug = $slug;
			if(Input::file('activites_header') != null && Input::file('activites_header')->isValid()) {
				$headerImage = Input::file('activites_header')->getClientOriginalName();
				Input::file('activites_header')->move($destinationPath,$headerImage);
				$activites->header_picture	= $headerImage;
			}
			if(Input::file('activites_plaquette') != null && Input::file('activites_plaquette')->isValid()) {
				$plaquetteName = Input::file('activites_plaquette')->getClientOriginalName();
				Input::file('activites_plaquette')->move($destinationPath,$plaquetteName);
				$activites->plaquette	= $plaquetteName;
			}			

			$activites->save();
			return back()
				->with('success', " l'activité a bien été créée");
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request) {
		$destinationPath = 'uploads/ressources/activites/';
		
		$id = $request->edit_activites_id;
		$data = Activite::find($id);
		if(isset($data)) {
			if($request->edit_activites_titre) {
				$data->titre = $request->edit_activites_titre;
			} else {
				$data->titre = $data->titre;
			}
			if($request->edit_activites_contenu) {
				$data->content = $request->edit_activites_contenu;
			}  else {
				$data->content = $data->content;
			}
			if(Input::file('edit_activites_header')!= null && Input::file('edit_activites_header')->isValid()) {
				$headerImage = Input::file('edit_activites_header')->getClientOriginalName();
				$data->header_picture = $headerImage; 
			} else {
				$data->header_picture = $data->header_picture;
			}
			if($data->slug != $request->edit_activites_titre) {
				$slug = Str::slug($request->edit_activites_titre);
				$data->slug = $slug;
			} else {
				$data->slug = $data->slug;
			}
			if(Input::file('edit_activites_plaquette')!= null && Input::file('edit_activites_plaquette')->isValid()) {
				$plaquette = Input::file('edit_activites_plaquette')->getClientOriginalName();
				$data->plaquette = $plaquette; 
				var_dump($plaquette); 
			} else {
				$data->plaquette = $data->plaquette;
			}
			$data->save();
			return back()
				->with('success', "L'activité a bien été modifié");
		} else {
			return back()
				->with('error', "l'activité n'as pas pu être modifier , veuillez réessayer plus tard");
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		$data = Activite::find($id);
		if($data) {
			if($data->active == 1) {
				$data->active = 0;
				$data->save();
				return back()
					->with('success', "l'activité a bien été désactivée");
			} else {
				$data->active = 1;
				$data->save();
				return back()
					->with('success', "l'activité à bien été activée");
			}
		} else {
			return back()
				->with('error', "l'activité n'as pas pu être désactivée veuillez réessayer");
		}
	}
}
