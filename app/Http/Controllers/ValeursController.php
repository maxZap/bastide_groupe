<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Valeur;
use View;
use Illuminate\Http\Request;
use Validator;
use Input;

class ValeursController extends Controller {

	/*/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		$valeurs = Valeur::all();
		return view::make('pages.valeur')
			->with('valeurs', $valeurs);

	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request) {
		$destinationPath = 'uploads/ressources/';
		$validator = Validator::make($request->all(), ['' => '']);

		if($validator->fails()) {
			return back()
				->with('error', 'veuillez remplir tout les champs');
		} else {
			$valeurs = new Valeur;
			$currentId = Valeur::OrderBy('ID', 'desc')->first()->id +1;
			$valeurs->titre = Input::get('valeur_titre');
			$valeurs->description = Input::get('valeur_description');
			if(Input::file('valeur_image') != null && Input::file('valeur_image')->isValid()) {
				$fileName = Input::file('valeur_image')->getClientOriginalName();
				Input::file('valeur_image')->move($destinationPath,$fileName);
				$valeurs->filename 	= $fileName;
				$valeurs->active = 1;
				$valeurs->save();
				return back()
					->with('success', 'La valeur a bien été enregistré');
			} else {
				return back()
					->with('error', ' Un problème est survenue, veuillez réessayer plus tard');
			}

		}
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request) {
		$destinationPath = 'uploads/ressources/';
			$id = $request->edit_valeur_id;
			$data = Valeur::find($id);
			if(isset($data)) {
				if($request->edit_valeur_titre) {
					$data->titre = $request->edit_valeur_titre;
				} else {
					$data->titre = $data->titre;
				}
				if($request->edit_valeur_description) {
					$data->description = $request->edit_valeur_description;
				} else {
					$data->description = $data->description ;
				}
				if(Input::file('edit_valeur_image')!= null && Input::file('edit_valeur_image')->isValid()) {
					$fileName = Input::file('edit_valeur_image')->getClientOriginalName();
					Input::file('edit_valeur_image')->move($destinationPath,$fileName);
					$data->filename 	= $fileName;
				} else {
					$data->filename = $data->filename;
				}
					$data->save();
					return back()
						->with('success', 'la valeur a  bien été modifié ');
			} else {
				return back()
					->with('error', 'impossible de modifié la valeur, veuillez réessayer plus tard');
			
			}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		$data = Valeur::find($id);
		if($data) {
			if($data->active == 1) {
				$data->active = 0;
				$data->save();
				return back()
					->with('success', 'Element désactivé');
			} else {
				$data->active = 1;
				$data->save();
				return back()
					->with('success', 'Element activé');
			}
		} else {
			return back()
				->with('error', 'Impossible de désactivé cet élement');
		}
	}
}
