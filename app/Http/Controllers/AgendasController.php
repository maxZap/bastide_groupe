<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Agenda;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class AgendasController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$agendas = Agenda::all();
			return view::make('admin.indexAgenda')
				->with('agendas', $agendas);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request) {
		$message = ['required' => 'veuillez remplir ce champs'];
		$validator = Validator::make($request->all(), [
			'titre' => 'required',
			'mois_debut' => 'required',
			'mois_fin' => 'required',
			'adresse' => 'required',
			'ville' => 'required',
			'horaire_debut' => 'required',
			'horaire_fin' => 'required',
			'numero_stand' => 'required',
			'description' => 'required',
		], $message);

		if($validator->fails()) {
			return back()
				->with('error', 'veuillez remplir tout les champs');
		} else {
			$agenda = New Agenda;
			$agenda->title = Input::get('titre');
			$agenda->mois_debut = Input::get('mois_debut');
			$agenda->mois_fin = Input::get('mois_fin');
			$agenda->adresse = Input::get('adresse');
			$agenda->ville = Input::get('ville');
			$agenda->horaire_debut = Input::get('horaire_debut');
			$agenda->horaire_fin = Input::get('horaire_fin');
			$agenda->numero_stand = Input::get('numero_stand');
			$agenda->description = Input::get('description');
			$agenda->site_internet = Input::get('site_internet');
			$agenda->active = 1;
			$agenda->save();
			return back()
				->with('success', "l'évênement a bien été enregistré");
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request) {
			$id = $request->edit_agenda_id;
			$data = Agenda::find($id);
			if(isset($data)){
				if($request->edit_agenda_titre) {
					$data->title = $request->edit_agenda_titre;
				} else {
					$data->title = $data->title;
				}
				if($request->edit_agenda_mois_debut) {
					$data->mois_debut = $request->edit_agenda_mois_debut;
				} else {
					$data->mois_debut = $data->mois_debut;
				}
				if($request->edit_agenda_mois_fin) {
					$data->mois_fin = $request->edit_agenda_mois_fin;
				} else {
					$data->mois_fin = $data->moins_fin;
				}
				if($request->edit_agenda_adresse) {
					$data->adresse = $request->edit_agenda_adresse;
				} else {
					$data->adresse = $data->adresse;
				}
				if($request->edit_agenda_ville) {
					$data->ville = $request->edit_agenda_ville;
				} else {
					$data->ville = $data->ville;
				}
				if($request->edit_agenda_horaire_debut) {
					$data->horaire_debut = $request->edit_agenda_horaire_debut;
				} else {
					$data->horaire_debut = $request->horaire_debut;
				}
				if($request->edit_agenda_horaire_fin) {
					$data->horaire_fin = $request->edit_agenda_horaire_fin;
				} else {
					$data->horaire_fin = $data->horaire;
				}
				if($request->edit_agenda_numero_stand) {
					$data->numero_stand = $request->edit_agenda_numero_stand;
				} else {
					$data->numero_stand = $data->numero_stand;
				}
				if($request->edit_agenda_description) {
					$data->description = $request->edit_agenda_description;
				} else {
					$data->description = $data->description;
				}
				if($request->edit_agenda_site_internet) {
					$data->site_internet = $request->edit_agenda_site_internet; 
				} else {
					$data->site_internet = $data->site_internet;
				}

				$data->save();
				return back()->with('success', "l'évênement a bien été ajouté");
			} else {
			 	return back()->with('error', 'une erreur est survenue merci de réessayer ultérieurement');
			}
		}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id){
		$data = Agenda::find($id);
		if(isset($data)){
			if($data->active == 1){
				$data->active = 0;
				$data->save();
				return back()
					->with('success', " l'evenement selectionée a été désactivé avec succès");
			} else {
				$data->active = 1;
				$data->save();
				return back()
					->with('success', "l'evenement date selectionnée a été activé avec succes");
			}
		} else {
			return back()
				->with('error', 'une erreur est survenue veuillez réessayer plus tard');
		}
	}

}
