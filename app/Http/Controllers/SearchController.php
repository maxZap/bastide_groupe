<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Post;
use App\Categorie;
use App\User;
use App\Comment;
use App\subComment;
use App\subCategorie;
use App\Folder;
use App\fileUpload;
use View;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use DB;

class SearchController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function searchBarFilters(Request $request) {
		$search = $request->id;
        if (is_null($search)) {
           return view('search.livesearch');		   
        }
        else{
            $posts = Post::with('categorie', 'subcategorie')->where('title','LIKE',"%{$search}%")->get();
            $categories = Categorie::with('subcategorie')->where('name','LIKE',"%{$search}%")->get();
            $subcategories = subCategorie::where('nom','LIKE',"%{$search}%")->get();
            $files = fileUpload::with('folder')->where('title','LIKE',"%{$search}%")->get();
            return view('search.livesearchajax')->with(compact('posts', 'categories', 'subcategories', 'files'));
        }
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function searchMedia(Request $request) {	
		$search = $request->id;
	
			switch ($search) {
			case 0:
				$rows = fileUpload::all();
				break;
			case 2:
				$rows = fileUpload::where('type', "=", 2)->get();
				break;
			case 3:
				$rows = fileUpload::where('type', '=', 3)->get();
				
				break;
			case 4:
				$rows = fileUpload::where('type', '=', 4)->get();
				break;
		}

		foreach($rows as $row) {
			echo 
			'<div class="searchFilterResults"><img src="../uploads/ressources/'.$row->picture_filename.'">
				<p>'.$row->title.'</p>
			</div>';
		}
	}


	public function mosaiqueFilter(Request $request) {
		$search = $request->id;
		switch ($search) {
		case 0:
			$rows = fileUpload::all();
			break;
		case 2:
			$rows = fileUpload::where('type', "=", 2)->get();
			break;
		case 3:
			$rows = fileUpload::where('type', '=', 3)->get();
			
			break;
		case 4:
			$rows = fileUpload::where('type', '=', 4)->get();
			break;
		}
		
		foreach($rows as $row) {
			if($search != '0' && $row->type == 2) {
				
				echo '<div class="video-searchFilterResults"><iframe width="800" height="400" src="'.$row->lien_youtube.'" frameborder="0" allowfullscreen></iframe><p>'.$row->title.'</p></div>';

			} elseif ($search != '0' && $row->type == 3) {
						echo '
							<div class="mediatheque-container-data" >
								<div id="imgWrapper" class="filteredImages">
									<img src="../uploads/images/'.$row->filename.'" id='.$row->id.' onclick="enlarge(this.src, this.id);">
									<p class="date" id="imgP'.$row->id.'">'.$row->created_at->format("d/m/Y").'</p>
									<figcaption class="caption" id="imgCaption'.$row->id.'">'.$row->title.'</figcaption>	
								</div>
							</div> ';

			} elseif($search === '0' || $search = ""){

				if(empty($row->filename) && !empty($row->lien_youtube) && $row->type ==2) {
					echo '<div class="mediatheque-container-data"><iframe width="800" height="400" src="'.$row->lien_youtube.'" frameborder="0" allowfullscreen></iframe><p>'.$row->title.'</p></div>';
				}
				elseif(!empty($row->filename) && $row->type != 4 )  { // gere les images
							echo '
							<div class="mediatheque-container-data images" >
								<div id="imgWrapper">
									<img src="../uploads/images/'.$row->filename.'" class="filteredImages" id='.$row->id.' onclick="enlarge(this.src, this.id);">
									<p class="date" id="imgP'.$row->id.'">'.$row->created_at->format("d/m/Y").'</p>
									<figcaption class="caption" id="imgCaption'.$row->id.'">'.$row->title.'</figcaption>	
								</div>
							</div> ';

				} elseif (!empty($row->filename) && empty($row->lien_youtube) && $row->type == 4) {
						echo '<div class="mediatheque-container-data"><img src="../uploads/images/pdfskin.jpg" class="pdfImage">
						<p>'.$row->title.'</p>
					</div>';
				}
		
			} 
		}
	}







	

	

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
