<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\subComment;
use Auth; 

class subCommentsController extends Controller {
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request) {
		$validator = Validator::make($request->all(), ['subComsContent' => 'required']);
		if($validator->fails()){
			return back()
				->with('error', 'le contenue de votre commentaire ne peut pas être vide');
		} else {
			$subComments = new subComment; 
			$subComments->content = Input::get('subComsContent');
			$subComments->comment_id = Input::get('commentId');
			$subComments->user_id = Auth::id(); 
			$subComments->save();
			return back()
				->with('success' , 'Votre réponse au commentaire à bien été enregistrée et est en attente de validation d\'un administrateur');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request, $id){
		if($request){	
			$data = subComment::find($id);
			$data->delete($request->all());
			return back()
				->with('success', 'commentaire supprimer avec succès' );
		} else {
			return back()->with('errors','un probleme est survenue lors de la suppression du commentaire');
		}
	}
}
