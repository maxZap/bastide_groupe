<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Carriere;
use View; 
use Illuminate\Http\Request;

class CarrieresController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		$carrieres = Carriere::where('active', '=', '1');
		return View::make('pages.carriere')
			->with('carrieres', $carrieres);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request) {
		$destinationPath = '/uploads/ressources/carriere/';
		$message 		 = ['required' => 'Vous devez remplir tout les champs'];
		$validator 				= Validator::make($request->all(), [
			'activites_titre' 	  => 'required',
			'activites_contenu'   => 'required',
			'activites_header' 	  => 'required',
			'activites_plaquette' => 'required'
		], $message); 

		$carrieres = new Carriere;
		$carriere->nom = Input::get('carriere_nom');
		$carriere->description = Input::get('carriere_description');
		$carriere->url = Input::get('carriere_url');
		
		if(Input::file('carriere_image_presentation') != null && Input::file('carriere_image_presentation')->isValid()) {
			$imagePresentation = Input::file('carriere_image_presentation')->getClientOriginalName();
			Input::file('carriere_image_presentation')->move($destinationPath, $imagePresentation);
		}

		$carriere->save();
		return back()
			->with('success', "la carriere à bien été ajoutée");
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request) {
		$destinationPath = 'uploads/ressources/carriere/';
		$id = $request->edit_carriere_id;
		$data = Carriere::find($id);
		if(isset($data)) {
			if($request->edit_carriere_nom) {
				$data->nom = $request->edit_carriere_nom;
			} else {
				$data->nom = $data->nom;
			}
			if($request->edit_carriere_description) {
				$data->description = $request->edit_carriere_description;
			} else {
				$data->description = $data->description;
			}
			if(Input::file('edit_carriere_image_presentation') != null && Input::file('edit_carriere_image_presentation')->isValid()) {
				$imagePresentation = Input::file('edit_carriere_image_presentation')->getClientOriginalName();
				$data->image_presentation = $imagePresentation;
			} else {
				$data->image_presentation = $data->image_presentation;
			}
			if($request->edit_carriere_url) {
				$data->url = $request->edit_carriere_url;
			} else {
				$data->url = $data->url;
			}
			$data->save();
			return back()
				->with('success', 'la carrière a bien été modifiée');
		} else {
			return back()
				->with('error', "la carrière n'as pas pu être modifiée, réessayer plus tard");
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		$data = Carriere::find($id);
		if($data) {
			if($data->active == 1) {
				$data->active = 0;
				$data->save();
				return back()
					->with('success', "la carrière a bien été désactivé"); 
			} else {
				$data->active = 1;
				$data->save();
				return back()
					->with('success', "la carrière a bien été activé");
			}
			$data->save();
		} else {
			echo 'fail';
			return back()
				->with('error', "impossible de désactiver cette carrière");
		}
	}

}
