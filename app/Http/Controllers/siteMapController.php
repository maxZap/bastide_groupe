<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class siteMapController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		$posts 		  = Post::orderBy('created_at','desc')->first();
		$categorie 	  = Categorie::orderBy('created_at', 'desc')->first();
		$subcategorie = Subcategorie::orderBy('created_at', 'desc')->first();
		return response()->view('sitemap.index', [
			'posts' => $posts,
			'categorie' => $categorie,
			'subcategorie' => $subcategorie,])->header('content-type', 'text/xml');	
	}

	public function posts() {
		$posts = Cache::remember('sitemap-post', 59, function() {
			return Post::orderBy('published_at', 'desc')->limit(50000)->get();
		});
		return response()->view('sitemap.posts', ['posts', $posts,])->header('Content-type', 'text/html');
	}
	
	public function categories() {
		$categories = Cache::remember('sitemap-categorie', 59, function() {
			return Post::orderBy('published_at', 'desc')->limit(50000)->get();
		});
		return response()->view('sitemap.posts', ['categories', $categories,])->header('Content-type', 'text/html');
	}

	public function subCategories() {
		$subCategories = Cache::remember('sitemap-subcategories', 59, function() {
			return Post::orderBy('published_at', 'desc')->limit(50000)->get();
		});
		return response()->view('sitemap.posts', ['subcategories', $subCategories,])->header('Content-type', 'text/html');
	}

	

}
