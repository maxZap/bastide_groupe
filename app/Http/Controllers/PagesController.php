<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\historique;
use App\fileUpload;
use App\Post;
use View;
use Auth;
use DB;
use Illuminate\Http\Request;

class PagesController extends Controller {

	public function Orthopedie() {
		return view::make('pages.orthopedie');
	}

	public function Activité() {
		return view::make('pages.activite');
	}

	public function Valeur() {
		return view::make('pages.valeur');
	}

	public function Historique() {
		$dates = DB::table('historiques')->where('active','=','1')->orderBy('date', 'asc')->get();
		$dateMax = count($dates);
		return view::make('pages.historique')
			->with(compact('dates', 'dateMax'));
	}

	public function Finance() {
		$files = FileUpload::where('active','=','1')->get();
		return view::make('pages.finance')
			->with('files', $files);
	}

	public function carriere() {
		return view::make('pages.carriere');
	}

	public function Presse() {
		$posts = DB::table('posts')->where('active','=','1')->paginate(5);
		$agendas = DB::table('agendas')->where('active','=','1')->get();
		$fileUploads = FileUpload::where('active','=','1')->get();
		return view::make('pages.presse', ['posts'=> $posts, 'agendas' => $agendas, 'fileUploads' => $fileUploads]);
	}


	public function Partenariat() {
		$fileUploads = FileUpload::where('active','=','1')->get();
		$posts = DB::table('posts')->first();
		return view::make('pages.partenariat', ['posts'=> $posts, 'fileUploads' => $fileUploads]);
	}

	public function Franchise() {
		return view::make('pages.franchise');
	}
}
