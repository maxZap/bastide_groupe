<?php namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Post;
use App\Categorie;
use App\Comment;
use App\Subcomment;
use App\subcategorie;
use App\Agenda;
use App\FileUpload;
use View;
use Auth;
use Cache;
use Illuminate\Support\Str;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class PostsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		$posts 		= Post::with('user', 'categorie', 'comment')->where('active','=','1')->OrderBy('created_at', 'DESC')->LIMIT(2)->get();
		$hotPosts 	= Post::with('categorie','subcategorie')->where('hot', '=', '1')->where('active','=','1')->get();
		$agendas 	= Agenda::where('active','=','1')->OrderBy('created_at', 'DESC')->LIMIT(2)->get();

		
		$fileuploads = FileUpload::where('lien_youtube', '!=', '')->where('active','=','1')->LIMIT(1)->get();

		return view::make('posts.index')
			->with(compact('posts', 'hotPosts', 'agendas', 'fileuploads'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request) {
		$destinationPath 		= 'uploads/images';
		$message 				= ['required' => 'Vous devez remplir tout les champs avant de poster'];
		$validator 				= Validator::make($request->all(), [
			'title' 			=> 'required',
			'content' 			=> 'required',
			'description'		=> 'required',
			'categories'   		=> 'required',
			'subcategories'		=> 'required'
		], $message); 
		if($validator->fails()) {
			return back()
				->with('error', 'Veuillez remplir tout les champs');
		} else {
			$posts 					= new Post;
			$currentId = Post::orderBy('id', 'desc')->first()->id + 1;
			$slug = str::slug($currentId . '-' . Input::get('title'));
			$posts->title 			= Input::get('title');
			$posts->content 		= Input::get('content');
			$posts->description 		= Input::get('description');
			if(Input::file('image')!= null && Input::file('image')->isValid()) {
				$fileName = Input::file('image')->getClientOriginalName();
				Input::file('image')->move($destinationPath,$fileName);
				$posts->filename 	= $fileName;
			}
			$posts->slug = $slug;
			$posts->user_id 		= Auth::id();
			$posts->categorie_id 	= Input::get('categories');
			$posts->subcategorie_id = Input::get('subcategories');
			$posts->save();
			return back()
				->with('success', "L'article à bien été enregistré");
		}
	}

 	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

 	
	public function show($categorie, $subcategorie, $slug) {
		$subcategories = subCategorie::where('slug', $subcategorie)->first();
		$subcategorieId = $subcategories['id'];
		$similarPosts = Post::where('subcategorie_id', $subcategorieId)->get();
		$postbyslug = Post::where('slug', $slug)->first();

		$id = $postbyslug->id;
		$posts = Post::where('slug', '=', $slug)->first();
		return View::make('posts.show')
			->with(compact('posts', 'similarPosts'));
	}
 
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request) {
		$destinationPath 	= 'uploads/images';
		$validator = Validator::make($request->all(), ['' => '']);
		if($validator->fails()){
			return back()
				->with('error', 'vous devez renseigner la catégorie d\'affiliation');
		} else {
			$id = $request->edit_post_id;
			$data = Post::find($id);
			if(isset($data)){
				$slug = str::slug($id . '-' . Input::get('title'));
				if($request->edit_post_title) {
					$data->title = $request->edit_post_title;
				} else {
					$data->title = $data->title;
				}
				if($request->edit_post_description){
					$data->description = $request->edit_post_description;
				} else {
					$data->description = $data->description;
				}
				if($request->edit_post_content){
					$data->content = $request->edit_post_content;
				} else {
					$data->content = $data->content;
				}
				if(Input::file('edit_post_image')!= null && Input::file('edit_post_image')->isValid()) {
					$fileName = Input::file('edit_post_image')->getClientOriginalName();
					Input::file('edit_post_image')->move($destinationPath,$fileName);
					$data->filename 	= $fileName;
				} else {
					$data->filename = $data->filename;
				}
				if($request->edit_post_categories) {
					$data->categorie_id = $request->edit_post_categories;
				} else {
					$data->categorie_id = $data->categorie_id;
 				}
 				if($request->edit_post_subcategories) {
 					$data->subcategorie_id = $request->edit_post_subcategories;
 				} else {
 					$data->subcategorie_id = $data->subcategorie_id;
 				}
 				$currentId = Post::find($id)->id;
				$slug = str::slug($currentId . '-' . Input::get('edit_post_title'));
				$data->slug = $slug;
				$data->save();
				return back()
					->with('success', 'Article modifié avec succés');
			} else {
				return back()
					->with('error', 'la sous categorie n\'a pas pu être modifiée ');
			}
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		$data = Post::find($id);
		if(isset($data)) {
			if($data->active == 1) {
				$data->active = 0;
				$data->save();
				return back()
					->with('success', "l'article a bien été archivé");		
			} else {
				$data->active = 1;
				$data->save();
				return back()
					->with('success', "l'article est désormais actif");
			} 
		} else {
			return back()
				->with('error', 'un problème est survenu merci de réessayer plus tard');
		}
	}

}
