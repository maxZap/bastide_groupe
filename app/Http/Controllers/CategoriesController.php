<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Categorie;
use App\Subcategorie;
use View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class CategoriesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request) {
		$data = Categorie::all();
		$subcategories = Subcategorie::with('categorie')->get();
		return view('admin.indexCategories')->with(compact('data','subcategories'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request) {
	 $validator = Validator::make($request->all(), ['nom' => 'required']); 
		if($validator->fails()){
			return back()
				->with('error' , 'Le nom de la catégorie doit être renseignée');
		} else {
			$data = new Categorie;
			$data->name = $request ->nom;
			$data->save();
			return back()
				->with('success','Categories créée');
		}
	}

	public function view(Request $request){
		if($request->ajax()){
			$id = $request->id;
			$info = Categorie::find($id);
			return response()->json($info);
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request) {	
		$id = $request->edit_id;
		$data = Categorie::find($id);
		if(isset($data)){
			if($request->edit_nom){
				$data->name = $request->edit_nom;
			} else {
				$data->name = $data->name;
			}
			if($request->edit_active) {
				$data->active = $request->edit_active;
			} else {
				$data->active = $data->active;
			}
			$data->save();
			return back()
				->with('success', 'categorie correctement modifié');
		} else {
			return back()
				->with('error', 'une érreur est survenue, merci de réessayer plus tard');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Request $request, $id) {
		$data = Categorie::find($id);
		if(isset($data) ) {
			if($data->active == 1) {
				$data->active = 0;
				$data->save();
				return back()
					->with('success', "la categorie a été archivé");
			} else {
				$data->active = 1;
				$data->save();
				return back()
					->with('success', "la categorie est bien affiché");
			}
		} else {
			return back()
				->with('error', 'un problème est survenue veuillez réessayer plus tard');
		}
	}
}
