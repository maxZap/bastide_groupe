<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Folder extends Model {

	public function fileUpload() {
        return $this->hasMany('App\fileUpload');
	} 

	public function categorie() {
		return $this->belongsTo('App\Categorie');
	}

	public function subcategorie() {
		return $this->belongsTo('App\subCategorie');
	}
}
