<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model {

	public $filliable = ['nom', 'created_at', 'updated_at'];

	protected $table = 'categories';

	public function post() {
   		return $this->hasMany('App\Post', 'post_id');
	}

	public function subComment(){
        return $this->hasMany('App\subComment');
	}

	public function subCategorie(){
		return $this->hasMany('App\subCategorie');
	}

	protected $guarded = array();

}
