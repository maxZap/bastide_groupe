<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('posts', function($table) {
			$table->increments('id');
			$table->string('title', 255);
			$table->text('content');
			$table->text('description');
			$table->string('filename');
			$table->tinyInteger('hot')->default(0);
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users');
			$table->integer('categorie_id')->unsigned();
			$table->foreign('categorie_id')->references('id')->on('categories');
			$table->integer('subcategorie_id')->unsigned();
			$table->foreign('subcategorie_id')->references('id')->on('subcategories');
			$table->timestamps();
		});	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
