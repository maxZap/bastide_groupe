<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Post;
use Illuminate\Support\Str;

class SlugifyPosts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		$posts = Post::all(); 
		$slugs = [];
		// loop through all posts
		foreach ($posts as $post) { 
  		// convert the title into a slug and save it to the slug field  
  			$post->slug = $post->id.'-'.Str::slug($post->title); 

  		// Check for unique slug
  			$count = '';
  			while (in_array($post->slug.$count, $slugs)) {
    			if ($count == '') {
      				$count = 1;
    			}
    			$count++;
  			}
  			// Add count
  			$post->slug .= $count;
  			$slugs[] = $post->slug;
  			// save the post  
  			$post->save(); 
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
