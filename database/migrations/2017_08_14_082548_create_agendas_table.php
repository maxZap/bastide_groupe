<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgendasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('agendas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->integer('jour_debut');
			$table->string('mois_debut');
			$table->integer('jour_fin');
			$table->string('mois_fin');
			$table->string('année');
			$table->string('adresse');
			$table->string('ville');
			$table->integer('horaire_debut');
			$table->integer('horaire_fin');
			$table->integer('numero_stand');
			$table->text('description');
			$table->integer('ordre');
			$table->string('site_internet');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('agendas');
	}

}
