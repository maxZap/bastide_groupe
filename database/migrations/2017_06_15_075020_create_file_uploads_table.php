<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileUploadsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('file_uploads', function(Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->string('filename')->nullable()
			$table->string('lien_youtube')->nullable();
			$table->integer('folder_id')->unsigned();
			$table->foreign('folder_id')->references('id')->on('folders');
			$table->integer('type');
			$table->string('picture_filename');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('file_uploads');
	}

}
