<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('activites', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('titre');
			$table->text('content');
			$table->string('plaquette');
			$table->string('slug');
			$table->tinyInteger('active');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('activites');
	}

}
