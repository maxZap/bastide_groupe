-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Ven 27 Octobre 2017 à 07:42
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bastgroupe`
--
CREATE DATABASE IF NOT EXISTS `bastgroupe` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bastgroupe`;

-- --------------------------------------------------------

--
-- Structure de la table `activites`
--

CREATE TABLE `activites` (
  `id` int(10) UNSIGNED NOT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `plaquette` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `header_picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `activites`
--

INSERT INTO `activites` (`id`, `titre`, `content`, `plaquette`, `slug`, `active`, `created_at`, `updated_at`, `header_picture`) VALUES
(1, 'orthopedie', '<p>Au gré de ses expériences dans l\'activté du maintiens a domicile, Bastide le confort medical a su indentifier des besoins auprès de ses patients et a souhaité enrichir sa préstation en apportant	 une offre complémentaire avec des prodtuis d\'orthopedie et de contention</p>\n 				<p> a ce titre, Bastide le confort medical a adapté des points de vente afin d\'accueilir les patients dans une cadre professionnel et dans le respect de leur intimité.</p>\n 				<p><span class="blue-title">Nos services : </span></p>\n 				<ul> \n		 			<li> Toutes les références dans tout les marques en orthopédie.</li>\n		 			<li> Aucune avance de frais pour les articles pris en charge.</li>\n		 			<li> Nous assurons toutes les formalités administratives.</li>\n		 			<li> Les produits non remboursés sont disponibles aux meilleurs prix du marché.</li>\n		 			<li> livraison adaptées aux besoins réel du patient.</li>\n	 			</ul>', 'test.pdf', 'orthopedie', 1, '2017-10-10 22:00:00', '2017-10-19 12:19:56', '');

-- --------------------------------------------------------

--
-- Structure de la table `agendas`
--

CREATE TABLE `agendas` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jour_debut` int(11) NOT NULL,
  `mois_debut` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jour_fin` int(11) NOT NULL,
  `mois_fin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `année` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adresse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ville` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `horaire_debut` int(11) NOT NULL,
  `horaire_fin` int(11) NOT NULL,
  `numero_stand` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `ordre` int(11) NOT NULL,
  `site_internet` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `agendas`
--

INSERT INTO `agendas` (`id`, `title`, `jour_debut`, `mois_debut`, `jour_fin`, `mois_fin`, `année`, `adresse`, `ville`, `horaire_debut`, `horaire_fin`, `numero_stand`, `description`, `ordre`, `site_internet`, `created_at`, `updated_at`, `active`) VALUES
(1, 'Salon Franchise Expo', 18, 'septembre', 22, '', '2017', 'parc des expositions', 'Paris', 0, 0, 23, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 'http://franchiseparis.com', '2017-08-16 22:00:00', '2017-10-13 10:51:00', 0),
(2, 'Salon Franchise Expo', 18, 'septembre', 22, 'septembre', '2017', 'parc des expositions', 'marseille', 9, 21, 30, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 2, 'http://franchisemarseille.com', '2017-08-15 22:00:00', '2017-08-17 22:00:00', 1),
(3, 'Salon Franchise Expo', 18, 'septembre', 22, 'septembre', '2017', 'parc des expositions', 'Lyon', 9, 21, 65, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 3, 'http://franchiselyon.com', '2017-08-17 22:00:00', '2017-08-22 22:00:00', 1),
(4, 'Salon Franchise Expo', 18, 'septembre', 22, 'septembre', '2017', 'parc des expositions', 'Bordeaux', 9, 21, 48, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 4, 'http://franchisebordeaux.com', '2017-08-17 22:00:00', '2017-08-22 22:00:00', 1),
(5, 'Salon Franchise Expo', 18, 'septembre', 22, 'septembre', '2017', 'parc des expositions', 'Lilles', 9, 21, 90, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 5, 'http://franchselille.com', '2017-08-16 22:00:00', '2017-08-19 22:00:00', 1),
(6, 'Salon Franchise Expo', 18, 'septembre', 22, 'septembre', '2017', 'parc des expositions', 'perpignan', 9, 21, 25, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 6, 'http://franchiseperpignan.com', '2017-08-23 22:00:00', '2017-10-04 07:04:59', 1),
(8, 'Salon de la fatigue ', 0, '04 Octobre 2017', 0, '13 Octobre 2017', '', 'Lit', 'Chambre', 0, 0, 20, 'fvb rhzefgruqeh guqehqgruqe ghruqeghrue io', 0, 'g hy uqthuzeo', '2017-10-04 12:43:02', '2017-10-05 07:18:38', 1);

-- --------------------------------------------------------

--
-- Structure de la table `carrieres`
--

CREATE TABLE `carrieres` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_presentation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `carrieres`
--

INSERT INTO `carrieres` (`id`, `nom`, `image_presentation`, `active`, `description`, `url`, `created_at`, `updated_at`) VALUES
(1, 'magasinier.ere', 'magasinier.jpg', 1, 'lorem ipsum dolor sit amet', 'osef.com', '2017-10-18 22:00:00', '2017-10-25 11:13:16'),
(2, 'technicien.ne conseil', 'technicien_conseil.jpg', 1, 'lorem ipsum dolor sit amet', 'osef.com', '2017-10-17 22:00:00', '2017-10-27 22:00:00'),
(3, 'Secrétaire adminitif.ve', 'secretaire.jpg', 1, 'lorem ipsum dolor sit amet', 'osef.com', '2017-10-17 22:00:00', '2017-10-26 22:00:00'),
(4, 'Responsable d\'agence', 'responsable_agence.jpg', 1, 'lorem ipsum dolor sit amet', 'osef.com', '2017-10-19 22:00:00', '2017-10-30 23:00:00'),
(5, 'Livreur.se', 'livreur.png', 1, 'lorem ipsum dolor sit amet', 'livreur.png', '2017-10-17 22:00:00', '2017-10-30 23:00:00'),
(6, 'infirmier.ère conseil', 'infirmier.jpg', 1, 'lorem ipsum dolor sit amet', 'osef.com', '2017-10-17 22:00:00', '2017-10-24 10:44:31');

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` tinyint(4) NOT NULL,
  `header_picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `created_at`, `updated_at`, `active`, `header_picture`) VALUES
(3, 'nos activités', 'nos-activités', '2017-06-20 12:19:32', '2017-10-02 12:13:17', 1, ''),
(4, 'Communication', 'Communication', '2017-06-21 05:04:12', '2017-07-31 11:57:12', 1, ''),
(5, 'nous rejoindre', 'nous-rejoindre', '2017-07-31 11:57:20', '2017-07-31 11:57:20', 1, ''),
(1, 'nous connaître', 'nous-connaître', '2017-10-18 22:00:00', '2017-10-15 22:00:00', 1, ''),
(2, 'valeurs', 'valeurs', '2017-09-19 22:00:00', '2017-09-15 22:00:00', 1, ''),
(9, 'ertyguiop^lm', '', '2017-10-02 07:23:55', '2017-10-05 11:14:58', 1, '');

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `comments`
--

INSERT INTO `comments` (`id`, `content`, `user_id`, `post_id`, `active`, `created_at`, `updated_at`) VALUES
(26, 'dtfgyhzetr^$upo', 9, 1, 1, '2017-08-01 10:58:45', '2017-08-01 10:58:49'),
(3, 'commentaire', 9, 1, 1, '2017-06-12 11:37:23', '2017-06-12 11:37:23');

-- --------------------------------------------------------

--
-- Structure de la table `file_uploads`
--

CREATE TABLE `file_uploads` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lien_youtube` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `folder_id` int(10) UNSIGNED NOT NULL,
  `type` int(11) NOT NULL,
  `picture_filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `file_uploads`
--

INSERT INTO `file_uploads` (`id`, `title`, `filename`, `lien_youtube`, `folder_id`, `type`, `picture_filename`, `created_at`, `updated_at`, `active`) VALUES
(1, '3ème Assise Bastide Medical', '', 'https://www.youtube.com/embed/9O1n4bWxZME', 2, 2, 'movieicon.svg', '2017-08-17 22:00:00', '2017-08-17 22:00:00', 1),
(2, 'Grand Prix Objectif 2016', 'blabla.jpg', '', 3, 3, 'imgicon.svg', '2017-08-27 22:00:00', '2017-08-23 22:00:00', 1),
(3, 'Kito de Pavant à bord du Bastide-Otio', 'blabla.jpg', '', 2, 3, 'imgicon.svg', '2017-08-07 22:00:00', '2017-08-16 22:00:00', 1),
(4, 'Plaquette Bastide le Confort Médical', 'bastide_fevrier2017.pdf', '', 1, 4, 'txticon.svg', '2017-08-02 22:00:00', '2017-08-14 22:00:00', 1),
(5, 'Rachat de la société jean michel', 'blabla.jpg', '', 1, 3, 'imgicon.svg', '2017-08-08 22:00:00', '2017-08-18 22:00:00', 1);

-- --------------------------------------------------------

--
-- Structure de la table `folders`
--

CREATE TABLE `folders` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categorie_id` int(10) UNSIGNED NOT NULL,
  `subcategorie_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `folders`
--

INSERT INTO `folders` (`id`, `name`, `slug`, `categorie_id`, `subcategorie_id`, `created_at`, `updated_at`) VALUES
(1, 'année 2013', '1-annee-2015', 1, 1, '2017-06-21 11:06:10', '2017-08-01 10:39:12'),
(2, 'annee 2014', '2-annee-2014', 4, 1, '2017-06-21 11:06:40', '2017-06-21 11:06:40');

-- --------------------------------------------------------

--
-- Structure de la table `historiques`
--

CREATE TABLE `historiques` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `historiques`
--

INSERT INTO `historiques` (`id`, `date`, `description`, `created_at`, `updated_at`, `active`) VALUES
(4, '1997', 'Création de quatre nouvelles agences : Cannes, Montélimar, Toulouse et Toulon, Rachat de la société L\'homme Rabier Clermond-Ferrand, première croissance externe de l\'entreprise : Introduction en Bourse sur le second marché le 17 mai', '2017-09-20 22:00:00', '2017-10-06 13:24:21', 1),
(3, '1993', 'Création d\'un site de stockage et de service a Villeurbane, initiation d\'une démarche qualité dans le domaine de l\'assistance respiratoire', '2017-09-20 22:00:00', '2017-09-08 22:00:00', 1),
(1, '1977', 'Création de la société a Nîmes ', '2017-09-07 22:00:00', '2017-09-05 22:00:00', 1),
(2, '1989', 'Déplacement de la société qui se sépare du siège social pour s\'installer  dans la zone commerciale de Ville Active, Création de l\'agence de Montpellier ', '2017-09-19 22:00:00', '2017-09-15 22:00:00', 1),
(5, '1998', 'Création des agences de Narbonne, Vitrolle, et Bordeaux. Rachat de la société Médical Guiraud implantée à Carcassone et Perpignant et de la société Hygiene Service a Paris ', '2017-09-25 22:00:00', '2017-09-21 22:00:00', 1),
(6, '1999', 'Rachat de la société S.PM. 2000 a Soissons, de la ociété Médieco Savoie à Chambéry et de la société médcial Chaubet a Foix, Pamiers et Lavelanet. Rachat du fond de commerce de la société Alpha Médical a Tours ', '2017-09-20 22:00:00', '2017-09-28 22:00:00', 1),
(7, '2000', ' Rachat du fond de commerce de la société Perimédical a Tours. De la société Sésame Rhône Alpe a Oullins ( Lyon) et de la société Aerodom implantée a Amiens et Rouen. Rachat du groupe Confortis (15 agences) et de la société Médical Home Santé ( à Lyon Croix-Rousse et Bassin Demi-Lune)  ', '2017-09-06 22:00:00', '2017-09-21 22:00:00', 1),
(8, '2008', 'Création des Agences de Metz, Orange, Màcon, Bourges et Le Havre , Ouverture de la plate-forme de stockage Sud à Gallargues. Lancement du concept de Franchises a Nevers, Bourgoin-Jallieu, Rochefort ', '2017-09-20 22:00:00', '2017-09-21 22:00:00', 1),
(9, '2009', '5 agences créées au Havre, à Angers à Evry , à Limoges et à Poitiers. 8 Nouvelles franchises a Argenteuil, Château, Thierry, Villefranche, Auch, Brive, Sain orner, Cambrai, Vienne ', '2017-09-13 22:00:00', '2017-09-28 22:00:00', 1),
(10, '2010', 'Acquisition des société Médikea MP, spécialisée dans la fourniture de solution de stomathérapie et l\'auto-sondage urinaire', '2017-09-13 22:00:00', '2017-09-21 22:00:00', 1),
(11, '2011', 'Acquisition des société A à Z Santé ( Respiratoire) et AB2M spécialisée dans la fourniture de solution de stomathérapie et l\'urologie ', '2017-09-12 22:00:00', '2017-09-28 22:00:00', 1),
(12, '2012', 'prise de contrôle a 100 dans la SARL MEDAVI NORD', '2017-09-18 22:00:00', '2017-09-21 22:00:00', 1),
(13, '2013', 'DOM\'Air acquis en Janvier 2013\r\nDorge Medic acquis en Juillet 2013', '2017-09-14 22:00:00', '2017-09-26 22:00:00', 1);

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2017_06_15_075020_create_file_uploads_table', 1),
('2017_06_16_071236_add_slug_to_posts', 2),
('2017_06_16_072645_slugify_posts', 3),
('2017_10_02_121253_add_active_to_categories', 4),
('2017_10_02_121803_add_active_to_subcategories', 5),
('2017_10_02_121929_add_active_to_historique', 5),
('2017_10_02_122006_add_active_to_file_uploads', 5),
('2017_10_02_122318_add_active_to_users', 5),
('2017_10_02_122340_add_active_to_posts', 5),
('2017_10_02_122404_add_active_to_agendas', 5),
('2017_10_16_075220_create_valeurs_table', 6),
('2017_10_17_152620_create_activite_contents_table', 7),
('2017_10_17_120959_create_activites_table', 8),
('2017_10_19_125222_add_pictures_to_activites', 9),
('2017_10_23_120350_create_carrieres_table', 10);

-- --------------------------------------------------------

--
-- Structure de la table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hot` tinyint(4) NOT NULL DEFAULT '0',
  `user_id` int(10) UNSIGNED NOT NULL,
  `categorie_id` int(10) UNSIGNED NOT NULL,
  `subcategorie_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `slug` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `posts`
--

INSERT INTO `posts` (`id`, `title`, `content`, `description`, `filename`, `hot`, `user_id`, `categorie_id`, `subcategorie_id`, `created_at`, `updated_at`, `slug`, `active`) VALUES
(1, 'lol', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commod \n\nhello\n\nLOLLOLOLOL lorem ipsum dolor sit amet grfegjçrqejgirjqegrhzqengfbnzqegfbzqehgfbqf fzeufqsqe consequat. Duis aute irure dolor in reprehenderit in voluptate velit ess cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'En developpement permanent de son réseau, Bastide le Confort Médical vient a la rencontre de ses futurs partenaires a l\'occasion', 'blabla.jpg', 1, 1, 1, 1, '2017-07-31 22:00:00', '2017-10-13 11:34:34', '1-lol', 1),
(2, 'mon deuxieme article ', 'ceci est le deuxieme contenu de mon deuxieme article ', 'En developpement permanent de son réseau, Bastide le Confort Médical vient a la rencontre de ses futurs partenaires a l\'occasion', 'blabla.jpg', 0, 9, 1, 1, '2017-08-01 22:00:00', '2017-10-03 07:14:40', '2-mon-deuxieme-article', 1),
(3, 'mon troisieme article', 'gbuighwuofh fg eysdgfZG FRYZQE GFYZ GFYZEFF ae zaef fhgfb hugy fze fzaeh dghzae dyzae f y gr ghuwogh ruie ghui greuio ghruiqeo', 'En developpement permanent de son réseau, Bastide le Confort Médical vient a la rencontre de ses futurs partenaires a l\'occasion', 'blabla.jpg', 0, 1, 2, 1, '2017-08-08 22:00:00', '2017-10-03 10:08:06', '3-mon-troisieme-article', 1);

-- --------------------------------------------------------

--
-- Structure de la table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categorie_id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `nom`, `categorie_id`, `slug`, `created_at`, `updated_at`, `active`) VALUES
(5, 'gouvernance', 1, 'gouvrernance', '2017-07-31 12:00:44', '2017-10-03 05:58:59', 1),
(1, 'historique', 1, 'historique', '2017-06-22 05:36:08', '2017-07-31 11:57:40', 1),
(6, 'présentation', 3, 'presentation', '2017-07-31 12:10:42', '2017-07-31 12:10:42', 1),
(7, 'activités', 3, 'activites', '2017-07-31 12:11:04', '2017-07-31 12:11:04', 1),
(8, 'presse', 4, 'presse', '2017-07-31 12:11:31', '2017-10-03 05:50:42', 1),
(9, 'finance', 4, 'finance', '2017-07-31 12:11:41', '2017-10-03 05:50:19', 1),
(10, 'Partenariats', 5, 'partenariats', '2017-07-31 12:11:52', '2017-07-31 12:14:22', 1),
(11, 'liens utiles', 4, 'liens-utiles', '2017-07-31 12:12:02', '2017-07-31 12:12:02', 1),
(12, 'franchise', 5, 'franchise', '2017-07-31 12:12:20', '2017-07-31 12:12:20', 1),
(13, 'carrière', 5, 'carriere', '2017-07-31 12:14:40', '2017-10-03 10:07:42', 1);

-- --------------------------------------------------------

--
-- Structure de la table `sub_comments`
--

CREATE TABLE `sub_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `comment_id` int(10) UNSIGNED NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `sub_comments`
--

INSERT INTO `sub_comments` (`id`, `content`, `user_id`, `comment_id`, `active`, `created_at`, `updated_at`) VALUES
(3, 'sous commentaire ', 9, 3, 2, '2017-06-14 07:40:03', '2017-06-14 07:40:08'),
(2, 'sous commentaire', 9, 3, 1, '2017-06-14 07:22:32', '2017-06-14 07:38:53'),
(24, 'sous commentaire\n', 9, 2, 1, '2017-06-14 12:40:20', '2017-06-14 12:40:20'),
(23, 'sous commentaire', 9, 2, 2, '2017-06-14 12:35:22', '2017-06-14 12:35:22'),
(25, 'sous commentaire', 9, 2, 1, '2017-06-14 13:19:15', '2017-06-14 13:19:15'),
(28, 'sous commentaire', 9, 3, 1, '2017-06-15 10:21:59', '2017-06-15 10:21:59'),
(29, 'htlù*d', 9, 2, 0, '2017-06-16 06:01:07', '2017-06-16 06:01:07');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activation_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `admin` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `nom`, `prenom`, `password`, `email`, `created_at`, `updated_at`, `remember_token`, `activation_token`, `admin`, `active`) VALUES
(1, 'zaplata', 'maxime', '$2y$10$bLM8UjyCy6HTOxcOCSnL7.rQt1kTSGib7wZKUetkNhFVeL0sKmpum', 'zaplatamaxime@gmail.com', NULL, '2017-10-12 10:48:45', 'AjS8Z8yaBt09kM9DhrXog6odC1KDFR2TvizZfv41r5y2NtJ0PITNPWY4VOMf', '', 1, 1),
(2, 'njkl', 'hjkl', '$2y$10$EjITxb3ih9im0diQhvQ8ueH5zw4C7K2tD3nFdC/ZptovTHH4o/z6K', 'salutsalut@salutsalut.salutsalut', '2017-05-31 13:51:35', '2017-08-01 11:31:41', '', '', 0, 1),
(3, 'ghklgho', 'ghog', 'vhnflvwnfj', 'lol@gmail.com', '2017-06-01 05:27:49', '2017-06-01 05:27:49', '', '', 0, 1),
(4, 'rtfjy', 'fjgfjfgj', 'ghjvjkh vhk', 'ffj@hhhh.com', '2017-06-01 05:44:09', '2017-06-01 05:44:09', '', '', 0, 1),
(5, 'salutsalutr', 'salutsalut', 'salutsalut', 'salut@salut.com', '2017-06-01 10:45:52', '2017-06-01 10:45:52', '', '', 0, 1),
(6, 'ghjgk', 'ghkgkg', '$2y$10$GXN7ovg6Me6rbEJYeHKD/el9137YmD9CAwIRKnyV9Ia9Y2oP2FgDa', 'gyigi@hnjil.com', '2017-06-01 11:58:40', '2017-06-01 11:58:40', '', '', 0, 1),
(7, 'jean-marc', 'delasalle-delarue', '$2y$10$6pCJOTHnJXG2sR3aViawOOOq8uK3/0L07NNg9qg2mrQLAywWcBfxC', 'Jeanmarchdelassaledelsarue@salut.com', '2017-06-01 11:59:38', '2017-06-01 11:59:38', '', '', 0, 1),
(8, 'lolololollo', 'lolollol', '$2y$10$YrINsTM/Hw/ygw7AA6DjVOgonjF2cdNR55unUB.wHY4GgWcZe2nTe', 'lolollo@lolloo.com', '2017-06-01 12:00:25', '2017-06-01 12:00:25', '', '', 0, 1),
(9, 'maxime', 'zaplata', '$2y$10$8rQBlmZwI95iUWbCLs81GOzhDrsIrOCWFX4UzgFlzsUHVPcfXOsrW', 'lololol@lolololol.com', '2017-06-01 12:33:56', '2017-08-01 11:15:08', 'Tr3VKqJtoOoHKtJTbvgqW0qkitKt4ckl7mmfszcFMF3qpFL5wsPK80kKbQ7i', '', 2, 1),
(10, 'zap', 'max', '$2y$10$na021FVsOS6aZZtHmNYALuaawZXR.rxSCCTRxlvQjmtgIewzr0/Ke', 'lol@lol.lol', '2017-06-02 12:19:23', '2017-06-02 12:21:04', 'm7Jj7qblFOYgAcm36PWVbcNHspLixK2pVAh3elAnA5iD6pT96ct21BBMYFAZ', '', 0, 1),
(11, 'vjgprieofhsq', 'hjsqohfdgowd²', '$2y$10$zFxDDn1oUrEve4V.EN7WreNFDGy600MbLc32AVDX.AkVsCpMB4XuW', 'lololol@lolololol.com', '2017-06-07 05:14:38', '2017-06-07 05:14:38', '', '', 0, 1),
(12, 'lolol', 'lolololol', '$2y$10$AKJLbhnBkfmdx4D2rtatu.hXTegHmNBLPZg8OyThtTX18qarBAune', 'lolololol@lolololol.lolololl', '2017-06-15 10:02:11', '2017-06-15 10:02:11', '', '', 0, 1),
(13, 'bhjvbkl', 'wfghjikop^l', '$2y$10$LTO7u5c0kdxsY6MFicnyveQ3UbtTMv7pz5x1.HnKTj4zQd1fEnBX2', 'email.email@email.email', '2017-06-15 10:04:02', '2017-06-15 10:04:02', '', '', 0, 1),
(15, 'jean', 'mochel', '$2y$10$jbLYuq9Tydbt127zXC8Ro.w.mwlIeCy9ET6zdXUU8pn9dd8YsxooS', 'jeanmichel@gmail.com', '2017-10-12 07:07:16', '2017-10-12 07:38:17', '', '', 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `valeurs`
--

CREATE TABLE `valeurs` (
  `id` int(10) UNSIGNED NOT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `valeurs`
--

INSERT INTO `valeurs` (`id`, `titre`, `description`, `filename`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Ecoute', 'Livraison adapatées aux besoins réels du patient. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'vendeuse_homme.png', 1, '2017-10-10 22:00:00', '2017-10-17 07:17:45'),
(2, 'Respect', 'Livraison adapatées aux besoins réels du patient. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'vendeuse_homme.png', 1, '2017-10-10 22:00:00', '2017-10-26 22:00:00'),
(3, 'Empathie', 'Livraison adapatées aux besoins réels du patient. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'vendeuse_homme.png', 1, '2017-10-17 22:00:00', '2017-10-26 22:00:00'),
(4, 'Sérieux', 'Livraison adapatées aux besoins réels du patient. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'vendeuse_homme.png', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Solidarité', 'Livraison adapatées aux besoins réels du patient. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'vendeuse_homme.png', 1, '2017-10-09 22:00:00', '2017-10-17 10:06:35'),
(7, 'huidhgs', 'vghjijkùmjohfg', 'DMBYAu4VwAA0wBs.jpg', 0, '2017-10-17 07:23:03', '2017-10-23 08:02:27');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `activites`
--
ALTER TABLE `activites`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `agendas`
--
ALTER TABLE `agendas`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `carrieres`
--
ALTER TABLE `carrieres`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_user_id_foreign` (`user_id`),
  ADD KEY `comments_post_id_foreign` (`post_id`);

--
-- Index pour la table `file_uploads`
--
ALTER TABLE `file_uploads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `file_uploads_folder_id_foreign` (`folder_id`);

--
-- Index pour la table `folders`
--
ALTER TABLE `folders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `folders_categorie_id_foreign` (`categorie_id`),
  ADD KEY `folders_subcategorie_id_foreign` (`subcategorie_id`);

--
-- Index pour la table `historiques`
--
ALTER TABLE `historiques`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_user_id_foreign` (`user_id`),
  ADD KEY `posts_categorie_id_foreign` (`categorie_id`),
  ADD KEY `posts_subcategorie_id_foreign` (`subcategorie_id`);

--
-- Index pour la table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_categories_categorie_id_foreign` (`categorie_id`);

--
-- Index pour la table `sub_comments`
--
ALTER TABLE `sub_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_comments_user_id_foreign` (`user_id`),
  ADD KEY `sub_comments_comment_id_foreign` (`comment_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `valeurs`
--
ALTER TABLE `valeurs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `activites`
--
ALTER TABLE `activites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `agendas`
--
ALTER TABLE `agendas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `carrieres`
--
ALTER TABLE `carrieres`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT pour la table `file_uploads`
--
ALTER TABLE `file_uploads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `folders`
--
ALTER TABLE `folders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `historiques`
--
ALTER TABLE `historiques`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT pour la table `sub_comments`
--
ALTER TABLE `sub_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `valeurs`
--
ALTER TABLE `valeurs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
